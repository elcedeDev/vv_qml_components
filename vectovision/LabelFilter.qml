// $Revision: 17190 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1

/*!
    \qmltype LabelFilter
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A label that implemets low pass filtering
 */
Label {
    id: labelFilter
    objectName: "AradexLabelFilter"
    property real filterTime : 1.0
    property real __updateTime: 0.0
    property real __coefficientFilter: filterTime > 0 ? Math.exp(-(__updateTime/filterTime)) : 0.0
    property real __filteredValue : 0

    signal setCycleTime(int cycleTime_ms)
    onSetCycleTime: __updateTime = cycleTime_ms / 1000

    signal timerActivated()
    onTimerActivated: {
        if(filterTime > 0)
        {
            if(__textIO !== "" && textIOValue !== undefined) {

                text = formatIOText(filterValue(Number(textIOValue)))
            }
        }
    }

    /*!
        \qmlmethod Label::filterValue()
        Simple average filter function
    */
    function filterValue(input){
        __filteredValue
                = __filteredValue * __coefficientFilter
                + input * (1.0 - __coefficientFilter);
        return __filteredValue
    }
}
