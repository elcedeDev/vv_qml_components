import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1

Dialog{
    id: messageDialog
    objectName: "AradexDialog"
    property alias status: messageDialog.status
    property string __styleName: S.styleName
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }
    property int colorChoice: 1
    property string titleText: ""
    property bool __smooth: S.isFast
    signal styleChanged(string strStyle)

    property bool buttonCancelVisible: true
    property string buttonCancelText: "Cancel"
    property bool __buttonNoVisible: false
    property string __buttonNoText: "No"
    property bool buttonOkVisible: true
    //property string buttonOkText: "Yes"
    property alias buttonOkText: acceptButton.text
    property alias dialogHeight: maindialogBox.height
    property alias dialogWidth: maindialogBox.width
    property string fileChooserText
    property alias localFileSystem: fileChooser.localFileSystem
    //property string currentPath: ""
    property alias currentPath: fileChooser.currentPath
    property string currentFileName: ""
    property alias errorText: fileChooser.errorText
    property alias fileExtension: fileChooser.fileExtension
    property alias hideDirectories: fileChooser.hideDirectories
    property alias __fileChooser: fileChooser

    onCurrentFileNameChanged: {
        fileChooser.currentFile = currentFileName
        console.log("currentFileName", currentFileName)
    }
/*
    onCurrentPathChanged: {
        fileChooser.currentPath = currentPath
    }
*/
//    onStatusChanged: {
//        if(status === S.DialogStatus.Open){
//            fileChooser.initializeDir()
//            fileChooser.__currentIndex = fileChooser.fileNameList.indexOf(currentFileName)
//            fileChooser.currentFile = currentFileName
//        }
//}

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    function initializeDir() {
        fileChooser.initializeDir()
        fileChooser.selectFile()
    }



    GroupBox{
        id: maindialogBox
        colorChoice: messageDialog.colorChoice
        height: S.Style.dialogFileChooserHeightInitial
        width: S.Style.dialogFileChooserWidthInitial
        anchors.centerIn: parent
        headerText: messageDialog.titleText
        headerWidth: width//Math.min(__paintedHeaderWidth * 1.2, width)
        fontSize: 14

        FileChooser{
            id: fileChooser
            anchors{
                horizontalCenter: parent.horizontalCenter
                left: parent.left
                //right: parent.right
                bottom: buttonRow.top
                top: maindialogBox.top
                topMargin: maindialogBox.__headerHeight * 2
            }
            title: messageDialog.fileChooserText
            currentFile: messageDialog.currentFileName
            currentPath: messageDialog.currentPath
            localFileSystem: messageDialog.localFileSystem

        }

        Row{
            id: buttonRow
            anchors{
                bottom: maindialogBox.bottom
                bottomMargin: S.Style.dialogButtonHeightInitial / 2
                horizontalCenter: maindialogBox.horizontalCenter
            }
            spacing: S.Style.dialogButtonHeightInitial / 2
            Button{
                id: cancelButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonCancelText
                onClicked: {
                    messageDialog.close()
                }
                opacity: (buttonCancelVisible === true) ? 1 : 0
            }
            Button{
                id: rejectButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: __buttonNoText
                onClicked: {
                    messageDialog.setNo()
                    messageDialog.close()
                }
                opacity: (__buttonNoVisible === true) ? 1 : 0
            }
            Button{
                id: acceptButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonOkText
                onClicked: {
                    if( fileChooser.currentFile !== "" )
                    {
                        currentPath = fileChooser.currentPath
                        currentFileName = fileChooser.currentFile

                        messageDialog.setOk()
                        messageDialog.close()
                    }
                }
                opacity: (buttonOkVisible === true) ? 1 : 0
            }
        }
    }
}

