// $Revision: 11896 $
import QtQuick 1.1


MouseArea {

    property int __originX
    property int __originY
    property int __globalOriginX
    property int __globalOriginY
    property int __dX
    property int __dY
    property int __EdX
    property int __EdY
    property bool isKeyboardBackground: true

    property bool ready: false
    signal move(int x, int y)
    signal swipe(string direction)
    signal releaseNoDrag()
    signal drag(int dx, int dy)



    onPressed: {
        drag.axis = Drag.XandYAxis
        __originX = mouse.x
        __originY = mouse.y
        __globalOriginX = globaleCursor.getXPosition()
        __globalOriginY = globaleCursor.getYPosition()
        __EdX = 0
        __EdY = 0

    }


    onPositionChanged: {
        __dX = globaleCursor.getXPosition() - __globalOriginX
        __dY = globaleCursor.getYPosition() - __globalOriginY
//        console.log("dxdy:  " + __dX + " / " + __dY)
        if(isKeyboardBackground === true){
            move(__dX, __dY)
            __globalOriginX += __dX
            __globalOriginY += __dY
            __EdX += Math.abs(__dX)
            __EdY += Math.abs(__dY)
        }else{
            drag(__dX, __dY)
        }

//        switch (drag.axis) {
//            case Drag.XandYAxis:
//            {

//                console.log("Mouse: " + mouse.y + " Origin: " + originY +"Move: "  + (mouse.y - originY))
////                if (Math.abs(mouse.x - originX) > 16) {
////                    drag.axis = Drag.XAxis
////                }
////                else if (Math.abs(mouse.y - originY) > 16) {
////                    drag.axis = Drag.YAxis
////                }
//                move(mouse.x - originX,mouse.y - originY)
////                console.log("Move: "  + (mouse.y - originY))
//                break
//            }
//            case Drag.XAxis:
//            {
//                console.log("Pos   X:   "+ (mouse.x - originX))
//                move(mouse.x - originX, 0)
//                break
//            }
//            case Drag.YAxis:
//            {
//                console.log("Pos   Y:   " + (mouse.y - originY))
//                move(0, mouse.y - originY)
//                break
//            }
//        }
    }

    onReleased: {
        switch (drag.axis) {
            case Drag.XandYAxis:
            {
                canceled(mouse)
                break
            }
            case Drag.XAxis:
//            {
//                swipe(mouse.x - __originX < 0 ? "left" : "right")
//                if(mouse.x - __originX < 0){

//                }else{

//                }
//                break
//            }
//            case Drag.YAxis:
//            {
//                swipe(mouse.y - __originY < 0 ? "up" : "down")
//                if(mouse.y - __originY < 0){

//                }else{

//                }        break
//            }
        }
        //
        if(Math.abs(__EdX) < 15 && Math.abs(__EdY) < 15){
            releaseNoDrag()
        }

    }
}
