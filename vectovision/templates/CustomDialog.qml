import QtQuick 1.1
import "style/Style.js"  as S
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1

Dialog{
    id: messageDialog
    objectName: "AradexDialog"
    property alias status: messageDialog.status
    property string __styleName: S.styleName
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    property bool __smooth: S.isFast
    signal styleChanged(string strStyle)

    property alias popupHeight: maindialogBox.height
    property alias popupWidth: maindialogBox.width


    GroupBox{
        id: maindialogBox
        colorChoice: 1
        height: S.Style.dialogHeightInitial
        width: S.Style.dialogWidthInitial
        anchors.centerIn: parent
        headerWidth: width


        Button{
            id: okButton
            anchors{
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: -width* 1.1
                bottom: maindialogBox.bottom
                bottomMargin: height / 2
            }
            width: S.Style.dialogButtonWidthInitial
            height: S.Style.dialogButtonHeightInitial
            text: "OK"
            onClicked: {
                messageDialog.setOk()
                messageDialog.close()
            }
        }

        Button{
            id: noButton
            anchors{
                horizontalCenter: parent.horizontalCenter
                bottom: maindialogBox.bottom
                bottomMargin: height / 2
            }
            width: S.Style.dialogButtonWidthInitial
            height: S.Style.dialogButtonHeightInitial
            text: "No"
            onClicked: {
                messageDialog.setNo()
                messageDialog.close()
            }
        }
        Button{
            id: cancelButton
            anchors{
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: width* 1.1
                bottom: maindialogBox.bottom
                bottomMargin: height / 2
            }
            width: S.Style.dialogButtonWidthInitial
            height: S.Style.dialogButtonHeightInitial
            text: "Cancel"
            onClicked: {
                messageDialog.close()
            }
        }

    }

}






