﻿// $Revision: 19572 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools.shortcut 1.1
import tools 1.1



/*!
    \qmltype Button
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A push button with a text label.

 */
FocusScope {
    id: button
    objectName: "AradexButton_aradexvvstyle"

    property variant currentStyle: S
    onCurrentStyleChanged: {
        S.Style[S.StyleName] = S.Style[S.styleName]
    }

    /*!
        \qmlProperty bool Button::activeFocusOnPress

        Specifies whether the button should
        gain active focus when pressed.

        The default value is \c true. */
    property bool __activeFocusOnPress: true


//    property int testEnum: IoType.IOT_BOOLEAN
//    property variant textenmu: testEnum
    /*!
        \qmlProperty string Button::changeTextTo
        The text to be displayed by target after button push.
        if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a text property
      */
    property string __changeTextTo

    /*!
        \qmlProperty string Button::changeStateTo
        The state a target is changet to after a button push.
       if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a state property
      */
    property string __changeStateTo


    /*!
        \qmlProperty string Button::fontColorNormal
        Font color of displayed Text, when button is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorNormal: S.Style[__styleName].buttonFontColor
    /*!
        \qmlProperty string Button::fontColorDisabled
        Font color of displayed Text, when button is disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorDisabled: S.Style[__styleName].buttonFontDisabledColor
    /*!
        \qmlProperty string Button::fontColorPressed
        Font color of displayed Text, while button is pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorPressed: S.Style[__styleName].buttonFontPressedColor

    /*!
        \qmlProperty string Button::__foregroundImageNormal
        String that holds the URL of the foreground image.
        The default value is \c "".
      */
//    property string __foregroundImageNormal: ""
    property alias foregroundImageNormal: buttonForegroundImage.source
    /*!
        \qmlProperty string Button::__foregroundImagePressed
        String that holds the URL of the foreground image when pressed.
        The default value is \c "".
      */
    property alias foregroundImagePressed: buttonForegroundImagePressed.source
    /*!
        \qmlProperty int Button::__foregroundImageHeight
        Height of an additional Image for the Button.
        The default value is \c 0.
      */

    property int __foregroundImageHeight: S.Style[__styleName].buttonForegroundImageHeight
    /*!
        \qmlProperty int Button::__foregroundImageWidth
        Width of an additional Image for the Button.
        The default value is \c 0.
      */
    property int __foregroundImageWidth: S.Style[__styleName].buttonForegroundImageHeight


    /*!
        \qmlProperty bool Button::__foregroundAnchorRight
        When \c true Additional Image is anchored right, otherwise
        it is anchored left.
        The default value is \c false.
      */
    property int __foregroundHorizontalAlignement: Qt.AlignHCenter
    /*!
        \qmlProperty bool Button::__foregroundAnchorBottom
        When \c true Additional Image is anchored at the top, otherwise
        it is anchored on the bottom.
        The default value is \c false.
      */
    property int __foregroundVerticalAlignement: Qt.AlignVCenter
    /*!
        \qmlProperty int Button::__foregroundXShift
        \brief x offset
        Offset of the additional Imagen in x - direction
        The default value is \c 0.
      */
    property int __foregroundXShift: 0
    /*!
        \qmlProperty int Button::__foregroundXShift
        \brief y offset
        Offset of the additional Imagen in y - direction
        The default value is \c 0.
      */
    property int __foregroundYShift: 0
    /*! \internal
        \qmlmethod Button::__pressedLeftShift
        Change of BackgroundImage Size on pressed in x direction
    */
    property int __pressedLeftShift: S.Style[__styleName].buttonPressedLeftShift
    /*! \internal
        \qmlmethod Button::__pressedRightShift
        Change of BackgroundImage Size on pressed in Pixels in negative x- direction
    */
    property int __pressedRightShift: S.Style[__styleName].buttonPressedRightShift
    /*! \internal
        \qmlmethod Button::__pressedTopShift
        Change of BackgroundImage Size on pressed in Pixels in  y- direction
    */
    property int __pressedTopShift: S.Style[__styleName].buttonPressedTopShift
    /*! \internal
        \qmlmethod Button::__pressedBottomShift
        Change of BackgroundImage Size on pressed in Pixels in negative y- direction
    */
    property int __pressedBottomShift: S.Style[__styleName].buttonPressedBottomShift

    /*!
        \qmlProperty string Button::imageNormal
        Background image URL of button, when  active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageNormal: (S.Style[__styleName].buttonImageNormal != "") ?
                                     S.Style[__styleName].imageFolder + S.Style[__styleName].buttonImageNormal : ""

    /*! \qmlproperty bool Button::__hovered

        This property holds whether the control is being hovered.
    */
    property alias __hovered: behavior.containsMouse

    /*!
        \qmlProperty string Button::imageDisabled
        Background image URL of button, when  disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageDisabled: (S.Style[__styleName].buttonImageDisabled != "") ?
                                       S.Style[__styleName].imageFolder + S.Style[__styleName].buttonImageDisabled : ""
    /*!
        \qmlProperty string Button::imagePressed
        Background image URL of button, while pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imagePressed: (S.Style[__styleName].buttonImagePressed != "") ?
                                      S.Style[__styleName].imageFolder + S.Style[__styleName].buttonImagePressed : ""

    property string __focusImage: (S.Style[__styleName].buttonImageFocus != "") ?
                                    S.Style[__styleName].imageFolder + S.Style[__styleName].buttonImageFocus : ""

    /*!
        \qmlProperty bool Button::isEnabled
        Enables the button. The default value is \c true.
      */
    property bool isEnabled:true
    onIsEnabledChanged: {
        enabled = isEnabled
    }
    enabled: (__enableIO === undefined) ? isEnabled: __enableIO.value


    /*!
        \qmlProperty bool Button::isPressed
        Presses the button. Is used for intitial settings
        The default value is \c true.
      */
    property bool __isPressed: false
    /*!
        \qmlProperty bool Button::keyBinding
        Button can get a hotkey like F1 - F12 for different key values
        use \l Qt::KeyHotLinks and insert values without "Qt::Key_SysReq"
        For example:
        If you want to use page down as a hotkey, the list shows
        "Qt::Key_PageDown" and you need to insert "PageDown"
      */
    property string keyBinding

    /*!
        \qmlProperty string Button::__leftTextMargin
        \brief Left margin of text on top of image margin:
        Additional left Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.leftonFoTextMargin.
      */
    property int __leftTextMargin: S.Style[__styleName].buttonTextLeftMargin
    /*!
        \qmlProperty string Button::__rightTextMargin
        \brief Right margin of text on top of image margin:
        Additional rightMargin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.rightTextMargin.
      */
    property int __rightTextMargin: S.Style[__styleName].buttonTextRightMargin
    /*!
        \qmlProperty string Button::__bottomTextMargin
        \brief Bottom margin of text on top of image margin:
        Additional bottom Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.bottomTextMargin.
      */
    property int __bottomTextMargin: S.Style[__styleName].buttonTextBottomMargin
    /*!
        \qmlProperty string Button::__topTextMargin
        \brief Top margin of text on top of image margin:
        Additional top Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.topTextMargin.
      */
    property int __topTextMargin: S.Style[__styleName].buttonTextTopMargin

    property int __buttonBorderLeftSize: S.Style[__styleName].buttonBorderLeftSize
    property int __buttonBorderTopSize: S.Style[__styleName].buttonBorderTopSize
    property int __buttonBorderRightSize: S.Style[__styleName].buttonBorderRightSize
    property int __buttonBorderBottomSize: S.Style[__styleName].buttonBorderBottomSize

    /*!
        \qmlProperty bool Button::sendToComponentOutOfScope
        If you want to send an Object to a Component out of Scope,
        you have to check this flag and give your target an objName
        The default value is \c false.*/
    property bool __sendToComponentOutOfScope: false


    /*!
        \qmlProperty string Button::target
        QML target Name for changeTextTo/changeStateTo
        needs to be a string with the id name of the target.
      */
    property string __buttonTarget


    /*!
        \qmlProperty string Button::text
        This property holds the text shown on the button. If the button has no
        text, the \l text property will be an empty string.

        The default value is the empty string.
    */
    property string text

    /*!
        \qmlProperty string Button::__textIO
        Connects the text shown on the button to an IO value.

        The default value is the empty string.
    */
    property string textIO
    /*!
      \internal
        \qmlProperty string Button::buttonTextIO
        Is not erased on stateChange like TextIO
    */
    property variant __textIO: eval(textIO)
    onTextIOChanged: {
        if (textIO !== ""){
            __textIO = eval(textIO)
        }
    }

    /*! \internal
        \qmlmethod Button::__formatButtonTextIOText()
        Basically a clickIO that will not be unset on stateChange.

    */
    property variant __onClicked_setIO_true: eval(onClicked_setIO_true)
    /*!
        \qmlProperty string Button::buttonClickIO
        A string that gives the name of an IO. The IO must be a bool.

    */
    property string onClicked_setIO_true
    // for Intialisation
    onOnClicked_setIO_trueChanged: {
        if (onClicked_setIO_true !== ""){
            __onClicked_setIO_true = eval(onClicked_setIO_true)
        }
    }

    /*!
        \qmlProperty int Button::digitsAfterComma
        The number of digits behind the decimal mark
    */
    property int __digitsAfterComma:2


    /*! \internal
        \qmlmethod Button::__formatButtonTextIOText()
        Basically a stateIO that will not be unset on stateChange

    */
    property variant __enableIO: eval(enableIO)
    /*!
        \qmlProperty string Button::buttonStateIO
        A string that gives the name of an IO. The IO must be a bool.
        The enabled status of the  Button will enabled(true)/not disabled(false)
        will always be synchronized with the io.
    */
    property string enableIO
    // for Intialisation
    onEnableIOChanged:  {
        if (enableIO !== ""){
            __enableIO = eval(enableIO)
        }
    }

    /*!
        \qmlProperty string Button::fontPointSize
        PointSize of displayed Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontSize: S.Style[__styleName].buttonFontSize

    /*!
        \qmlproperty enumeration Button::verticalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignTop
        \li Text.AlignBottom
        \li Text.AlignVCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __verticalTextAlignement:  S.Style[__styleName].buttonVerticalAlignement
    /*!
        \qmlproperty enumeration Button::horizontalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignLeft
        \li Text.AlignRight
        \li Text.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __horizontalTextAlignement: S.Style[__styleName].buttonHorizontalAlignement
    /*!
     \qmlsignal Button::clicked()
     This signal is emitted when the user clicks the button. A click is defined
     as a press followed by a release. The corresponding handler is
     \c onClicked.
     */
    signal clicked()
    /*!
     \qmlsignal Button::pressed()
     This signal is emitted when the user presses the button. A click is defined
     as a press followed by a release. The corresponding handler is
     \c onPressed.
     */
    signal pressed()
    /*!
     \qmlsignal Button::pressed()
     This signal is emitted when the user presses the button. A click is defined
     as a press followed by a release. The corresponding handler is
     \c onPressed.
     */
    signal released()
    /*!
        \qmlsignal Button::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)

    /*!
        \qmlsignal Button::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()
    onLanguageChanged: {
        label.text = qsTr(button.text)
    }

    /*
        \qmlsignal Button::outOfScopeStateChange(string objName, string stateName)

        If a state change is directed at a component out of change a signal
        containing \a objName and \a stateName has to be send to c++ main.
      */
    signal outOfScopeStateChange(string objName, string stateName)
    /*
        \qmlsignal Button::outOfScopeTextChange(string objName, string stateName)

        If a text change is directed at a component out of change a signal
        containing \a objName and \a newText has to be send to c++ main.
      */
    signal outOfScopeTextChange(string objName, string newText)

    signal scaledSizeChanged(real scaleFactor)

    /*! \internal
        \qmlmethod Button::__formatButtonTextIOText(buttonTextIOValue)
        Takes the value of the IO connected to the text on the button and formats it
        appropriately, by reducig digits.

    */
    function __getText(){
        var text = __textIO.value
        switch (__textIO.type){
        case IoType.IOT_DOUBLE:
            text = Number(text).toFixed(__digitsAfterComma)
            break;
        case IoType.IOT_INTEGER:
            text = Number(text).toFixed(__digitsAfterComma)
            break;
        default:
            text = String(text)

        }

        return text
    }

    /*!
        \qmlmethod Button::clickedAction()
        Behaviour when Button is Clicked including change of appearance
        and changeTextTo/changeStateTo Actions.

    */
    function clickedAction(){
        button.clicked()

        if(__onClicked_setIO_true !== undefined && __onClicked_setIO_true.type === IoType.IOT_BOOLEAN){
            __onClicked_setIO_true.value = true
        }
//        if (__sendToComponentOutOfScope == true){
//            if (changeStateTo != ""){
//                outOfScopeStateChange(buttonTarget, changeStateTo)
//            }
//            if (__changeTextTo != ""){
//                outOfScopeTextChange(buttonTarget, __changeTextTo)
//            }
//        }else{
//            if (changeStateTo != ""){
//                __target.state = changeStateTo
//            }
//            if (__changeTextTo != ""){
//                __target.text = __changeTextTo
//            }
//        }
    }
    /*!
        \qmlmethod Button::pressedAction()
        Behaviour when Button is pressed including change of appearance.


    */

    function pressedAction(){
        button.pressed()
        __pressed = true        
    }
    /*!
        \qmlmethod Button::releasedAction()
        Behaviour when Button is Clicked including change of appearance
        and changeTextTo/changeStateTo Actions.

    */
    function releasedAction(){
        button.released()
        __pressed = false
    }





    /*! \qmlproperty shortKey
        \ brief The corresponding Qt.Key to the Key binding input.
        The corresponding Qt.Key to the Key binding input.

    */
    property variant shortKey: eval("Qt.Key_" + keyBinding)

    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName


    property bool __smooth: S.isFast



    /*!
        \internal
        \qmlProperty string Button::__target
        QML target Object for changeTextTo/changeStateTo.
        Is initialized with target
      */
    property variant __target: eval(__buttonTarget)
    /*!
        \internal
        \qmlProperty string Button::__pressed
        \brief True if button is currently pressed.
      */
    property bool __pressed: false

    implicitWidth:  S.Style.buttonWidthInitial
    implicitHeight:  S.Style.buttonHeightInitial
//    clip: true


    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]        
    }

    Shortcut {
        key: shortKey
        onActivated: {

            if(button.enabled){
                if(__activeFocusOnPress){
                    button.focus = true
                }
                clickedAction()
                pressedAction()
            }
        }
        onReleased: {
            if(button.enabled){
                 releasedAction()
            }
        }
    }

    MouseArea {
        id: behavior
        anchors.fill: parent
        hoverEnabled: true
        onReleased: releasedAction()
        onCanceled: releasedAction()
        onPressed: if (button.enabled){
                       pressedAction()
                       if (__activeFocusOnPress){
                       forceActiveFocus()
                   }
                   }
        onClicked:{
            if(__activeFocusOnPress){
                button.focus = true
            }
            if (button.enabled){
                clickedAction()
            }
        }
    }

    function commitKeyPressed() {
        if(button.enabled && button.activeFocus){
            clickedAction()
            pressedAction()
        }
    }    

    Keys.onReturnPressed: commitKeyPressed()
    Keys.onEnterPressed: commitKeyPressed()


    BorderImage {
        id: buttonImageDisabled
        anchors.fill: button
        smooth: __smooth
        border{
            left: __buttonBorderLeftSize
            top: __buttonBorderTopSize
            right: __buttonBorderRightSize
            bottom: __buttonBorderBottomSize
        }
        source: __imageDisabled
        visible: !enabled
    }
    BorderImage {
        id: buttonImage
        anchors{
            fill: button
        }
        smooth: __smooth
        border{
            left: __buttonBorderLeftSize
            top: __buttonBorderTopSize
            right: __buttonBorderRightSize
            bottom: __buttonBorderBottomSize
        }
        source: __imageNormal
        visible: enabled
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: buttonImage
                source : __imagePressed

                anchors.leftMargin: __pressedLeftShift
                anchors.rightMargin: __pressedRightShift
                anchors.topMargin: __pressedTopShift
                anchors.bottomMargin: __pressedBottomShift

            }
        }
        transitions: Transition {
            NumberAnimation {
                properties: "anchors.leftMargin, anchors.rightMargin, anchors.topMargin,anchors.bottomMargin"
                easing.type: Easing.OutQuad
                duration: S.isFast ? S.Style[__styleName].buttonSizeAnimationTime : 0
            }

        }
        BorderImage {
            id: focus
            opacity:  (button.activeFocus && S.focusOn) ? S.Style[__styleName].focusOpacity : 0
            smooth: __smooth
            anchors{
                fill: parent
                leftMargin: S.Style[__styleName].buttonFocusShiftLeft
                topMargin:  S.Style[__styleName].buttonFocusShiftRight
                rightMargin: S.Style[__styleName].buttonFocusShiftTop
                bottomMargin: S.Style[__styleName].buttonFocusShiftBottom
            }
            border{
                left: S.Style[__styleName].buttonFocusBorderLeft
                right: S.Style[__styleName].buttonFocusBorderRight
                top: S.Style[__styleName].buttonFocusBorderTop
                bottom: S.Style[__styleName].buttonFocusBorderBottom
            }
            source: __focusImage
        }
    }


    Image {
        id: buttonForegroundImage
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        smooth: __smooth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? 0 :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width) / 2 :
                (parent.width - width)
            ) + __foregroundXShift

        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height) / 2 :
                (parent.height - height)
            )+ __foregroundYShift
        source: ""

        opacity: __pressed ? 0 : 1
    }
    Image {
        id: buttonForegroundImagePressed
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        smooth: __smooth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? (0 + __pressedLeftShift) :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width - __pressedRightShift) / 2 :
                (parent.width - width)
           ) + __foregroundXShift
        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 + __pressedTopShift :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height - __pressedBottomShift) / 2 :
                (parent.height - height)
           ) + __foregroundYShift
        source: ""
        opacity: __pressed ? 1 : 0
    }



    Text {
        id: label
        objectName: "buttonLabel"
        enabled: button.enabled



        font.pointSize: {fontSize > 0 ? fontSize : 1}
        font.family: S.Style[__styleName].buttonFontFamily;
        anchors{
            fill: parent
            leftMargin: S.Style[__styleName].buttonTextBorderLeftSize + __leftTextMargin
            topMargin:  S.Style[__styleName].buttonTextBorderTopSize + __topTextMargin
            rightMargin: S.Style[__styleName].buttonTextBorderRightSize  + __rightTextMargin
            bottomMargin: S.Style[__styleName].buttonTextBorderBottomSize + 1 + __bottomTextMargin
        }

        horizontalAlignment: button.__horizontalTextAlignement
        verticalAlignment:  button.__verticalTextAlignement


        smooth: __smooth
        text: (__textIO !== undefined && button.text !== undefined) ? __getText() : qsTr(button.text)

        color: {
            if(!enabled){
                __fontColorDisabled
            }else{
                __fontColorNormal
            }
        }
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: label
                color: __fontColorPressed
                anchors.leftMargin: S.Style[__styleName].buttonTextBorderLeftSize + __leftTextMargin +__pressedLeftShift
                anchors.topMargin:  S.Style[__styleName].buttonTextBorderTopSize + __topTextMargin + __pressedRightShift
                anchors.rightMargin: S.Style[__styleName].buttonTextBorderRightSize + __rightTextMargin + __pressedTopShift
                anchors.bottomMargin: S.Style[__styleName].buttonTextBorderBottomSize + __bottomTextMargin + __pressedBottomShift
            }
        }
        transitions: Transition {
                ColorAnimation {
                target: label
                property: "color"

                duration: S.Style[__styleName].buttonTextAnimationTime
            }

        }
        clip: true
        elide: Text.ElideRight
    }





}
