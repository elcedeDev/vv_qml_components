// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S

FocusScope {
    id:root
    objectName: "AradexThemeShape"
    implicitHeight: S.Style.themeshapeHeightInitial
    implicitWidth: S.Style.themeshapeWidthInitial
//    height: S.Style.themeshapeHeightInitial
//    width: S.Style.themeshapeWidthInitial
    property bool hasImage: S.Style[__styleName].themeHasImage
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    property int colorChoice: 0
    property bool __smooth: S.isFast

    property variant themeImages: [S.Style[__styleName].themeImage0,
        S.Style[__styleName].themeImage1,
        S.Style[__styleName].themeImage2,
        S.Style[__styleName].themeImage3,
        S.Style[__styleName].themeImage4,
        S.Style[__styleName].themeImage5,
        S.Style[__styleName].themeImage6,
        S.Style[__styleName].themeImage7]

    property variant themeColors: [S.Style[__styleName].themeColor0,
        S.Style[__styleName].themeColor1,
        S.Style[__styleName].themeColor2,
        S.Style[__styleName].themeColor3,
        S.Style[__styleName].themeColor4,
        S.Style[__styleName].themeColor5,
        S.Style[__styleName].themeColor6,
        S.Style[__styleName].themeColor7]

    property string color: themeColors[colorChoice]


    BorderImage{
        id: image
        anchors.fill: parent
        smooth: __smooth
        border{
            left: S.Style[__styleName].themeBorderLeft
            right: S.Style[__styleName].themeBorderRight
            top: S.Style[__styleName].themeBorderTop
            bottom: S.Style[__styleName].themeBorderBottom
        }
        source: (themeImages[colorChoice] !== "" && hasImage) ? S.Style[__styleName].imageFolder + themeImages[colorChoice] : ""
    }
//    ThemeShapeElement{
//        color: (parent.color !== 0) ? parent.color : "#00000000"
//        opacity: (hasImage) ? 0 : 1
//        anchors.fill: parent

//    }



}
