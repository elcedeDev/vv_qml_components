// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1

Rectangle {
    id: root
    objectName: "AradexLabelList"
    property bool header: true
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    signal scaledSizeChanged(real scaleFactor)

    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    property int colorCombo: 0
    property int rows: 2
    property int space: S.Style.labelHeightInitial + 4
    property int leftRightMargin: S.Style[__styleName].labellistLeftRightMargin
    property int seperatorWidth: S.Style[__styleName].labellistSeperatorWidth
    clip:true
    height:  S.Style.labellistHeightInitial
    width:  S.Style.labellistWidthInitial

    color: "#00000000"
    property variant labelColors: [S.Style[__styleName].labelFontColor0,
        S.Style[__styleName].labelFontColor1,
        S.Style[__styleName].labelFontColor2,
        S.Style[__styleName].labelFontColor3]
    Repeater{
        id: seperators
        model: rows -1
        delegate:
            Rectangle{
                id: segmentImage
                x: leftRightMargin
                y: (index + 1) * space
                height: seperatorWidth
                width: root.width - 2 * leftRightMargin
                color: labelColors[colorCombo]


        }
    }

}
