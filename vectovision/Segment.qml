// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S

Rectangle {
    id: root
    objectName: "AradexSegment"
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    signal scaledSizeChanged(real scaleFactor)

    property bool header: true
    property int colorChoice: 0
    height:  S.Style.segmentHeightInitial
    width:  S.Style.segmentWidthInitial
    color: "#00000000"
    property variant segmentImages: [S.Style[__styleName].segmentImage0,
        S.Style[__styleName].segmentImage1,
        S.Style[__styleName].segmentImage2,
        S.Style[__styleName].segmentImage3,
        S.Style[__styleName].segmentImage4,
        S.Style[__styleName].segmentImage5,
        S.Style[__styleName].segmentImage6,
        S.Style[__styleName].segmentImage7]
        BorderImage{
            id: segmentImage
            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: S.Style[__styleName].segmentImageHeight

            source: (segmentImages[colorChoice] !== "") ? S.Style[__styleName].imageFolder + segmentImages[colorChoice] : ""
            border{
                left: S.Style[__styleName].segmentBorderLeft
                right: S.Style[__styleName].segmentBorderRight
                top: S.Style[__styleName].segmentBorderTop
                bottom: S.Style[__styleName].segmentBorderBottom
            }
        }
}
