// $Revision: 17918 $
/****************************************************************************
    **
    ** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
    ** All rights reserved.
    ** Contact: Nokia Corporation (qt-info@nokia.com)
    **
    ** This file is part of the Qt Components project.
    **
    ** $QT_BEGIN_LICENSE:BSD$
    ** You may use this file under the terms of the BSD license as follows:
    **
    ** "Redistribution and use in source and binary forms, with or without
    ** modification, are permitted provided that the following conditions are
    ** met:
    ** * Redistributions of source code must retain the above copyright
    ** notice, this list of conditions and the following disclaimer.
    ** * Redistributions in binary form must reproduce the above copyright
    ** notice, this list of conditions and the following disclaimer in
    ** the documentation and/or other materials provided with the
    ** distribution.
    ** * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
    ** the names of its contributors may be used to endorse or promote
    ** products derived from this software without specific prior written
    ** permission.
    **
    ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
    ** $QT_END_LICENSE$
    **
    ****************************************************************************/
import QtQuick 1.1
import "AppManager.js" as Utils
import "../../style/Style.js"  as S

Item {
    id: root
    // Force popup to be on top of any siblings
    z: Infinity
    property Item visualParent
    property variant status: S.DialogStatus.Closed
    property int __animationDuration: S.Style[__styleName].animationDuration
    property Item fader
    property bool __platformInverted: false
    signal faderClicked
    property string __styleName


    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    /*!
        \qmlsignal Button::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)

    onStyleChanged:{
        __styleName = strStyle
    }



    function open() {
        if (status == S.DialogStatus.Open || status == S.DialogStatus.Opening)
            return
        var notify = false
        if (!fader) {
            fader = faderComponent.createObject(visualParent ? visualParent : Utils.visualRoot())
            notify = true
        }
        fader.z = root.z
        fader.animationDuration = root.__animationDuration
        // Save original parent if not saved
        if (parentCache.oldParent == null)
            parentCache.oldParent = parent
        root.parent = fader
        status = S.DialogStatus.Opening
        fader.state = "Visible"
//        if (notify)
//            platformPopupManager.privateNotifyPopupOpen()
    }
    function close() {
        if (status != S.DialogStatus.Closed) {
            status = S.DialogStatus.Closing
            if (fader)
                fader.state = "Hidden"
        }
    }
    onStatusChanged: {
        if (status == S.DialogStatus.Closed && fader) {
            // Temporarily setting root window as parent
            // otherwise transition animation jams
            root.parent = null
            fader.destroy()
            fader = null // Prevent reuse in open()
            root.parent = parentCache.oldParent
        }
    }
    Component.onCompleted: {
        // Save original parent if not saved
        if (parentCache.oldParent == null)
            parentCache.oldParent = parent
    }
    //if this is not given, application may crash in some cases
    Component.onDestruction: {
        if (parentCache.oldParent != null) {
            parent = parentCache.oldParent
        }
    }
    QtObject {
        id: parentCache
        property QtObject oldParent: null
    }
    //This eats mouse events when popup area is clicked
    MouseArea {
        anchors.fill: parent
    }
    Component {
        id: faderComponent
        Fader {
            platformInverted: root.__platformInverted
            onClicked: root.faderClicked()
            popupRect: Qt.rect(root.x, root.y, root.width, root.height)
        }
    }
}
