// $Revision: 18858 $
// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../style/Style.js"  as S



FocusScope {
    id: errorspinner
    objectName: "AradexErrorSpinnerLanguage"

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFact)
    signal timerActivated()
    signal languageChanged()
    signal requestIoList(string pattern)
    signal ioAdded(variant io, string pattern)
    onIoAdded: {
        if(pattern.match(showErrors && __errorString)){
            ioList = ioList.concat([[io, 1]])
        }else if(pattern.match(showWarnings && __warningString)){
            ioList = ioList.concat([[io, 2]])
        }else if(pattern.match(showMessages && __messageString)){
            ioList = ioList.concat([[io, 3]])
        }
//        tmpstr = ""
//        for (var i=0; i<ioList.ioList.length; i++){
//            tmpstr.append(ioList[0].name)
//        }
        ioList = ioList.sort(function(a, b){ return a[1] - b[1]})

//      console.log(__errorString + "   add:    "/*+ io + "   List    " */+ ioList.length)
//      console.log("(index + 1):  " + (index + 1) + "    ioList.length:   " + ioList.length)
//      console.log(ioList.length+ "/" + ioListLength + "/" + errorListView.model + "/" +errorListView.opacity + "/" + errorListView.z)
    }

    signal ioRemoved(variant io, string pattern)
    onIoRemoved: {
        ioList = ioList.filter(function(x, index){
            if(x[0].name === io.name){
                if(errorListView.currentIndex == errorListView.count){
                    errorListView.currentIndex --
                }

                return false
            }else{
                return true
            }

        })
        ioList.sort(function(a, b){ return a[1] - b[1]})

    }

    property variant ioList: []

    // internal counter for triggering garbage collection
    property int __iGcCounter: 0



    onLanguageChanged: {
        var tmp = ioList
        ioList = []
        ioList = tmp
    }

    onTimerActivated: {
        if(showErrors) requestIoList(__errorString)
        if(showWarnings) requestIoList(__warningString)
        if(showMessages) requestIoList(__messageString)

        // trigger garbage collection because signal with string parameters
        // let slowly increase memory consumption
        __iGcCounter = __iGcCounter + 1
        if (__iGcCounter >= 1000) {
            gc();
            __iGcCounter = 0;
            //console.log("ErrorSpinner: triggering garbage collection")
        }
    }
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
        var tmp = ioList
        ioList = []
        ioList = tmp

    }
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    implicitWidth:  S.Style.ErrorWidgetWidthInitial
    implicitHeight:  S.Style.ErrorWidgetHeightInitial
    property bool __isFast: S.isFast
    property bool showErrors: false
    property bool showWarnings: false
    property bool showMessages: false
    property string __errorString: "iError"
    property string __warningString: "iWarning"
    property string __messageString: "iMessage"
    property variant errorModel: ermo
    property alias errorModelPath: errorModelLoader.source
    property QtObject ermo

    property string __backgroundImage: S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].errorwidgetBackgroundImage;


    property string __nextImage: (S.Style[__styleName].messageNextImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].messageNextImage : ""

    property string __previousImage: (S.Style[__styleName].messagePreviousImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].messagePreviousImage : ""

    property string __errorImage: (S.Style[__styleName].errorwidgetErrorImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].errorwidgetErrorImage : ""
    property string __warningImage: (S.Style[__styleName].errorwidgetWarningImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].errorwidgetWarningImage : ""
    property string __messageImage: (S.Style[__styleName].errorwidgetMessageImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                         S.Style[__styleName].errorwidgetMessageImage : ""


    // only IO containing ErrorString

    // List of Io Names coming from C++. Values are set once on startup
    property int __count: 0


    // integer Value of Io variable. Values update every <updateTime> ms
    property variant errorIoStates: []
    // List of all ListElements from ErrorListModel.qml that are present on control.
//    property variant errorIoValues: []//: errorIoList.map(function(x){return x.value})
    property variant errorIoValueMap
    // at first names and ErrorStates (0 = active, 1 = inactive ...) are combined
    // then only errors with state active are kept
    property int __errorCount: ioList.filter(function(x){return x[1] === 1 }).length
    property int __warningCount: ioList.filter(function(x){return x[1] === 2 }).length
    property int __messageCount: ioList.filter(function(x){return x[1] === 3 }).length



    Component.onCompleted: {
//        errorModel = ermo
        errorModelLoader.source = errorModelPath
        errorModel = errorModelLoader.item
        var tempMap = new Object();
        for (var i=0; i<errorModel.children.length; i++){
            tempMap[errorModel.children[i].name] = errorModel.children[i]
        }
        errorIoValueMap = tempMap        
        ioList = new Array()        
    }

    Component.onDestruction: {
        //ioList.destroy()
        //errorIoValueMap.destroy()
    }

    Loader{
        id: errorModelLoader
    }

    // overlay for Buttons and counters
    BorderImage {
        id: background
        anchors{
            top: parent.top
            bottom: parent.bottom
            left: notificationCounter.left
            right: parent.right
        }
        z: 1
        border{
            left: S.Style[__styleName].textfieldBorderLeftSize
            top: S.Style[__styleName].textfieldBorderTopSize
            right: S.Style[__styleName].textfieldBorderRightSize
            bottom: S.Style[__styleName].textfieldBorderBottomSize
        }
        source: __backgroundImage
    }
    Button{
        id: previousButton
        width: height
        height: (errorspinner.height)*0.8
        smooth: __isFast
        z:2
        anchors{
            top: errorspinner.top
            right: nextButton.left
            topMargin: height / 8
            leftMargin: width / 8
            rightMargin: width / 8
        }
        __imageNormal: __previousImage
        __imagePressed: __previousImage
        __buttonBorderLeftSize: 0
        __buttonBorderRightSize: 0
        __buttonBorderTopSize: 0
        __buttonBorderBottomSize: 0
        onClicked: {
            if(errorListView.currentIndex > 0){
                errorListView.currentIndex--
            }
//            console.log("clicked NEXT")
        }
    }
    Button{
        id: nextButton
        width: height
        height: (errorspinner.height)*0.8
        smooth: __isFast
        z:2
        anchors{
            top: errorspinner.top
            right: errorspinner.right
            topMargin: height / 8
            leftMargin: width / 8
            rightMargin: width / 8
        }
        __imageNormal: __nextImage
        __imagePressed: __nextImage
        __buttonBorderLeftSize: 0
        __buttonBorderRightSize: 0
        __buttonBorderTopSize: 0
        __buttonBorderBottomSize: 0
        onClicked: {
            if(errorListView.currentIndex < errorListView.count){
                errorListView.currentIndex++
            }
//            console.log("clicked NEXT")
        }
    }


Item{
    id: notificationCounter
    anchors{
        top: defaultBanner.top
        topMargin: defaultBanner.height / 16
        bottom: defaultBanner.bottom
        bottomMargin: defaultBanner.height / 16
        right: previousButton.left
        rightMargin: height/3
    }
    width: height
    z: 1
    MouseArea{
        id: errorArea
        anchors{
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: __errorCount > 0 ? parent.height/3 : 0
        onClicked: errorListView.currentIndex = 0
        Image{
            id: levelImage
            source: __errorImage
            smooth: __isFast
            width: height
            anchors{
                top: parent.top
                right: parent.right
                bottom: parent.bottom
            }
        }
        Text{
            id: levelImageLabel
            text: __errorCount
            horizontalAlignment: Qt.AlignRight
            font.pointSize: (height/1.5) > 0 ? (height/1.5) : 1
            color: S.Style[__styleName].errorWidgetFontColor
            clip: true
            width: height * 2
            anchors{
                top: parent.top
                right: levelImage.left
                rightMargin: parent.height / 4
                bottom: parent.bottom
            }
        }
    }
    MouseArea{
        id: warningArea

         anchors{
             top: errorArea.bottom
             left: parent.left
             right: parent.right
         }
         height: __warningCount > 0 ? parent.height/3 : 0
         onClicked: errorListView.currentIndex = __errorCount
         Image{
             id: levelImage2
             source: __warningImage
             smooth: __isFast
             width: height
             anchors{
                 top: parent.top
                 right: parent.right
                 bottom: parent.bottom
             }
         }
         Text{
             id: levelImageLabel2
             text: __warningCount
             horizontalAlignment: Qt.AlignRight
             font.pointSize: (height/1.5) > 0 ? (height/1.5) : 1
             color: S.Style[__styleName].errorWidgetFontColor
             clip: true
             width: height * 2
             anchors{
                 top: parent.top
                 right: levelImage2.left
                 rightMargin: parent.height / 4
                 bottom: parent.bottom
             }
         }

    }
    MouseArea{
        id: messageArea
         anchors{
             top: warningArea.bottom
             left: parent.left
             right: parent.right
         }
         height: __messageCount > 0 ? parent.height/3 : 0
         onClicked: errorListView.currentIndex = __errorCount + __warningCount
         Image{
             id: levelImage3
             source: __messageImage
             smooth: __isFast
             width: height
             anchors{
                 top: parent.top
                 right: parent.right
                 bottom: parent.bottom
             }
         }
         Text{
             id: levelImageLabel3
             text: __messageCount
             horizontalAlignment: Qt.AlignRight
             font.pointSize: (height/1.5) > 0 ? (height/1.5) : 1
             color: S.Style[__styleName].errorWidgetFontColor
             z:2
             clip: true
             width: height * 2
             anchors{
                 top: parent.top
                 right: levelImage3.left
                 rightMargin: parent.height / 4
                 bottom: parent.bottom
             }
         }

    }
}
    // Default Image if no Errors/Warnings/Messages are present
    ErrorWidget{
        id: defaultBanner
        anchors.fill: parent
        x: 0
        y: 0
        errtext: errorModel.children[1].text
        errperc: "0 / 0"
        errno: errorModel.children[1].num
        messageType: 3
        opacity: (ioList.length > 0) ? 0 : 1


    }

    ListView {
        id: errorListView
        anchors.fill: parent
        clip: true
        snapMode: ListView.SnapToItem
//        highlightMoveSpeed: 1
        model: ioList.length


        delegate: /*Rectangle{
          width: errorListView.width
          height: errorListView.height
          color: "transparent"
//              rotation: (index % 2) ? -10 : 10*/

          ErrorWidget{
            width: errorListView.width
            height: errorListView.height
            x: 0
            y: 0
            __styleName: errorspinner.__styleName
            errtext: errorIoValueMap[ioList[index][0].name] === undefined ? (ioList[index][0].name + ": " +errorModel.children[0].text) : errorIoValueMap[ioList[index][0].name].text
            errperc: (index + 1) + "/" + ioList.length
            errno: errorIoValueMap[ioList[index][0].name] === undefined ? errorModel.children[0].num : errorIoValueMap[ioList[index][0].name].num
            messageType: ioList[index][1]


          }
//            }
        onMovementEnded: {
            errorListView.currentIndex = errorListView.visibleArea.yPosition * errorListView.count
//            console.log("MOVED errorListView.currentIndex: " + errorListView.currentIndex + " / " + errorListView.visibleArea.yPosition)

        }

        // Workaround: Compute visibleArea
        Component.onCompleted: errorListView.visibleArea
    }

//    }
}
