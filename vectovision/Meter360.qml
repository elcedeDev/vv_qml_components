// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1

FocusScope {
    id: root
    objectName: "AradexMeter360"
    implicitHeight: S.Style.meter360HeightInitial
    implicitWidth: S.Style.meter360WidthInitial

    /*! \internal
        \qmlProperty string Meter360::__minText
        Minumum text.
    */
    property string __minText:(startStopLabel === true && Math.abs(__angleSpan === 360))?
                                  formatNumber((minValue),__scientificNumScale ,digitsAfterCommaScale).toString().replace(" ", "") +
                                  formatNumber((maxValue), __scientificNumScale,digitsAfterCommaScale).toString().replace(" ", "")  :
                                  formatNumber(minValue, __scientificNumScale, digitsAfterCommaScale)
    
	/*! \internal
        \qmlProperty string Meter360::__maxText
        Maximum text.
    */
    property string __maxText: formatNumber(maxValue, __scientificNumScale, digitsAfterCommaScale)

    property string __smooth: S.isFast

    /*! \internal
        \qmlProperty real Meter360::__maxLabelSizeAlt
         Upper estimate of the label width.
    */
    property real __maxLabelSizeAlt: Math.max(__minText.length, __maxText.length) * __fontPointSize * 0.70
    
	/*! \internal
        \qmlProperty real Meter360::__maxLabelSize
         Estimate of the label width.
    */
	property real __maxLabelSize: Math.max(__getValueLength(minValue), __getValueLength(maxValue)) * __fontPointSize * 0.70
    function __getValueLength(valueInput){
        var minInt = Math.floor(valueInput)
        var intLength = Math.max(1, Math.floor(Math.log(minInt)/Math.log(10)))
        // digits  befor comma  + comma + digits after comma
        return intLength + 1 + digitsAfterCommaScale
    }


    //
    //  Public Properties
    //

    /*!
        \qmlProperty bool Meter360::continuous

        \brief True if the meter is continuous.
        If set to true the meter will go if value exceeds maximum value.
        This will only make sense if the meter covers a span of 360°.

        The default value is \c false. */
    property bool continuous: false
    
	/*!
        \qmlProperty bool Meter360::animationOn

        \brief Animation to smooth the appearance of a value Change.
        If set to true the meter will display intermediate values on a value change
        instead of just jumping from value A to value B. There is also a dampening effect.

        The default value is \c false. */
    property bool animationOn: false
    
	/*!
        \qmlProperty bool Meter360::startStopLabel
        \brief If True first label
        The default value is \c false. */
    property bool startStopLabel: false
    
	/*!
        \qmlProperty int Meter360::digitsAfterCommaScale
        \brief Number of Ddecimal places for tick marks.
        Number of Ddecimal places for tick marks.
        The default value is \c 2. */
    property int digitsAfterCommaScale: 2
    
	/*!
        \qmlProperty int Meter360::digitsAfterCommaText
        \brief Number ofdecimal places for text.
        Number of Ddecimal places for text.
        The default value is \c 2. */
    property int digitsAfterCommaText: 2
    
	/*!
        \qmlProperty real Meter360::initialValue
        \brief Initial value.
        Initial Value. Is only relevant if no meterIO is set.
        The default value is \c 0. */
    property real initialValue: 0
    
	/*!
        \qmlProperty int Meter360::labelShift
        \brief Additional spacing for tick mark text in pixels.
        Additional spacing for tick mark text in pixels.
        The default value is \c S.Style[__styleName].meterScaleLabelShift . */
    property int __labelShift: S.Style[__styleName].meterScaleLabelShift
    
	/*!
        \qmlProperty real Meter360::stepSize
        \brief Scale major step size.
	    The default value is \c 1.  */
    property real stepSize: 1
    
	property real __bigScaleRadian: stepSize / __valueSpan * __radianSpan
	property real __smallScaleRadian: (stepSize/ (minorSteps + 1)) / __valueSpan * __radianSpan

    /*! \internal
        \qmlProperty bool Meter360::__turningScale
        \brief The scale turns instead of the clock hand.
        The default value is \c false. */
    property bool __turningScale: false
    
	/*! \internal
        \qmlProperty int Meter360::__overshootAnimationDuration
        \brief Duration of the overshoot animation in milliseconds.
        The default value is \c 200. */
    property int __overshootAnimationDuration: 200

    /*!
        \qmlProperty real Meter360::ioFactor
        \brief A factor the meterIO values will be multiplied with.
        The default value is \c 1.  */
    property real ioFactor: 1
    
	/*!
        \qmlProperty real Meter360::ioOffset
        \brief An offset that will be added to the meterIO value.
        The default value is \c 0.  */
	property real ioOffset: 0
    
	/*! \internal
        \qmlProperty int Meter360::__majorTickNum
        \brief Specifies the number of Major Tick Marks */
    property int __majorTickNum: Math.floor(Math.abs(__valueSpan/ stepSize))

    /*!
        \qmlProperty int Meter360::minorSteps
        \brief Specifies how many minor steps to put between two major steps.
        The default value is \c 3. */
    property int minorSteps: 3
    
	/*!
        \qmlProperty real Meter360::minValue
        \brief Minimum value of the scale.
        The default value is \c 0. */
    property real minValue: 0
    
	/*!
        \qmlProperty int Meter360::angleStart
        \brief The angle position of the first Tick mark.
        The angle position of the first major Tick mark, where 0 degree/ 360 degree would be
        twelfe o'clock and 180 degree equals 6 o'clock.
        The default value is \c -160. */
    property int angleStart: -160
    
	/*!
        \qmlProperty int Meter360::angleStop
        \brief The angle position of the first Tick mark.
        The angle position of the last major Tick mark, where 0 degree/ 360degree would be
        twelfe o'clock and 180 degree equals 6 o'clock.
        The default value is \c 160. */
    property int angleStop: 160
    
	/*! \internal
        \qmlProperty bool Meter360::__scientificNumScale
        \brief If true the major tick Labels will be given in scientific notation.
        The default value is \c false. */
    property bool __scientificNumScale: false
    
	/*! \internal
        \qmlProperty bool Meter360::__scientificNumText
        \brief If true the central label will be given in scientific notation.
        The default value is \c false. */
    property bool __scientificNumText: false
    
	/*! \internal
        \qmlProperty variant Meter360::value
        \brief The value the meter shows
        The default value is \c initialValue or the ioValue. */
    property variant value: (__valueIO !== undefined) ? ioOffset +(ioFactor *__valueIO.value) : initialValue
    
	/*! \internal
        \qmlProperty variant Meter360::__valueIO
        \brief The IO object whose value the meter shows.
        The default value is \c 0. */
    property variant __valueIO: eval(valueIO)
    
	/*!
        \qmlProperty string Meter360::valueIO
        \brief The name of an IO whose value the meter shows.
        The default value is \c '' */
	property string valueIO
    onValueIOChanged: {
        if (valueIO !== ""){
            __valueIO = eval(valueIO)
        }
    }

    //
    //  Images
    //

    /*! \internal
        \qmlProperty string Meter360::__scaleBigImage
        \brief ource of a big scale mark.
        The default value is \c S.Style[__styleName].scaleBigImage. */
    property string __scaleBigImage: (S.Style[__styleName].meterScaleBigImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].meterScaleBigImage : ""
    
	/*! \internal
        \qmlProperty string Meter360::__scaleSmallImage
        \brief ource of a small scale mark.
        The default value is \c S.Style[__styleName].scaleSmallImage. */
    property string __scaleSmallImage: (S.Style[__styleName].meterScaleSmallImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].meterScaleSmallImage : ""
    
	/*! \internal
        \qmlProperty string Meter360::__backgroundImage
        \brief ource of a background image.
        The default value is \c S.Style[__styleName].backgroundImage. */
    property string __backgroundImage: (S.Style[__styleName].meterBackgroundImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].meterBackgroundImage : ""
    
	/*! \internal
        \qmlProperty string Meter360::__overlayImage
        \brief ource of a overlay Image.
        The default value is \c S.Style[__styleName].overlayImage. */
    property string __overlayImage: (S.Style[__styleName].meterOverlayImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].meterOverlayImage : ""
    
	/*! \internal
        \qmlProperty string Meter360::handImage
        \brief ource of a clock hand.
        The default value is \c S.Style[__styleName].handImage. */
    property string __handImage: (S.Style[__styleName].meterHandImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].meterHandImage : ""

    //
    //  Private Properties
    //
    
	/*! \internal
        \qmlProperty Meter360::__actualSmallscaleCount
        \brief Number of small scales including invisible ones. */
    property int __actualSmallscaleCount: Math.min(Math.floor((minorSteps + 1)* (__valueSpan / stepSize)), (__majorTickNum + 1) * (minorSteps + 1))
    
	/*! \internal
        \qmlProperty Meter360::__startRadian
        \brief Radian value of first big scale mark. */
    property real __startRadian: (angleStart/360) * 2 * Math.PI
    
	/*!
        \qmlProperty Meter360::showCriticalZones
        \brief Enables displaying colored zones */
    property bool showCriticalZones: false
    property real greenBegin: 0
    property real greenEnd: 0
    property real yellowBegin: 0
    property real yellowEnd: 0
    property real redBegin: 0
    property real redEnd: 0

    property real __angleSpan: Math.abs(angleStop - angleStart)
    property real __radianSpan: __angleSpan/360 * 2*Math.PI
    property real __currentHandAngle: 0
    property real __valueSpan: maxValue - minValue
    property real maxValue: 6
    property string __fontFamily:S.Style[__styleName].meterFontFamily
    property string __fontColorMiddleText: S.Style[__styleName].meterFontColorMiddleText
    property string __fontColorLabelText: S.Style[__styleName].meterFontColorLabelText
    
	/*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change. */
    property string __styleName: S.styleName

    property int __fontPointSize: S.Style[__styleName].meterFontPointSize
     property bool __fontBold: S.Style[__styleName].meterFontBold

    property real __handAngle: angleStart + ((__shownValue - minValue)/__valueSpan * __angleSpan)
    property real __handRadian: __handAngle * Math.PI / 180

    property alias __scaleFactor: background.__scaleFactor

    property real __shownValue: (__tooSmall)? minValue : (__tooBig ? maxValue : Number(value))
    property bool __tooBig: stepSize > 0 ?
                                ((value > maxValue) && !continuous):
                                ((value < maxValue) && !continuous)
    property bool __tooSmall: stepSize > 0 ? value < minValue && !continuous : value > minValue && !continuous
    property bool __outOfRange: __tooBig || __tooSmall
    property bool __completed: false
    property int __valueTextPointSize: S.Style[__styleName].meterValueTextPointSize
    property real __valueTextVerticalOffsetFactor: S.Style[__styleName].meterTextVerticalOffsetFactor
	
	property int __maxMajorScaleStepsToDraw: Math.min(height, width) / 10 
	property int __maxMinorScaleStepsToDraw: Math.min(height, width) / 3

    /*!
        \qmlsignal Meter360::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents */
    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
    }
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    signal scaledSizeChanged(real scaleFactor)

    onValueChanged: {

        if(__tooBig === true && animationOn === true){
            overshootanimation.start()
        }else if(__tooSmall === true && animationOn === true){
            undershootanimation.start()
        }
    }
    function repeatString(num, stringToRepeat) {
        return new Array(isNaN(num)? 1 : ++num).join(stringToRepeat);
    }
    function formatNumber(num, scientific, digits){
        if(scientific === true){
            return Number(num).toExponential(digits)
        }else{
            return Number(num).toFixed(digits)
        }
    }

	Image {
		id: background
		property real __buffer: __maxLabelSize
		property real __edgeLength: Math.min(parent.width, parent.height) - (2* __buffer)
		height: __edgeLength
		width: __edgeLength
		property real __scaleFactor: width/sourceSize.width
		property real originX: __edgeLength / 2
		property real originY: __edgeLength / 2

		// bigscales
		property real __bigscaleWidth: S.Style[__styleName].meterBigScaleWidthFactor * __edgeLength
		property real __bigscaleHeight: S.Style[__styleName].meterBigScaleHeightFactor * __edgeLength
		property real __bigscaleRadius: S.Style[__styleName].meterBigScaleRadiusFactor * __edgeLength + __bigscaleHeight/2
		// smallscales
		property real __smallscaleWidth: S.Style[__styleName].meterSmallScaleWidthFactor * __edgeLength
		property real __smallscaleHeight: S.Style[__styleName].meterSmallScaleHeightFactor * __edgeLength
		property real __smallscaleRadius: S.Style[__styleName].meterSmallScaleRadiusFactor * __edgeLength + __smallscaleHeight/2
		// labels
		property real __actualRadius: (S.Style[__styleName].meterBigScaleRadiusFactor + S.Style[__styleName].meterBigScaleHeightFactor) * __edgeLength + __maxLabelSize * 0.5 + __labelShift
		// zones
		property real __zoneRadius: S.Style[__styleName].meterZoneRadiusFactor * __edgeLength

		anchors.centerIn: parent
		anchors.topMargin: __buffer
		anchors.leftMargin: __buffer
		anchors.rightMargin: __buffer
		anchors.bottomMargin:  __buffer

		smooth: __smooth
		source: __backgroundImage
		sourceSize{
			height: background.__edgeLength
			width: background.__edgeLength
		}


		PieChart {
			id: piechart
			anchors.centerIn: parent
			width: background.__zoneRadius * 2
			height: width
			opacity: showCriticalZones == true ? 1 : 0
			slices: [
				PieSlice {
					id: greenSlice
					anchors.fill: parent
					color: S.Style[__styleName].meterGreenZoneColor
					fromAngle: ((greenBegin - minValue) / __valueSpan * __angleSpan) + angleStart
					angleSpan: ((greenEnd - greenBegin) / __valueSpan * __angleSpan)
					diskWidth: S.Style[__styleName].meterGreenZoneWidthFactor * background.__edgeLength
					diskOpacity: S.Style[__styleName].meterZonesOpacity

				},
				PieSlice {
					id: yellowSlice
					anchors.fill: parent
					color: S.Style[__styleName].meterYellowZoneColor
					fromAngle: ((yellowBegin - minValue) / __valueSpan * __angleSpan) + angleStart
					angleSpan: ((yellowEnd - yellowBegin) / __valueSpan * __angleSpan)
					diskWidth: S.Style[__styleName].meterYellowZoneWidthFactor * background.__edgeLength
					diskOpacity:S.Style[__styleName].meterZonesOpacity
				},
				PieSlice {
					anchors.fill: parent
					color: S.Style[__styleName].meterRedZoneColor
					fromAngle: ((redBegin - minValue) / __valueSpan * __angleSpan) + angleStart
					angleSpan: ((redEnd - redBegin) / __valueSpan * __angleSpan)
					diskWidth: S.Style[__styleName].meterRedZoneWidthFactor * background.__edgeLength
					diskOpacity: S.Style[__styleName].meterZonesOpacity
				}
			]

	   }



		Text{
			id: shownText
			font.pointSize: __valueTextPointSize
			font.bold: true
			font.family: __fontFamily
			color: __fontColorMiddleText
			text: formatNumber(Number(value),__scientificNumText ,digitsAfterCommaText)
			horizontalAlignment: Text.AlignHCenter
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.top: parent.verticalCenter
			anchors.topMargin: __valueTextVerticalOffsetFactor * background.__edgeLength
		}

		Repeater {
			id: bigscales
			objectName: "Meter360Repeater"
			parent: background
			anchors.fill: parent
			model: Math.min(__majorTickNum + 1, __maxMajorScaleStepsToDraw)

			delegate: Image {
				property real radian: ((__turningScale === true) ? -__handRadian : 0 ) + index * __bigScaleRadian + __startRadian
				property real angle: radian * 360 /(2* Math.PI)
				width:  background.__bigscaleWidth
				height:  background.__bigscaleHeight
				source: __scaleBigImage
				y: background.originY - ( background.__bigscaleRadius * Math.cos(radian)) - height/2
				x: background.originX + ( background.__bigscaleRadius * Math.sin(radian)) - width/2
				rotation: angle
				transformOrigin: Item.Center
				opacity: 1
				smooth: __smooth
				sourceSize{
					height: height
					width: width 
				}
				asynchronous: false
			}
		}

		Repeater {
			id: smallscales
			model: Math.min(__actualSmallscaleCount, __maxMinorScaleStepsToDraw)
			parent: background
			anchors.fill: parent
			delegate: Image {
				property real radian: ((__turningScale === true) ? -__handRadian : 0 ) + index * __smallScaleRadian + __startRadian
				property real angle: radian * 180 /Math.PI
				width:  background.__smallscaleWidth
				height:  background.__smallscaleHeight
				opacity: (index%(minorSteps + 1)=== 0) ? 0 : 1
				source: __scaleSmallImage
				y: background.originY - ( background.__smallscaleRadius * Math.cos(radian)) - height/2
				x: background.originX + ( background.__smallscaleRadius * Math.sin(radian)) - width/2
				rotation: angle
				transformOrigin: Item.Center
				smooth: __smooth
				sourceSize{
					height: height
					width: width
				}
				asynchronous: false
			}
		}
		
		Repeater {
			id: scalelabels
			model: Math.min((__angleSpan === 360) ? __majorTickNum : __majorTickNum +1, __maxMajorScaleStepsToDraw)
			delegate: Text {
				id: tester
				parent: background
				property real radian: ((__turningScale === true) ? -__handRadian : 0 ) + (index * __bigScaleRadian)   + __startRadian
				property real angle: radian * 360 /(2* Math.PI)
				font.family: __fontFamily
				font.pointSize: __fontPointSize
				font.bold: __fontBold
				color: __fontColorLabelText
				y: background.originY - ( background.__actualRadius * Math.cos(radian)) - height/2
				x: background.originX + ( background.__actualRadius * Math.sin(radian)) - width/2
				opacity: 1
				text: {(startStopLabel === true && index == 0 && Math.abs(__angleSpan) === 360)?
						  formatNumber((minValue),__scientificNumScale ,digitsAfterCommaScale) + " / " +
						  formatNumber((maxValue), __scientificNumScale, digitsAfterCommaScale):
						   formatNumber((minValue + index * stepSize), __scientificNumScale, digitsAfterCommaScale).toString()
				}
			}

		}
		
		Image {
			id: meterHand
			property real handRadius: S.Style[__styleName].meterHandRadiusFactor * background.__edgeLength + height/2
			property real radian: ((__turningScale) ? 0 : __handAngle) * Math.PI / 180
			width: S.Style[__styleName].meterHandWidthFactor * background.__edgeLength
			height: S.Style[__styleName].meterHandHeightFactor * background.__edgeLength
			source: __handImage
			sourceSize{
				height: height
				width: width
			}
			y: background.originY - (handRadius * Math.cos(radian)) - height/2
			x: background.originX + (handRadius * Math.sin(radian)) - width/2
			rotation: (__turningScale) ? 0 : __handAngle
			transformOrigin: Item.Center
			smooth: S.isFast
		}
	}

	SequentialAnimation{
		id: overshootanimation

		NumberAnimation{
			target: root

			easing.type: Easing.InSine
			property: "__shownValue"
			duration: 200
			to: root.__shownValue * 1.005
		}
		NumberAnimation{
			target: root
			property: "__shownValue"
			easing.type: Easing.InSine
			duration: 200
			to: root.__shownValue
		}
	}
	
	SequentialAnimation{
		id: undershootanimation
		NumberAnimation{
			target: root
			easing.type: Easing.InSine
			property: "__shownValue"
			duration: 200
			to: root.__shownValue / 0.996
		}
		NumberAnimation{
			target: root

			property: "__shownValue"
			easing.type: Easing.InSine
			duration: 200
			to: root.__shownValue
		}
	}
	
	Behavior on __shownValue {
		enabled: (animationOn) ? true : false
		SpringAnimation {
			spring: 1.4
			damping: 0.15
		}
	}
}
