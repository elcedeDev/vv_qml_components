// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools.shortcut 1.1
import tools 1.1



/*!
    \qmltype Button
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A push button with a text label.

 */
FocusScope {
    id: button
    objectName: "AradexRadioButton"

    /*!
        \qmlProperty bool RadioButton::activeFocusOnPress

        Specifies whether the button should
        gain active focus when pressed.

        The default value is \c true. */
    property bool __activeFocusOnPress: false


//    property int testEnum: IoType.IOT_BOOLEAN
//    property variant textenmu: testEnum
    /*!
        \qmlProperty string RadioButton::changeTextTo
        The text to be displayed by target after button push.
        if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a text property
      */
    property string __changeTextTo

    /*!
        \qmlProperty string RadioButton::changeStateTo
        The state a target is changet to after a button push.
       if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a state property
      */
    property string onClicked_changeStateTo




    /*!
        \qmlProperty string RadioButton::fontColorNormal
        Font color of displayed Text, when button is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorNormal: S.Style[__styleName].radiobuttonFontColor
    /*!
        \qmlProperty string RadioButton::fontColorDisabled
        Font color of displayed Text, when button is disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorDisabled: S.Style[__styleName].radiobuttonFontDisabledColor
    /*!
        \qmlProperty string RadioButton::fontColorPressed
        Font color of displayed Text, while button is pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorPressed: S.Style[__styleName].radiobuttonFontPressedColor
    /*!
        \qmlProperty string RadioButton::fontColorPressed
        Font color of displayed Text, while button is pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorPressedAndDisabled: S.Style[__styleName].radiobuttonFontPressedAndDisabledColor

    /*!
        \qmlProperty string RadioButton::__foregroundImageNormal
        String that holds the URL of the foreground image.
        The default value is \c "".
      */
    property int __lightImageHeight: S.Style[__styleName].radiobuttonLightImageHeight
    /*!
        \qmlProperty int RadioButton::__foregroundImageWidth
        Width of an additional Image for the Button.
        The default value is \c 0.
      */
    property int __lightImageWidth: S.Style[__styleName].radiobuttonLightImageWidth

    /*!
        \qmlProperty bool RadioButton::__foregroundAnchorRight
        When \c true Additional Image is anchored right, otherwise
        it is anchored left.
        The default value is \c false.
      */
    property int __lightHorizontalAlignement: S.Style[__styleName].radiobuttonLightHorizontalAlignement
    /*!
        \qmlProperty bool RadioButton::__foregroundAnchorBottom
        When \c true Additional Image is anchored at the top, otherwise
        it is anchored on the bottom.
        The default value is \c false.
      */
    property int __lightVerticalAlignement: S.Style[__styleName].radiobuttonLightVerticalAlignement


    /*!
        \qmlProperty int RadioButton::__foregroundXShift
        \brief x offset
        Offset of the additional Imagen in x - direction
        The default value is \c 0.
      */
    property int __lightXShift: 0
    /*!
        \qmlProperty int RadioButton::__foregroundXShift
        \brief y offset
        Offset of the additional Imagen in y - direction
        The default value is \c 0.
      */
    property int __lightYShift: 0


    property string __lightImageOff: (S.Style[__styleName].radiobuttonLightImageOff !== "") ?
                                         S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonLightImageOff : ""


    /*!
        \qmlProperty string RadioButton::__foregroundImagePressed
        String that holds the URL of the foreground image when pressed.
        The default value is \c "".
      */
    property string __lightImageOn: (S.Style[__styleName].radiobuttonLightImageOn !== "") ?
                                        S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonLightImageOn : ""
    /*!
        \qmlProperty string RadioButton::__foregroundImageNormal
        String that holds the URL of the foreground image.
        The default value is \c "".
      */
    property alias foregroundImageNormal: buttonForegroundImage.source
    /*!
        \qmlProperty string RadioButton::__foregroundImagePressed
        String that holds the URL of the foreground image when pressed.
        The default value is \c "".
      */
    property alias foregroundImagePressed: buttonForegroundImagePressed.source

    /*!
        \qmlProperty int RadioButton::__foregroundImageHeight
        Height of an additional Image for the Button.
        The default value is \c 0.
      */

    property int __foregroundImageHeight: 0
    /*!
        \qmlProperty int RadioButton::__foregroundImageWidth
        Width of an additional Image for the Button.
        The default value is \c 0.
      */
    property int __foregroundImageWidth: 0

    /*!
        \qmlProperty bool RadioButton::__foregroundAnchorRight
        When \c true Additional Image is anchored right, otherwise
        it is anchored left.
        The default value is \c false.
      */
    property int __foregroundHorizontalAlignement: Qt.AlignHCenter
    /*!
        \qmlProperty bool RadioButton::__foregroundAnchorBottom
        When \c true Additional Image is anchored at the top, otherwise
        it is anchored on the bottom.
        The default value is \c false.
      */
    property int __foregroundVerticalAlignement: Qt.AlignHCenter
    /*!
        \qmlProperty bool RadioButton::__foregroundAnchorBottom
        When \c true Additional Image is anchored at the top, otherwise
        it is anchored on the bottom.
        The default value is \c false.
      */

    /*!
        \qmlProperty int RadioButton::__foregroundXShift
        \brief x offset
        Offset of the additional Imagen in x - direction
        The default value is \c 0.
      */
    property int __foregroundXShift: 0
    /*!
        \qmlProperty int RadioButton::__foregroundXShift
        \brief y offset
        Offset of the additional Imagen in y - direction
        The default value is \c 0.
      */
    property int __foregroundYShift: 0

    /*! \internal
        \qmlmethod RadioButton::__pressedLeftShift
        Change of BackgroundImage Size on pressed in x direction
    */
    property int __pressedLeftShift: S.Style[__styleName].radiobuttonPressedLeftShift

    /*! \internal
        \qmlmethod RadioButton::__pressedRightShift
        Change of BackgroundImage Size on pressed in Pixels in negative x- direction
    */
    property int __pressedRightShift: S.Style[__styleName].radiobuttonPressedRightShift
    /*! \internal
        \qmlmethod RadioButton::__pressedTopShift
        Change of BackgroundImage Size on pressed in Pixels in  y- direction
    */
    property int __pressedTopShift: S.Style[__styleName].radiobuttonPressedTopShift
    /*! \internal
        \qmlmethod RadioButton::__pressedBottomShift
        Change of BackgroundImage Size on pressed in Pixels in negative y- direction
    */
    property int __pressedBottomShift: S.Style[__styleName].radiobuttonPressedBottomShift


    /*!
        \qmlProperty string RadioButton::imageNormal
        Background image URL of button, when  active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageNormal: (S.Style[__styleName].radiobuttonImageNormal !== "") ?
                                     S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonImageNormal : ""

    /*! \qmlproperty bool TextField::__hovered

        This property holds whether the control is being hovered.
    */
    property alias __hovered: behavior.containsMouse

    /*!
        \qmlProperty string RadioButton::imageDisabled
        Background image URL of button, when  disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageDisabled: (S.Style[__styleName].radiobuttonImageDisabled !== "") ?
                                       S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonImageDisabled : ""
    /*!
        \qmlProperty string RadioButton::imagePressed
        Background image URL of button, while pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imagePressed: (S.Style[__styleName].radiobuttonImagePressed !== "") ?
                                      S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonImagePressed : ""
    /*!
        \qmlProperty string RadioButton::imagePressedAndDisabled
        Background image URL of button, while pressed and disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imagePressedAndDisabled: (S.Style[__styleName].radiobuttonImagePressedAndDisabled !== "") ?
                                                 S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonImagePressedAndDisabled: ""

    property string __focusImage: (S.Style[__styleName].radiobuttonImageFocus !== "") ?
                                    S.Style[__styleName].imageFolder + S.Style[__styleName].radiobuttonImageFocus : ""
    /*!
        \qmlProperty bool RadioButton::isEnabled
        Enables the button. Is used for intitial settings
        The default value is \c true.
      */
    property bool isEnabled:true
    onIsEnabledChanged: {
        enabled = isEnabled
    }
    enabled: (__enableIO === undefined) ? isEnabled: __enableIO.value


    /*!
        \qmlProperty bool RadioButton::isPressed
        Presses the button. Is used for intitial settings
        The default value is \c true.
      */
    property bool __isPressedInitially: false

    property bool __smooth: S.isFast

    /*!
        \qmlProperty bool RadioButton::keyBinding
        Button can get a hotkey like F1 - F12 for different key values
        use \l Qt::KeyHotLinks and insert values without "Qt::Key_SysReq"
        For example:
        If you want to use page down as a hotkey, the list shows
        "Qt::Key_PageDown" and you need to insert "PageDown"
      */
    property string keyBinding
    /*!
        \qmlProperty string ToggleButton::__leftTextMargin
        \brief Left margin of text on top of image margin:
        Additional left Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.leftTextMargin.
      */
    property int __leftTextMargin: S.Style[__styleName].radiobuttonTextLeftMargin
    /*!
        \qmlProperty string ToggleButton::__rightTextMargin
        \brief Right margin of text on top of image margin:
        Additional rightMargin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.rightTextMargin.
      */
    property int __rightTextMargin: S.Style[__styleName].radiobuttonTextRightMargin
    /*!
        \qmlProperty string ToggleButton::__topTextMargin
        \brief Top margin of text on top of image margin:
        Additional top Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.topTextMargin.
      */
    property int __topTextMargin: S.Style[__styleName].radiobuttonTextTopMargin
    /*!
        \qmlProperty string ToggleButton::__bottomTextMargin
        \brief Bottom margin of text on top of image margin:
        Additional bottom Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.bottomTextMargin.
      */
    property int __bottomTextMargin: S.Style[__styleName].radiobuttonTextBottomMargin

    /*!
        \qmlProperty bool RadioButton::sendToComponentOutOfScope
        If you want to send an Object to a Component out of Scope,
        you have to check this flag and give your target an objName
        The default value is \c false.*/
    property bool __sendToComponentOutOfScope: false


    /*!
        \qmlProperty string RadioButton::text
        This property holds the text shown on the button. If the button has no
        text, the \l text property will be an empty string.

        The default value is the empty string.
    */
    property string text

    property string textIO
    /*!
        \qmlProperty string RadioButton::__textIO
        Connects the text shown on the button to an IO value.

        The default value is the empty string.
    */
    property variant __textIO: eval(textIO)
    onTextIOChanged: {
        if (textIO !== ""){
            __textIO = eval(textIO)
        }
    }

//    property
    /*!
        \qmlProperty string RadioButton::buttonRadioIO
        A string that gives the name of an IO. The IO must be a bool.
        The state of the Toggle Button will pressed(true)/not pressed(false)
        will always be synchronized with the io.
    */
    property variant __stateIO: parent.__stateIO
    property variant buttonRadioIOValue: (__stateIO !== undefined) ?  __stateIO.value : 0
    onButtonRadioIOValueChanged: {
        if(__stateIO !== undefined){
            if (String(buttonRadioIOValue) === onClicked_changeStateTo){
                button.parent.initialState =  __stateIO.value
                changeState()
            }
        }
    }

    /*!
        \qmlProperty string RadioButton::buttonStateIO
        A string that gives the name of an IO. The IO must be a bool.
        The enabled status of the Toggle Button will enabled(true)/not disabled(false)
        will always be synchronized with the io.
    */
    property variant __enableIO: eval(enableIO)
    property string enableIO
    onEnableIOChanged: {
        if (enableIO !== ""){
            __enableIO = eval(enableIO)
            enabled = __enableIO.value
        }

    }
    /*!
        \qmlProperty string RadioButton::unit
        A string that is put behind the IO values shown on the button.
        The default value is the empty string.
    */
    property string __unit
    /*!
        \qmlProperty bool RadioButton::unit
        Whether to put a unit behind the IOString. If unit is given it will
        be used. Otherwise the unit specified for the IO in V8 is used
    */
    property bool __showUnit: false

    /*!
        \qmlProperty int RadioButton::digitsAfterComma
        The number of digits behind the decimal mark
    */
    property int __digitsAfterComma:2




    property int fontSize: S.Style[__styleName].radiobuttonFontSize

    /*!
        \qmlproperty enumeration TextField::verticalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignTop
        \li Text.AlignBottom
        \li Text.AlignVCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __verticalTextAlignement: S.Style[__styleName].radiobuttonVerticalAlignement

    /*!
        \qmlproperty enumeration TextField::horizontalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignLeft
        \li Text.AlignRight
        \li Text.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __horizontalTextAlignement: S.Style[__styleName].radiobuttonHorizontalAlignement

//    /*!
//        \qmlproperty enumeration TextField::buttonNumber
//        \brief Number for RadioGroup

//        Int Number that is passed to radioVariable of RadioGroup to identify this
//        specific radio button.
//        */
//    property int buttonNumber: 0


    /*!
     \qmlsignal RadioButton::clicked()
     This signal is emitted when the user clicks the button. A click is defined
     as a press followed by a release. The corresponding handler is
     \c onClicked.
     */
    signal clicked()
    /*!
        \qmlsignal RadioButton::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)
    /*!
        \qmlsignal RadioButton::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*
        \qmlsignal TextField::outOfScopeStateChange(string objName, string stateName)

        If a state change is directed at a component out of change a signal
        containing \a objName and \a stateName has to be send to c++ main.
      */
    signal outOfScopeStateChange(string objName, string stateName)
    /*
        \qmlsignal TextField::outOfScopeTextChange(string objName, string stateName)

        If a text change is directed at a component out of change a signal
        containing \a objName and \a newText has to be send to c++ main.
      */
    signal outOfScopeTextChange(string objName, string newText)

    signal scaledSizeChanged(real scaleFactor)

    /*! \internal
        \qmlmethod Button::getText()
        Takes the value of the IO connected to the text on the button and formats it
        appropriately, by reducig digits.

    */
    function __getText(){
        var text = __textIO.value
        switch (__textIO.type){
        case IoType.IOT_DOUBLE:
            text = Number(text).toFixed(__digitsAfterComma)
            break;
        case IoType.IOT_INTEGER:
            text = Number(text).toFixed(__digitsAfterComma)
            break;
        default:
            text = String(text)

        }

        return text
    }

// TODO ANPASSEN!!!
    /*!
        \qmlmethod RadioButton::changeState()
        Behaviour when Button is Clicked including change of appearance
        and changeTextTo/changeStateTo Actions.

    */
    function changeState(){
        button.clicked()
		if( __target === undefined )
		{
			__target = parent
		}

if (__sendToComponentOutOfScope == true){
            if (onClicked_changeStateTo != ""){
                outOfScopeStateChange(__target.state, onClicked_changeStateTo)
            }
            if (__changeTextTo != ""){
                outOfScopeTextChange(__target.state, __changeTextTo)
            }
        }else{
            if (onClicked_changeStateTo != ""){
                __target.state = onClicked_changeStateTo
            }
            if (__changeTextTo != ""){
                __target.text = __changeTextTo
            }
        }
    }
    /*!
        \qmlmethod RadioButton::pressedAction()
        Behaviour when Button is pressed including change of appearance.


    */

    function pressedAction(){
        if(__stateIO !== undefined){
            switch (__stateIO.type){
                case IoType.IOT_INTEGER:{
                    if(!isNaN(buttonRadioIOValue)){
                        __stateIO.value =  parseInt(buttonRadioIOValue)
                    }
                    break;
                }
                case IoType.IOT_STRING:{
                    __stateIO.value =  buttonRadioIOValue
                    break;
                }
            }
        }
        if (enabled) changeState()
        if(button.parent.initialState !== undefined) button.parent.initialState = onClicked_changeStateTo
    }


    /*! \internal
        The corresponding Qt.Key to the Keey binding input.
    */
    property variant shortKey: eval("Qt.Key_" + keyBinding)

    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    property bool __pressed: parent.initialState === undefined ? false : (parent.initialState === onClicked_changeStateTo) ? true: false

    /*!
        \internal
        \qmlProperty string RadioButton::__target
        QML target Object for changeTextTo/changeStateTo.
        Is initialized with target
      */
    property variant __target: eval(parent.stateTarget)
//    property variant __target: (parent.stateTarget !== "") ? eval(parent.stateTarget) : eval(stateTarget)


    implicitWidth:  S.Style.radiobuttonWidthInitial
    implicitHeight:  S.Style.radiobuttonHeightInitial
    clip: false


    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


//    onIsPressedInitiallyChanged: __pressed = __isPressedInitially

    Shortcut {
        key: shortKey
        onActivated: {
            if(button.enabled){
                if(__activeFocusOnPress){
                     button.forceActiveFocus()
                }
                pressedAction()
            }
        }
    }



    MouseArea {
        id: behavior
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            if(button.enabled){
                pressedAction()
            }
        }
        onClicked: {
            if(button.enabled){
                button.forceActiveFocus()
            }
        }
    }

    // no dynamic loading since that causes problems in editor
    BorderImage {
        id: buttonImage
        anchors.fill: button
        border{
            left: S.Style[__styleName].radiobuttonBorderLeftSize
            top: S.Style[__styleName].radiobuttonBorderTopSize
            right: S.Style[__styleName].radiobuttonBorderRightSize
            bottom: S.Style[__styleName].radiobuttonBorderBottomSize
        }
        source: __imageNormal
        visible: (enabled)
        smooth: __smooth
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: buttonImage
                source : __imagePressed

                anchors.leftMargin: __pressedLeftShift
                anchors.rightMargin: __pressedRightShift
                anchors.topMargin: __pressedTopShift
                anchors.bottomMargin: __pressedBottomShift

            }

        }
        transitions: Transition {
            NumberAnimation {
                properties: "anchors.leftMargin, anchors.rightMargin, anchors.topMargin,anchors.bottomMargin"
                easing.type: Easing.Linear
                duration: S.isFast ?  S.Style[__styleName].radiobuttonSizeAnimationTime : 0
            }
        }
        BorderImage {
            id: focus
            opacity:  (button.activeFocus) ? S.Style[__styleName].focusOpacity : 0
            anchors{
                fill: parent
                leftMargin: S.Style[__styleName]. radiobuttonFocusShiftLeft
                topMargin:  S.Style[__styleName]. radiobuttonFocusShiftRight
                rightMargin: S.Style[__styleName]. radiobuttonFocusShiftTop
                bottomMargin: S.Style[__styleName]. radiobuttonFocusShiftBottom
            }
            border{
                left: S.Style[__styleName]. radiobuttonFocusBorderLeft
                right: S.Style[__styleName]. radiobuttonFocusBorderRight
                top: S.Style[__styleName]. radiobuttonFocusBorderTop
                bottom: S.Style[__styleName]. radiobuttonFocusBorderBottom
            }
            smooth: __smooth

            source: __focusImage
        }
    }

    // no dynamic loading since that causes problems in editor
    BorderImage {
        id: buttonImageDisabled
        anchors.fill: button
        border{
            left: S.Style[__styleName].radiobuttonBorderLeftSize
            top: S.Style[__styleName].radiobuttonBorderTopSize
            right: S.Style[__styleName].radiobuttonBorderRightSize
            bottom: S.Style[__styleName].radiobuttonBorderBottomSize
        }
        source: __imageDisabled
        visible: (!enabled)
        smooth: __smooth
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: buttonImageDisabled
                source : __imagePressedAndDisabled

                anchors.leftMargin: __pressedLeftShift
                anchors.rightMargin: __pressedRightShift
                anchors.topMargin: __pressedTopShift
                anchors.bottomMargin: __pressedBottomShift

            }

        }
        transitions: Transition {
            NumberAnimation {
                properties: "anchors.leftMargin, anchors.rightMargin, anchors.topMargin,anchors.bottomMargin"
                easing.type: Easing.Linear
                duration: S.isFast ?  S.Style[__styleName].radiobuttonSizeAnimationTime : 0
            }
        }
    }

    Image {
        id: buttonForegroundImage
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        smooth: __smooth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? 0 :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width) / 2 :
                (parent.width - width)
            ) + __foregroundXShift

        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height) / 2 :
                (parent.height - height)
            )+ __foregroundYShift
        opacity: __pressed ? 0 : 1
    }
    Image {
        id: buttonForegroundImagePressed
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        smooth: __smooth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? (0 + __pressedLeftShift) :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width - __pressedRightShift) / 2 :
                (parent.width - width)
           ) + __foregroundXShift
        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 + __pressedTopShift :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height - __pressedBottomShift) / 2 :
                (parent.height - height)
           ) + __foregroundYShift
        opacity: __pressed ? 1 : 0
    }
    Image {
        id: lightImage
        height: button.__lightImageHeight
        width :button.__lightImageWidth
        smooth: __smooth
        // calculate Image Position for different Alignement settings
        x: ((__lightHorizontalAlignement === Qt.AlignLeft) ? 0 :
                (__lightHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width) / 2 :
                (parent.width - width)
           ) + __lightXShift
        y: ((__lightVerticalAlignement === Qt.AlignTop) ? 0 :
                (__lightVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height) / 2 :
                (parent.height - height)
           ) + __lightYShift

        source: __lightImageOff
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: lightImage
                source : __lightImageOn
                x: ((__lightHorizontalAlignement === Qt.AlignLeft) ? 0  + __pressedLeftShift :
                        (__lightHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width) / 2 :
                        (parent.width - width - __pressedRightShift)
                   ) + __lightXShift
                y: ((__lightVerticalAlignement === Qt.AlignTop) ? 0 + __pressedTopShift :
                        (__lightVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height) / 2 :
                        (parent.height - height- __pressedBottomShift)
                   ) + __lightYShift
            }
        }
    }

    Text {
        id: label
        objectName: "buttonLabel"
//        property bool buttonStateIOValue: (__enableIO !== "") ?  eval(__enableIO + ".value") : false//enabled

//        property string buttonTextIOValue: (__textIO !== "") ?  eval(__textIO + ".value") : false //""

//        property string buttonRadioIOValue: (__radioIO !== "") ?  eval(__radioIO + ".value") : 0 //__pressed
//        onButtonStateIOValueChanged: {
//            if(__enableIO !== ""){
//                button.isEnabled = buttonStateIOValue
//            }
//        }
//        onButtonRadioIOValueChanged: {
//            if(__radioIO !== ""){
//                if (buttonRadioIOValue === changeStateTo){
//                    parent.parent.initialState =  eval(__radioIO + ".value")
//                    changeState()
//                }
//            }

//        }

        signal languageChanged()
        //font.pointSize: fontSize
        font.pointSize: {button.fontSize > 0 ? button.fontSize : 1}
        font.family: S.Style[__styleName].buttonFontFamily;
        anchors{
            fill: parent
            leftMargin: S.Style[__styleName].radiobuttonTextBorderLeftSize + __leftTextMargin
            rightMargin: S.Style[__styleName].radiobuttonTextBorderRightSize + __rightTextMargin
            topMargin:  S.Style[__styleName].radiobuttonTextBorderTopSize + __topTextMargin
            bottomMargin: S.Style[__styleName].radiobuttonTextBorderBottomSize + __bottomTextMargin
        }

        horizontalAlignment: button.__horizontalTextAlignement
        verticalAlignment:  button.__verticalTextAlignement

        smooth: S.isFast
        text: (__textIO !== undefined) ? __getText() : qsTr(button.text)
        onLanguageChanged: {
            text = (__textIO != "") ? __getText() : qsTr(button.text)
        }
        color: {

            if(button.enabled){
                 __fontColorNormal
            }else{
               __fontColorDisabled
            }
        }
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: label
                color: {
                    if(!button.enabled){
                        __fontColorPressedAndDisabled
                    }else{
                        __fontColorPressed

                    }
                }
                anchors.leftMargin: S.Style[__styleName].togglebuttonTextBorderLeftSize + __leftTextMargin +__pressedLeftShift
                anchors.rightMargin: S.Style[__styleName].togglebuttonTextBorderRightSize + __rightTextMargin + __pressedRightShift
                anchors.topMargin:  S.Style[__styleName].togglebuttonTextBorderTopSize + __topTextMargin + __pressedTopShift
                anchors.bottomMargin: S.Style[__styleName].togglebuttonTextBorderBottomSize + __bottomTextMargin + __pressedBottomShift



            }
        }
        clip: true
        elide: Text.ElideRight


    }

}
