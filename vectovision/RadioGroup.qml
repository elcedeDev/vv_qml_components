// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S

Item {
    id: radioGroup
    
	objectName: "AradexRadioGroup"
    
	property string __styleName: S.styleName
    
	signal styleChanged(string strStyle)
    
	onStyleChanged:{
        __styleName = strStyle
    }
    
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

	signal scaledSizeChanged(real scaleFactor)
    
	implicitWidth:  S.Style.radiogroupWidthInitial
    
	implicitHeight:   S.Style.radiogroupHeightInitial
    
	/*!
        \qmlProperty string RadioGroup::initialState
        \brief Initial State as well as current state.
        State the currently pressed radiobutton has selected. Is also used
        to set the Initial state. */
    property string initialState
    
	/*!
        \qmlProperty string RadioGroup::statetarget
        \brief Target name.
        The object name of the QML Object whose states will be changed when
        different radio buttons are selected.*/
    property string stateTarget
	
	
    property variant __stateIO: eval(stateIO)
	
	/*!
        \qmlProperty string RadioGroup::stateIo
        A string that gives the name of an IO. The IO must be a String or an Int.
        The state of the Toggle Button that is pressed(true)/not pressed(false)
        will always be synchronized with the io.
    */
    property string stateIO
	
    onStateIOChanged: {
        if (stateIO !== undefined && stateIO !== "") {
            __stateIO = eval(stateIO)
        }
    }
	
    Component.onCompleted: {
		if (stateTarget === "" ) {
			state = initialState
		}
		else {
			eval(stateTarget + ".state = \"" + initialState + "\"")
		}
    }
	
	onStateChanged: {
		 if (__stateIO !== undefined) {
			__stateIO.value = state
		}
	}
}
