// $Revision: 18441 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1


/*!
    \qmltype TextField
    \inqmlmodule ???
    \since ???
    \ingroup ???
    \brief Displays a single line of editable plain text.

    TextField is used to accept a line of text input. Input constraints can be
    placed on a TextField item (for example, through a \l validator or \l
    inputMask). Setting \l echoMode to an appropriate value enables
    TextField to be used for a password input field.

*/

FocusScope {
    id: textfield
    objectName: "AradexTextField"

    property bool __doNotAccept: false

    property int __contentHeight: textEditComponent.height
    /*!
        \qmlproperty bool TextField::activeFocusOnPress

        This property is set to \c true if the TextField should gain active
        focus on a mouse press.

        The default value is \c true.
    */
    property alias __activeFocusOnPress: textEditComponent.activeFocusOnPress

    property alias wrapMode: textEditComponent.wrapMode



    /*!
        \qmlProperty string TextField::textfieldFontSize
        PixelSize of displayed Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property alias fontSize: textEditComponent.fontSize


    /*! \internal
        Array holding all Text colors for the Label.
    */
    property variant __fontColors: [S.Style[__styleName].multiFontColor0,
        S.Style[__styleName].multiFontColor1,
        S.Style[__styleName].multiFontColor2,
        S.Style[__styleName].multiFontColor3]
    /*!
        \qmlProperty string TextField::fontColorNormal
        Font color of displayed Text, when TextField is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontColorChoice: 0

    /*! \internal
        Array holding all Text colors for the Label.
    */
    property variant __fontPlaceholderColors: [S.Style[__styleName].multiPlaceholderColor0,
        S.Style[__styleName].multiPlaceholderColor1,
        S.Style[__styleName].multiPlaceholderColor2,
        S.Style[__styleName].multiPlaceholderColor3]
    /*!
        \qmlProperty string TextField::fontPlaceholderNormal
        Font color of displayed placeholder Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontPlaceholderColorChoice: 0

    /*!
        \qmlProperty string TextField::fontPlaceholderColor
        Font color of displayed Text when Textfield is inactive. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorInactive: S.Style[__styleName].multiFontInactiveColor

    /*!
        \qmlProperty string TextField::fontReadOnlyColor
        Font color of displayed Text when Textfield is in readOnly mode. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorReadOnly: __fontColors[fontColorChoice]//S.Style[__styleName].multiFontReadOnlyColor


    /*!
        \qmlProperty string Button::imageNormal
        Background image URL of button, when  active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageEnabled: S.Style[__styleName].imageFolder +
                                  S.Style[__styleName].multiImageEnabled
    /*!
        \qmlProperty string Button::imageDisabled
        Background image URL of button, when  disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageDisabled: S.Style[__styleName].imageFolder +
                                   S.Style[__styleName].multiImageDisabled

    /*!
        \qmlProperty string Button::imagePressed
        Background image URL of button, while pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageFocus: S.Style[__styleName].imageFolder +
                                S.Style[__styleName].multiImageFocus

    /*!
        \qmlProperty string Button::imageReadOnly
        Background image URL of button, while in ReadOnly mode. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */

    property string __imageReadOnly: S.Style[__styleName].imageFolder +
                                   S.Style[__styleName].multiImageReadOnly


    /*!
       \qmlproperty string TextField::text

       This property holds the text displayed in the TextField.

       If \l echoMode is set to TextInput::Normal, this holds the
       same value as the TextField::text property. Otherwise,
       this property holds the text visible to the user, while
       the \l text property holds the actual entered text.
    */
    property string text


    /*!
        \qmlproperty font TextField::font.family

        Sets the font of the TextField.
    */
    property alias __fontFamily: textEditComponent.font.family

    /*!

        Sets an input mask on the TextField, restricting the allowable text
        inputs. See QLineEdit::inputMask for further details, as the exact same
        mask strings are used by TextField.

        Character	Meaning
        A	ASCII alphabetic character required. A-Z, a-z.
        a	ASCII alphabetic character permitted but not required.
        N	ASCII alphanumeric character required. A-Z, a-z, 0-9.
        n	ASCII alphanumeric character permitted but not required.
        X	Any character required.
        x	Any character permitted but not required.
        9	ASCII digit required. 0-9.
        0	ASCII digit permitted but not required.
        D	ASCII digit required. 1-9.
        d	ASCII digit permitted but not required (1-9).
        #	ASCII digit or plus/minus sign permitted but not required.
        H	Hexadecimal character required. A-F, a-f, 0-9.
        h	Hexadecimal character permitted but not required.
        B	Binary character required. 0-1.
        b	Binary character permitted but not required.
        >	All following alphabetic characters are uppercased.
        <	All following alphabetic characters are lowercased.
        !	Switch off case conversion.
        \	Use \ to escape the special characters listed above to use them as separators.

        The mask consists of a string of mask characters and separators, optionally followed by a semicolon and the character used for blanks. The blank characters are always removed from the text after editing.

        Examples:
        Mask	Notes
        000.000.000.000;_	IP address; blanks are _.
        HH:HH:HH:HH:HH:HH;_	MAC address
        0000-00-00	ISO Date; blanks are space
        >AAAAA-AAAAA-AAAAA-AAAAA-AAAAA;#	License number; blanks are - and all (alphabetic) characters are converted to uppercase.

        \sa acceptableInput, validator
    */
//    property alias inputMask: textEdit.inputMask

    /*!
        \qmlproperty enumeration TextField::inputMethodHints

        Provides hints to the input method about the expected content of the
        text field and how it should operate.

        The value is a bit-wise combination of flags, or \c Qt.ImhNone if no
        hints are set.

        The default value is \c Qt.ImhNone.

        Flags that alter behavior are:

        \list
        \li Qt.ImhHiddenText - Characters should be hidden, as is typically used when entering passwords.
                This is automatically set when setting echoMode to \c TextInput.Password.
        \li Qt.ImhSensitiveData - Typed text should not be stored by the active input method
                in any persistent storage like predictive user dictionary.
        \li Qt.ImhNoAutoUppercase - The input method should not try to automatically switch to upper case
                when a sentence ends.
        \li Qt.ImhPreferNumbers - Numbers are preferred (but not required).
        \li Qt.ImhPreferUppercase - Uppercase letters are preferred (but not required).
        \li Qt.ImhPreferLowercase - Lowercase letters are preferred (but not required).
        \li Qt.ImhNoPredictiveText - Do not use predictive text (for example, dictionary lookup) while typing.

        \li Qt.ImhDate - The text editor functions as a date field.
        \li Qt.ImhTime - The text editor functions as a time field.
        \endlist

        Flags that restrict input (exclusive flags) are:

        \list
        \li Qt.ImhDigitsOnly - Only12igits are allowed.
        \li Qt.ImhFormattedNumbersOnly - Only number input is allowed. This includes decimal point and minus sign.
        \li Qt.ImhUppercaseOnly - Only uppercase letter input is allowed.
        \li Qt.ImhLowercaseOnly - Only lowercase letter input is allowed.
        \li Qt.ImhDialableCharactersOnly - Only characters suitable for phone dialing are allowed.
        \li Qt.ImhEmailCharactersOnly - Only characters suitable for email addresses are allowed.
        \li Qt.ImhUrlCharactersOnly - Only characters suitable for URLs are allowed.
        \endlist

        Masks:
        \list
        \li Qt.ImhExclusiveInputMask - This mask yields nonzero if any of the exclusive flags are used.
        \endlist
    */
//    property int inputMethodHints: Qt.ImhDigitsOnly


    /*!
        \qmlproperty int TextField::maximumLength

        This property holds the maximum permitted length of the text in the
        TextField.

        If the text is too long, it is truncated at the limit.
    */
//    property alias maximumLength: textEdit.maximumLength

    /*!
        \qmlproperty int TextField::isEnabled

        This property enables and disables the component.

        The default value is \c true.
    */
    property bool isEnabled: true
//    // before disabling cursor has to be unset and text deselected
//    onIsEnabledChanged: {

//    }

    /*!
        \qmlproperty string TextField::placeholderText

        This property contains the text that is shown in the text field when the
        text field is empty and has no focus.
    */
    property alias placeholderText: placeholderTextComponent.text

    /*!
        \qmlproperty string TextField::placeholderText

        This property contains the size of the placeholder Text.
    */
    property alias placeholderTextSize: placeholderTextComponent.textSize

    /*!
        \qmlproperty bool TextField::readOnly

        Sets whether user input can modify the contents of the TextField. Read-
        only is different from a disabled text field in that the text field will
        appear to be active and text can still be selected and copied.

        If readOnly is set to \c true, then user input will not affect the text.
        Any bindings or attempts to set the text property will still
        work, however.
    */
    property alias readOnly: textEditComponent.readOnly


    /*!
        \qmlProperty string Button::unit
        A string that is put behind the IO values shown on the button.
        The default value is the empty string.
    */
    property string __unit
    /*!
        \qmlproperty Validator TextField::validator

        Allows you to set a validator on the TextField. When a validator is set,
        the TextField will only accept input which leaves the text property in
        an intermediate state. The accepted signal will only be sent
        if the text is in an acceptable state when enter is pressed.

        Currently supported validators are \l{QtQuick2::IntValidator},
        \l{QtQuick2::DoubleValidator}, and \l{QtQuick2::RegExpValidator}. An
        example of using validators is shown below, which allows input of
        integers between 11 and 31 into the text input:

        \code
        import QtQuick 2.1
        import QtQuick.Controls 1.0

        TextField {
            validator: IntValidator {bottom: 11; top: 31;}
            focus: true
        }
        \endcode

        \sa acceptableInput, inputMask, accepted
    */
//    property alias validator: textEdit.validator

    /*!
        \qmlproperty enumeration TextField::horizontalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li TextInput.AlignLeft
        \li TextInput.AlignRight
        \li TextInput.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int horizontalTextAlignement: S.Style[__styleName].multiHorizontalAlignement

    /*!
        \qmlsignal TextField::accepted()

        This signal is emitted when the Return or Enter key is pressed.
        Note that if there is a \l validator or \l inputMask set on the text
        field, the signal will only be emitted if the input is in an acceptable
        state.
    */
    signal accepted()

    signal clicked()
    /*!
        \qmlsignal TextField::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)
    /*!
        \qmlsignal TextField::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*!
        \qmlsignal TextField::scaledSizeChanged(real scaleFactor)

        When an the size of the main window is changed, this signal is emitted from
        c++ main class. A scale factor is given by \a scaleFactor

    */
    signal scaledSizeChanged(real scaleFactor)
    signal focusToRoot()

    /*!
        \qmlsignal TextField::openKeyboard()
    */
    signal openKeyboard(int layoutPage)
    /*!
        \qmlsignal TextField::closeKeyboard()
    */
    signal closeKeyboard()

    property string textIO
    property variant __textIO: eval(textIO)
    onTextIOChanged: {
        if(textIO !== ""){
            __textIO = eval(textIO)
        }
    }




    property variant textIOValue: (__textIO !== undefined) ? String(__textIO.value) : textfield.text


    /*!
        \qmlmethod TextField::copy()

        Copies the currently selected text to the system clipboard.
    */
    function copy() {
        textEditComponent.copy()
    }

    /*!
        \qmlmethod TextField::cut()

        Moves the currently selected text to the system clipboard.
    */
    function cut() {
        textEditComponent.cut()
    }

    /*!
        \qmlmethod TextField::deselect()

        Removes active text selection.
    */
    function deselect() {
        textEditComponent.deselect();
    }

    /*!
        \qmlmethod string TextField::getText(int start, int end)

        Removes the section of text that is between the \a start and \a end
        positions from the TextField.
    */
    function getText(start, end) {
        return textEditComponent.getText(start, end);
    }

    /*!
        \qmlmethod TextField::insert(int position, string text)

        Inserts \a text into the TextField at \a position.
    */
    function insert(position, text) {
        textEditComponent.insert(position, text);
    }

    /*!
        \qmlmethod bool TextField::isRightToLeft(int start, int end)

        Returns \c true if the natural reading direction of the editor text
        found between positions \a start and \a end is right to left.
    */
    function isRightToLeft(start, end) {
        return textEditComponent.isRightToLeft(start, end);
    }

    /*!
        \qmlmethod TextField::paste()

        Replaces the currently selected text by the contents of the system
        clipboard.
    */
    function paste() {
        textEditComponent.paste()
    }

    /*!
        \qmlmethod TextField::redo()

        Performs the last operation if redo is \l {canRedo}{available}.
    */
    function redo() {
        textEditComponent.redo();
    }

    /*!
        \qmlmethod TextField::select(int start, int end)

        Causes the text from \a start to \a end to be selected.

        If either start or end is out of range, the selection is not changed.

        After calling select, selectionStart will become the lesser
        and selectionEnd will become the greater (regardless of the order passed
        to this method).

        \sa selectionStart, selectionEnd
    */
    function select(start, end) {
        textEditComponent.select(start, end)
    }

    /*!
        \qmlmethod TextField::selectAll()

        Causes all text to be selected.
    */
    function selectAll() {
        textEditComponent.selectAll()
    }

    /*!
        \qmlmethod TextField::selectWord()

        Causes the word closest to the current cursor position to be selected.
    */
    function selectWord() {
        textEditComponent.selectWord()
    }

    /*!
        \qmlmethod TextField::undo()

        Reverts the last operation if undo is \l {canUndo}{available}. undo()
        deselects any current selection and updates the selection start to the
        current cursor position.
    */
    function undo() {
        textEditComponent.undo();
    }

    function clickedAction(){
        clicked()
    }
    function disable(){
        textEditComponent.deselect()
        //focusToRoot()
        textfield.focus = false
        textfield.parent.forceActiveFocus()
        textfield.isEnabled = false
    }
    function enable(){
        textfield.isEnabled = true
    }


    onActiveFocusChanged: {
        if(readOnly === false){
            if(activeFocus == true){
                textEditComponent.selectAll()
                openKeyboard(0) // start with alpha numeric layout
            }else{
                textEditComponent.deselect()
                accepted()
                closeKeyboard()

            }
        }
    }

    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    /*! \internal
        The scale Factor is always initialized with 1.
    */
    property real __scale: 1

    width: S.Style.multilinetextWidthInitial
    height: S.Style.multilinetextHeightInitial
    clip: true
    enabled: isEnabled


    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    onScaledSizeChanged: {
       __scale = scaleFactor
    }

    onAccepted: {
        closeKeyboard()
        if(__doNotAccept == true){
            return
        }
//        text = textEdit.text
        if(__textIO != "" && textIOValue !== undefined){
            switch (__textIO.type){
                case IoType.IOT_DOUBLE:{
                    text = String(textIOValue)
                    __textIO.value = parseFloat(text)
                    break;
                }
                case IoType.IOT_INTEGER:{
                    text = String(textIOValue)
                    __textIO.value = parseInt(text)
                    break;
                }
                case IoType.IOT_BOOLEAN:{
                    __textIO.value = text
                    break;
                }
                case IoType.IOT_STRING:{
                    __textIO.value = text
                    break;
                }
            }
        }
    }

    onTextIOValueChanged: {
        if((__textIO !== undefined && textIOValue !== undefined)){
            if(!textfield.activeFocus) text = textIOValue
        }
    }


    BorderImage {
        id: textfieldImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].multiBorderLeftSize
            top: S.Style[__styleName].multiBorderTopSize
            right: S.Style[__styleName].multiBorderRightSize
            bottom: S.Style[__styleName].multiBorderBottomSize
        }
        source: __imageEnabled
        visible: !readOnly && enabled
    }
    BorderImage {
        id: textfieldDisabledImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].multiBorderLeftSize
            top: S.Style[__styleName].multiBorderTopSize
            right: S.Style[__styleName].multiBorderRightSize
            bottom: S.Style[__styleName].multiBorderBottomSize
        }
        source: __imageDisabled
        opacity: !enabled ? S.Style[__styleName].multiDisabledOpacity : 0

    }
    BorderImage {
        id: textfieldReadOnlyImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].multiBorderLeftSize
            top: S.Style[__styleName].multiBorderTopSize
            right: S.Style[__styleName].multiBorderRightSize
            bottom: S.Style[__styleName].multiBorderBottomSize
        }
        source: __imageReadOnly
        opacity: (readOnly && enabled) ? S.Style[__styleName].multiReadOnlyOpacity : 0
    }
    BorderImage {
        id: textfieldFocusImage
        anchors.fill: textfield
        opacity: (textfield.activeFocus && S.focusOn) ? S.Style[__styleName].multiFocusOpacity : 0
        anchors{
            leftMargin: S.Style[__styleName]. multiFocusShiftLeft
            topMargin:  S.Style[__styleName]. multiFocusShiftRight
            rightMargin: S.Style[__styleName]. multiFocusShiftTop
            bottomMargin: S.Style[__styleName]. multiFocusShiftBottom
        }
        border{
            left: S.Style[__styleName]. multiFocusBorderLeft
            right: S.Style[__styleName]. multiFocusBorderRight
            top: S.Style[__styleName]. multiFocusBorderTop
            bottom: S.Style[__styleName]. multiFocusBorderBottom
        }
        source: __imageFocus
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        z: textfield.activeFocus === true ? - 2 : 2
        onPressed: {
            textEditComponent.forceActiveFocus()
        }

    }
    Flickable{
        id: flickArea
        //TODO find better measure for intial shift!!!
        property real contentYShift: 0
//        interactive: false
        Item{
            id: emptyItem
            anchors.fill: parent

        }



        anchors{
            fill: parent
            leftMargin: S.Style[__styleName].multiTextBorderLeftSize
            rightMargin: S.Style[__styleName].multiTextBorderRightSize
            topMargin: S.Style[__styleName].multiTextBorderTopSize
            bottomMargin: S.Style[__styleName].multiTextBorderBottomSize
        }

        clip:true



        function ensureVisible(r){
                contentHeight = emptyItem.height
                if (contentX > r.x/__scale){
                    contentX = r.x/__scale;
                }else if (contentX+width <= (r.x/__scale)+(r.width/__scale)){
                    contentX = (r.x/__scale)+(r.width/__scale)-width;
                }

                if (contentY > r.y/__scale){
                    contentY = r.y/__scale + contentYShift;
                }else if (contentY+height <= (r.y/__scale)+(r.height/__scale)){
                    contentY = (r.y/__scale)+(r.height/__scale)-height + contentYShift ;
                }
                contentY = Math.max(contentY, contentYShift)

        }


        TextEdit {
            id: textEditComponent
            objectName: "TextFieldText"
            z:2
            signal languageChanged()
            property int fontSize: S.Style[__styleName].multiFontSize
            property int topBorderSize: S.Style[__styleName].multiBorderTopSize
            property int bottomBorderSize: S.Style[__styleName].multiBorderBottomSize
            property int leftBorderSize: S.Style[__styleName].multiBorderLeftSize
            property int rightBorderSize: S.Style[__styleName].multiBorderRightSize
            property real realInputWidth: textEditComponent.width / __scale
            property real realInputHeight: textEditComponent.height / __scale
            property real realLineHeight: textEditComponent.cursorRectangle.height /// __scale
            property string tempText
            enabled: textfield.isEnabled
            selectByMouse: true
            selectionColor: S.Style[__styleName].multiSelectionColor
            selectedTextColor: S.Style[__styleName].multiSelectedTextColor

            font.family: S.Style[__styleName].fontFamily
            font.pointSize: fontSize * __scale > 0 ? fontSize * __scale : 1
            

            anchors.horizontalCenter:  parent.horizontalCenter
            anchors.horizontalCenterOffset:{
                   (((realInputWidth - flickArea.width)/2) + leftBorderSize)
    //                (realInputWidth - flickArea.width)/2
            }
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: {
                ((realInputHeight - emptyItem.height)/2)
            }

            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            onCursorRectangleChanged: flickArea.ensureVisible(cursorRectangle)
            horizontalAlignment: horizontalTextAlignement

            width: ((flickArea.width - leftBorderSize - rightBorderSize) * __scale) - 3

            color:{
                if(enabled){
                    if(readOnly){
                        __fontColorReadOnly
                    }else{
                        __fontColors[fontColorChoice]
                    }
                }else{
                    __fontColorInactive
                }
            }

            clip: true
            text: textfield.text
            activeFocusOnPress: enabled && !readOnly

            Component.onCompleted: {
                flickArea.ensureVisible(cursorRectangle)
//                textEdit.forceActiveFocus()
                textfield.clip = true


            }
            Text {
                id: placeholderTextComponent
                signal languageChanged()
                property int textSize: S.Style[__styleName].multiPlaceholderTextSize
                font.family: parent.font.family
                font.pointSize: textSize * __scale > 0 ? textSize * __scale : 1 
                opacity: (textEditComponent.text.length || !textEditComponent.enabled || textEditComponent.readOnly) ? 0 : S.Style[__styleName].multiPlaceholderOpacity
                color: __fontPlaceholderColors[fontPlaceholderColorChoice]
                clip: true
                horizontalAlignment: parent.horizontalAlignment
                Behavior on opacity { NumberAnimation { duration: 90 } }
                anchors{
                    fill: parent
                    baseline: parent.bottom
//                    baselineOffset: S.Style[__styleName].multiBottomBorderSize
                }

            }
        }

    }
}
