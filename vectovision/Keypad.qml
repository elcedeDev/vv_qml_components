import QtQuick 1.1
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1
import "../../style/Style.js"  as S


Item {

    id: idAradexKeyPad
    objectName: "AradexKeyPad"

    signal sendKey(string key)
    signal sendSpecialKey(int specialKey)

    property int columns    : 3
    property int rows       : 5
    property int spacing    : 3
    property int keyWidth   : 60
    property int keyHeight  : 40

    property string __styleName : S.styleName
    property alias model    : idGridView.model


    width:  (keyWidth  + spacing) * columns
    height: (keyHeight + spacing) * rows

    signal scaledSizeChanged(real minScale)
    signal languageChanged()
    signal styleChanged(string newStyle)
    signal newStyleAdded(string styleName, variant styleObject)

    // catch all mouse events on the background of the keypad
    MouseArea {
        anchors.fill : parent

        GridView {
            id              : idGridView
            interactive     : false
            anchors.fill    : parent
            cellWidth       : keyWidth  + spacing
            cellHeight      : keyHeight + spacing

            model: ListModel{
                ListElement { btnText : "1"     ; chrKey: 0 }
                ListElement { btnText : "2"     ; chrKey: 0 }
                ListElement { btnText : "3"     ; chrKey: 0 }
                ListElement { btnText : "4"     ; chrKey: 0 }
                ListElement { btnText : "5"     ; chrKey: 0 }
                ListElement { btnText : "6"     ; chrKey: 0 }
                ListElement { btnText : "7"     ; chrKey: 0 }
                ListElement { btnText : "8"     ; chrKey: 0 }
                ListElement { btnText : "9"     ; chrKey: 0 }
                ListElement { btnText : "."     ; chrKey: 0 }
                ListElement { btnText : "0"     ; chrKey: 0 }
                ListElement { btnText : "\u232b"; chrKey: 0x01000003 } // Qt::Key_Backspace
                ListElement { btnText : "\u2190"; chrKey: 0x01000012 } // Qt::Key_Left
                ListElement { btnText : "\u2192"; chrKey: 0x01000014 } // Qt::Key_Right
                ListElement { btnText : "OK"    ; chrKey: 0x01000004 } // Qt::Key_Return

            }

            delegate: Button {
                id      : idButton
                text    : btnText
                width   : keyWidth
                height  : keyHeight                
                __activeFocusOnPress : false
                onClicked: {
                    if (chrKey === 0) {
                        sendKey(text)
                    } else {
                        sendSpecialKey(chrKey)
                    }
                }

                // connect signal handlers manually
                Component.onCompleted: {
                    idAradexKeyPad.newStyleAdded    .connect(idButton.newStyleAdded)
                    idAradexKeyPad.styleChanged     .connect(idButton.styleChanged)
                    idAradexKeyPad.languageChanged  .connect(idButton.languageChanged)
                    idAradexKeyPad.scaledSizeChanged.connect(idButton.scaledSizeChanged)
                }
            }
        }
    }
}
