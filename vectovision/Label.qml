// $Revision: 20404 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1

/*!
    \qmltype Label
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A push button with a text label.
 */
FocusScope {
    id: label
    objectName: "AradexLabel"

    /*! \internal
        \qmlProperty bool Label::__activeFocusOnPress
        Specifies whether the button should
        gain active focus when pressed.
        The default value is \c true. 
	*/
    property bool __activeFocusOnPress: true
    
	/*! \internal
        \qmlmethod Label::__paintedTextWidth
        Width of the text element inside the label.
	*/
    property alias __paintedTextWidth: textInput.paintedWidth
    
	/*! \internal
        \qmlmethod Label::__paintedTextHeight
        Height of the text element inside the label.
	*/
    property alias __paintedTextHeight: textInput.paintedHeight
    
	/*! \internal
        \qmlProperty int Label::__imageChoice
        The background image for the Label. The selection has to be an Integer between 0-3.
        The selected images will be S.Style[__styleName].labelImage0 - S.Style[__styleName].labelImage3
        accordingly.
	*/
    property int __imageChoice: 0 /*colorChoice*/

    /*! \internal
        Array holding all label background images. 
	*/
    property variant labelImages: [S.Style[__styleName].labelImage0,
        S.Style[__styleName].labelImage1,
        S.Style[__styleName].labelImage2,
        S.Style[__styleName].labelImage3]

    /*!
        \qmlProperty int Label::colorChoice
        The background of the Text inside the Label. The selection has to be an Integer between 0-3.
        The selected images will be S.Style[__styleName].labelImage0 - S.Style[__styleName].labelImage3
        accordingly. 
	*/
    property int colorChoice: 0
    
	/*! \internal
        Array holding all Text colors for the Label.
    */
    property variant labelColors: [S.Style[__styleName].labelFontColor0,
        S.Style[__styleName].labelFontColor1,
        S.Style[__styleName].labelFontColor2,
        S.Style[__styleName].labelFontColor3]

    property bool __smooth: S.isFast
    property string __backgroundImage: ""

    /*!
        \qmlProperty string Label::fontColorNormal
        Font color of displayed Text, when button is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability. 
	*/
    property alias __fontColorNormal: textInput.color

    /*! \internal
        String holding relative path to the image folder. 
	*/
    property string __imageFolder: S.Style[__styleName].imageFolder

    /*! \internal
        \qmlProperty string Label::__leftTextMargin
        \brief Left margin of text on top of image margin:
        Additional left Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.leftonFoTextMargin. */
    property int __leftTextMargin: S.Style[__styleName].labelTextLeftMargin
    
	/*! \internal
        \qmlProperty string Label::__rightTextMargin
        \brief Right margin of text on top of image margin:
        Additional rightMargin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.rightTextMargin.
      */
    property int __rightTextMargin: S.Style[__styleName].labelTextRightMargin
    
	/*!
        \qmlProperty string Label::__bottomTextMargin
        \brief Bottom margin of text on top of image margin:
        Additional bottom Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.bottomTextMargin.
      */
    property int __bottomTextMargin: S.Style[__styleName].labelTextBottomMargin
    
	/*! \internal
        \qmlProperty string Label::__topTextMargin
        \brief Top margin of text on top of image margin
        Additional top Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.topTextMargin.
    */
    property int __topTextMargin: S.Style[__styleName].labelTextTopMargin

    /* \internal
        \qmlProperty string Label::__paintedHeight
        \brief Height of the text inside the Label in pixels.
        The default value is \c <Stylefile>.topTextMargin.
    */
    property int __paintedHeight: textInput.paintedHeight
    
	/*
        \qmlProperty string Label::__paintedWidth
        \brief Width of the text inside the Label in pixels.
		The default value is \c <Stylefile>.topTextMargin.
	*/
    property int __paintedWidth: textInput.paintedWidth

    /*!
        \qmlProperty string Label::text
        This property holds the text shown on the label. If the button has no
        text, the \l text property will be an empty string.
		The default value is the empty string.
    */
    property alias text: textInput.text
	
    property string __textColorNormal: ""
    
	/*! \internal
        \qmlProperty variant Label::__textIO
        Contains the IO object
    */
    property variant __textIO: eval(textIO)
    
	/*!
        \qmlProperty string Label::textIO
        Connects the text shown on the button to an IO value.
        The default value is the empty string.
    */
	property string textIO
    onTextIOChanged: {
        if (textIO !== ""){
            __textIO = eval(textIO)
        }
    }
    property variant textIOValue: (__textIO !== undefined) ? __textIO.value : label.text
    onTextIOValueChanged:{
        if (__textIO !== undefined && textIOValue !== undefined){
            label.text = formatIOText(textIOValue)
        }
    }

    /*!
        \qmlProperty string Label::unit
        A string that is put behind the IO values shown on the label.
        The default value is the empty string.
    */
    property string unit
    onUnitChanged: {
        if (__textIO !== undefined&& textIOValue !== undefined) {
            label.text = formatIOText(textIOValue)
        }
    }

    /*!
        \qmlProperty bool Label::unitEnabled
        Whether to put a unit behind the IOString. If unit is given it will
        be used. Otherwise the unit specified for the IO in V8 is used
    */
    property bool unitEnabled: false
    onUnitEnabledChanged:  {
        if (__textIO !== undefined&& textIOValue !== undefined) {
            label.text = formatIOText(textIOValue)
        }
    }
    
	/*!
        \qmlProperty int Label::digitsAfterComma
        The number of digits behind the decimal mark.
		The default value is \c 2 string.
    */
    property int digitsAfterComma:2
    onDigitsAfterCommaChanged:  {
        if (__textIO !== undefined&& textIOValue !== undefined) {
            label.text = formatIOText(textIOValue)
        }
    }

    /*! \internal
        \qmlProperty int Label::__borderSizeLeft
        Left border of the background BorderImage.
        The default value is <StyleFile>.labelBorderLeftSize.
    */
    property int __borderSizeLeft: S.Style[__styleName].labelBorderLeftSize
    
	/*! \internal
        \qmlProperty int Label::__borderSizeRight
        Right border of the background BorderImage.
        The default value is  <StyleFile>labelBorderRightSize.
    */
    property int __borderSizeRight: S.Style[__styleName].labelBorderRightSize
    
	/*! \internal
        \qmlProperty int Label::__borderSizeTop
        Top border of the background BorderImage.
        The default value is  <StyleFile>.labelBorderTopSize
    */
    property int __borderSizeTop: S.Style[__styleName].labelBorderTopSize
    
	/*! \internal
        \qmlProperty int Label::__borderSizeBottom
        Bottom border of the background BorderImage.
        The default value is  <StyleFile>.labelBorderBottomSize.
    */
    property int __borderSizeBottom: S.Style[__styleName].labelBorderBottomSize
    
	/*! \internal
        \qmlProperty int Label::__textBorderSizeLeft
        Invisible left border of the text inside the component.
        The default value is  <StyleFile>.labelTextBorderLeftSize.
    */
    property int __textBorderSizeLeft: S.Style[__styleName].labelTextBorderLeftSize
    
	/*! \internal
        \qmlProperty int Label::__textBorderSizeRight
        Invisible right border of the text inside the component.
        The default value is  <StyleFile>.labelTextBorderRightSize.
    */
    property int __textBorderSizeRight: S.Style[__styleName].labelTextBorderRightSize
    
	/*! \internal
        \qmlProperty int Label::__textBorderSizeTop
        Invisible top border of the text inside the component.
        The default value is  <StyleFile>.labelTextBorderTopSize.
    */
    property int __textBorderSizeTop: S.Style[__styleName].labelTextBorderTopSize
    
	/*! \internal
        \qmlProperty int Label::__textBorderSizeBottom
        Invisible top border of the text inside the component.
        The default value is  <StyleFile>.labelTextBorderTopSize.
    */
    property int __textBorderSizeBottom: S.Style[__styleName].labelTextBorderBottomSize

    /*!
        \qmlProperty int Label::fontPointSize
        
		PointSize of displayed Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
        
	   The default value is \c <Stylefile>.labelFontSize.
      */
    property int fontSize: S.Style[__styleName].labelFontSize
    
	/*! \internal
        \qmlProperty string Label::__labelFontFamily
        PointSize of displayed Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.

        The default value is \c <Stylefile>.labelFontSize.
      */
    property string __fontFamily: S.Style[__styleName].labelFontFamily

    property bool __fontBold: false
    property bool __fontOutline: false
    property string __fontOutlineColor: "#000000"

    /*!
        \qmlproperty enumeration Label::verticalAlignment

        Sets the alignment of the text within the Label item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignTop
        \li Text.AlignBottom
        \li Text.AlignVCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of Label, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property alias __verticalTextAlignement: textInput.verticalAlignment

    /*!
        \qmlproperty enumeration Label::horizontalAlignment

        Sets the alignment of the text within the Label item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignLeft
        \li Text.AlignRight
        \li Text.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of Label, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property alias horizontalTextAlignement: textInput.horizontalAlignment


    /*!
        \qmlsignal Label::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)

    /*!
        \qmlsignal Label::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)

    */
    signal scaledSizeChanged(real scaleFactor)

    /*!
        \qmlmethod Label::formatIOText()
        Takes the value of the IO connected to the text on the button and formats it
        appropriately, by reducig digits and adding a unit.
    */
    function formatIOText(rawText){
        var text
        if (__textIO !==  undefined){
            switch (__textIO.type){
                case IoType.IOT_DOUBLE:{
                        text = Number(rawText).toFixed(digitsAfterComma)
                        if (unitEnabled == true){
                            if(unit !== ""){
                                text += " " + unit
                            }else{
                                text += " " +__textIO.unit
                            }

                        }
                        break;
                }
                case IoType.IOT_INTEGER:{
                    text = Number(rawText).toFixed(digitsAfterComma)
                    if (unitEnabled == true){
                        if(unit !== ""){
                            text += " " + unit
                        }else{
                            text += " " +__textIO.unit
                        }

                    }
                    break;
                }
                default:{
                    text = rawText
                }
            }

            return text
        }else{

            return rawText
        }
    }


    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    implicitWidth:  S.Style.labelWidthInitial
    implicitHeight:  S.Style.labelHeightInitial

    clip: true

    /* \qmlproperty wrapMode : enumeration
	   Set this property to wrap the text to the Text item's width. The text will only wrap if an explicit width has been set. wrapMode can be one of:

        Text.NoWrap (default) - no wrapping will be performed. If the text contains insufficient newlines, then paintedWidth will exceed a set width.
        Text.WordWrap - wrapping is done on word boundaries only. If a word is too long, paintedWidth will exceed a set width.
        Text.WrapAnywhere - wrapping is done at any point on a line, even if it occurs in the middle of a word.
        Text.Wrap - if possible, wrapping occurs at a word boundary; otherwise it will occur at the appropriate point on the line, even in the middle of a word.
    */

    property alias wrapMode: textInput.wrapMode

    onStyleChanged: {
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    // no dynamic loading since that causes problems in editor
	
	/*!
        \qmlProperty bool Label::animationOn
        \brief Enables highlighting of  the label text if the value has changed.
        The default value is \c false. 
	*/
    property bool animationOn: false
	
	/*! \internal
        \qmlProperty color Label::__animationHighlightColor
        \brief If animation is enabled and the value has changed the text color is aet to this value.
        The default value is \c StyleName.labelAnimationHighlightColor. 
	*/
	property color __animationHighlightColor : S.Style[__styleName].labelAnimationHighlightColor
    
	/*! \internal
        \qmlProperty int Label::__animationHighlightHoldTime
        \brief If animation is enabled and the text value has changed the highlight color
		is kept at least '__animationHighlightHoldTime' milliseconds.
        The default value is \c StyleName.labelAnimationHighlightHoldTime. 
	*/
	property int __animationHighlightHoldTime : S.Style[__styleName].labelAnimationHighlightHoldTime
    
	/*! \internal
        \qmlProperty int Label::__animationHighlightTransitionTime
        \brief Time in milliseconds for transition to standard text color.
		If animation is enabled, the text did not change and '__animationHighlightHoldTime' is passed
		the text color starts changing back to the standard color for this time in milliseconds.
        The default value is \c StyleName.labelAnimationHighlightTransitionTime. 
	*/
	property int __animationHighlightTransitionTime : S.Style[__styleName].labelAnimationHighlightTransitionTime


    BorderImage{
        id: labelImage
        anchors.fill: parent
        border{
            left: __borderSizeLeft
            right: __borderSizeRight
            top: __borderSizeTop
            bottom: __borderSizeBottom
        }
        smooth: __smooth
        source: (labelImages[__imageChoice] !== "") ? S.Style[__styleName].imageFolder + labelImages[__imageChoice] : __backgroundImage
    }

    Text {
        id: textInput
        objectName: "labelElement"

        signal languageChanged()
        font.pointSize: {fontSize > 0 ? fontSize : 1}
        font.family: __fontFamily
        font.bold: __fontBold

        style: __fontOutline ? Text.Outline : Text.Normal
        styleColor: __fontOutlineColor
        anchors{
            fill: parent
            leftMargin: __textBorderSizeLeft + 1 + __leftTextMargin
            topMargin:  __textBorderSizeRight  + 1 +__topTextMargin
            rightMargin: __textBorderSizeTop + 1 + __rightTextMargin
            bottomMargin: __textBorderSizeBottom + 1 + __bottomTextMargin
        }
        horizontalAlignment: S.Style[__styleName].labelHorizontalAlignement
        verticalAlignment:  S.Style[__styleName].labelVerticalAlignement

        smooth: __smooth


        text:  (__textIO !== "" && textIOValue !== undefined) ? formatIOText(textIOValue) : label.text //label.text
		onTextChanged: {
			if (animationOn) {
					textHighlightAnimation.restart()
			}
		}

		function getColor() {
			return (__textColorNormal == "") ? labelColors[colorChoice] : __textColorNormal;
		}
		
        color: getColor()

        clip: true
		
		SequentialAnimation {
			id: textHighlightAnimation
			loops: 1
			PropertyAnimation {
				target: textInput
				property: "color"
				to: __animationHighlightColor
				duration: 0
			}

			PauseAnimation { duration: __animationHighlightHoldTime }
			
			PropertyAnimation {
				target: textInput
				property: "color"
				to: textInput.getColor()
				duration: __animationHighlightTransitionTime
			}
		}
    }
}
