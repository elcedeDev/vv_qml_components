// $Revision: 20400 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1



/*!
    \qmltype Label
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A push button with a text label.

 */
FocusScope {
    id: label
    objectName: "AradexIndicator"


    /*! \internal
        String holding relative path to the image folder.
    */
    property string __imageFolder: S.Style[__styleName].imageFolder

    property bool doubleIndicator: false
    property bool inverseState: false

//    property int numberOfStates: 2

    property string __doubleColorOn: __imageFolder + S.Style[__styleName].indicatorColorOn
    property string __doubleColorOff:  __imageFolder + S.Style[__styleName].indicatorColorOff

//    property int singleState: 0
//    property int __singleState: (__intIO !== undefined) ? __intIO.value : singleState

    property bool initialState: false
    property bool __indicatorState: (__valueIO !== undefined) ? __valueIO.value : initialState

    /*! \internal
        Array holding all label background images.
    */
    property variant __singleColors: [S.Style[__styleName].indicatorColor0,
        S.Style[__styleName].indicatorColor1,
        S.Style[__styleName].indicatorColor2,
        S.Style[__styleName].indicatorColor3]


    /*!
        \qmlProperty string Label::LabelTextIO
        Connects the text shown on the button to an IO value.

        The default value is the empty string.
    */
    property variant __valueIO: eval(valueIO)
    property string valueIO
    onValueIOChanged: {
        if (valueIO !== ""){
            __valueIO = eval(valueIO)
        }
    }

    /*!
        \qmlProperty string Label::LabelTextIO
        Connects the text shown on the button to an IO value.

        The default value is the empty string.
    */
//    property variant __intIO: eval(intIO)
//    property string intIO
//    onIntIOChanged: {
//        if (intIO !== ""){
//            __intIO = eval(intIO)
//        }
//    }
    /*!
        \qmlProperty int Label::__borderSizeLeft
        Left border of the background BorderImage.
        The default value is <StyleFile>.labelBorderLeftSize.
    */
    property int __borderSizeLeft: S.Style[__styleName].labelBorderLeftSize
    /*!
        \qmlProperty int Label::__borderSizeRight
        Right border of the background BorderImage.
        The default value is  <StyleFile>labelBorderRightSize.
    */
    property int __borderSizeRight: S.Style[__styleName].labelBorderRightSize
    /*!
        \qmlProperty int Label::__borderSizeTop
        Top border of the background BorderImage.
        The default value is  <StyleFile>.labelBorderTopSize
    /*!.
    */
    property int __borderSizeTop: S.Style[__styleName].labelBorderTopSize
    /*!
        \qmlProperty int Label::__borderSizeBottom
        Bottom border of the background BorderImage.
        The default value is  <StyleFile>.labelBorderBottomSize.
    */
    property int __borderSizeBottom: S.Style[__styleName].labelBorderBottomSize
    /*!
        \qmlsignal Label::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)


    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFactor)


    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    implicitWidth:  (doubleIndicator == true) ? S.Style.IndicatorDoubleWidthInitial : S.Style.IndicatorSingleWidthInitial
    implicitHeight:  (doubleIndicator == true) ? S.Style.IndicatorDoubleHeightInitial : S.Style.IndicatorSingleHeightInitial


    clip: true

    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    // no dynamic loading since that causes problems in editor




    BorderImage{
        id: labelImage
        anchors.fill: parent
        border{
            left: __borderSizeLeft
            right: __borderSizeRight
            top: __borderSizeTop
            bottom: __borderSizeBottom
        }
        source:{
            if(doubleIndicator == true){
                if (inverseState) {
                    (__indicatorState === true) ? __doubleColorOff : __doubleColorOn
                } else {
                    (__indicatorState === true) ? __doubleColorOn : __doubleColorOff
                }
            }else{
                if (inverseState) {
                    (__indicatorState === true) ? __imageFolder + __singleColors[0] : __imageFolder + __singleColors[1]
                }
                else {
                    (__indicatorState === true) ? __imageFolder + __singleColors[1] : __imageFolder + __singleColors[0]
                }

            }

//            else{
//                if(__boolIO !== undefined){
//                    (__boolIO.value ? __imageFolder + __singleColors[1] : __imageFolder + __singleColors[0])
//                }else{
//                    __imageFolder + __singleColors[__singleState]
//                }


//            }
        }
    }






}
