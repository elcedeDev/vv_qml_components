// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S

Rectangle {
    id:root
    objectName: "AradexThemeShapeElement"
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    radius: 7
    Rectangle{
        height: parent.radius
        width: parent.radius
        x: 0
        y: 0
        color: parent.color
    }

}
