// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools.shortcut 1.1
import tools 1.1

Rectangle {
    id:root
    objectName: "AradexThermomoeter"
    width: 200
    height: 360
    color: '#00000000' // 'transparent'
    clip:true

    implicitWidth: __originalWidth
    implicitHeight: __originalHeight

    /*! \internal
    The style from Style.js needs to be redirected
    to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    property int ySpan: stopY - startY

    property real labelStepSize: S.Style[__styleName].labelStepSize
    property bool textVisible: true
    property bool labelVisible: true

    property real scaleStart: 4
    property real scaleStop: 10
    property real scaleStep: 1
    property bool onlyToMinScale: true
    property bool scientificNumText: false
    property int digitsAfterCommaLabel: 2



    /*!
        \qmlProperty bool Thermomoeter::majorTickStepSize

        \brief Specifies the size of one tick step.

        The default value is \c 6. */
    property real majorTickStepSize: 1
    /*!
        \qmlProperty bool Thermomoeter::minorTickNum

        \brief Specifies how many minor tics to put between two major ticks.

        The default value is \c 6. */
    property int minorTickNum: 3
    /*!
        \qmlProperty bool Thermomoeter::minValue

        \brief Minimum value of the scale.

        The default value is \c 0. */
    property real minValue: 0.0
    /*!
        \qmlProperty bool Thermomoeter::minValue

        \brief Minimum value of the scale.

        The default value is \c 100. */
    property real maxValue: 12
    /*!
        \qmlProperty bool Thermomoeter::startAngle

        \brief The angle position of the first Tick mark.

        The angle position of the first major Tick mark, where 0°/ 360° would be
        twelfe o'clock and 180° equals 6 o'clock.

        The default value is \c 0. */
    property int startY: 30
    /*!
        \qmlProperty bool Thermomoeter::stopAngle

        \brief The angle position of the first Tick mark.

        The angle position of the last major Tick mark, where 0°/ 360° would be
        twelfe o'clock and 180° equals 6 o'clock.

        The default value is \c 0. */
    property int stopY: 333
    /*!
        \qmlProperty bool Thermomoeter::initialValue

        \brief The initial value the meter shows

        The default value is \c 0. */
    property string value: (__meterIO != "") ? eval(__meterIO + ".value"): initialValue
    property real initialValue: 0.0

    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Image URL of thermometer big scale(tick) mark.
        If edited manually in editor it will lose automatic style changing capability.
      */
    property string scaleBigImage: S.Style[__styleName].imageFolder + "meter/ThermometerScaleBig.png"
    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Image URL of thermometer small scale(tick) mark.
        If edited manually in editor it will lose automatic style changing capability.
      */
    property string scaleSmallImage: S.Style[__styleName].imageFolder + "meter/ThermometerScaleSmall.png"
    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Image URL of thermometer marker.
        If edited manually in editor it will lose automatic style changing capability.
      */
    property string markerImage: S.Style[__styleName].imageFolder + "meter/ThermometerMarker.png"
    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Background image URL of thermometer. Usually this includes an empty thermometer.
        If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string backgroundImage: S.Style[__styleName].imageFolder + "meter/ThermometerBackground.png"
    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Foreground image URL of thermometer. Will often be emitted.
        If edited manually in editor it will lose automatic style changing capability.
      */
    property string foregroundImage: S.Style[__styleName].imageFolder + "meter/ThermometerForeground.png"
    /*!
        \qmlProperty string Thermometer::scaleBigImage
        Image URL of thermometer filling.
        If edited manually in editor it will lose automatic style changing capability.
      */
    property string fillingImage: S.Style[__styleName].imageFolder + "meter/ThermometerFilling.png"



    /*! \internal
        Width of the big scale mark.
    */
    property real __bigScaleWidth: S.Style[__styleName].thermometerBigScaleWidth
    /*! \internal
        Height of the big scale mark.
    */
    property real __bigScaleHeight: S.Style[__styleName].thermometerBigScaleHeight
    /*! \internal
        Shift of the big scale mark in x direction
    */
    property real __bigScaleShift: S.Style[__styleName].thermometerBigScaleShift
    /*! \internal
        Aspect ratio preserving scale Factor.
    */
    property real __internalScale: Math.min(height/S.Style.meter360HeightInitial, width/S.Style.meter360WidthInitial)
    /*! \internal
        X- value of the center for a lebel showing the value.
    */
    property real __labelCenterX: S.Style[__styleName].thermometerLabelCenterX
    /*! \internal
        Y- value of the center for a lebel showing the value.
    */
    property real __labelCenterY: S.Style[__styleName].thermometerLabelCenterY
    /*! \internal
        Height of the marker that shows the exact value.
    */
    property real __markerHeight: S.Style[__styleName].thermometerMarkerHeight
    /*! \internal
        Shift of the marker in X- direction.
    */
    property real __markerShift: S.Style[__styleName].thermometerMarkerShift
    /*! \internal
        Width of the marker that shows the exact value.
    */
    property real __markerWidth: S.Style[__styleName].thermometerMarkerWidth
    /*! \internal
        initial width
    */
    property real __originalWidth:S.Style.meter360WidthInitial
    /*! \internal
       initial height
    */
    property real __originalHeight:S.Style.meter360HeightInitial
    /*! \internal
        Width of the aspect ratio preserving widget inside the component.
    */
    property real __scaledWidth: __originalWidth * __internalScale
    /*! \internal
        Height of the aspect ratio preserving widget inside the component
    */
    property real __scaledHeight: __originalHeight * __internalScale
    /*! \internal
        Width of the small scale mark.
    */
    property real __smallScaleWidth: S.Style[__styleName].thermometerSmallScaleWidth
    /*! \internal
        Height of the small scale mark.
    */
    property real __smallScaleHeight: S.Style[__styleName].thermometerSmallScaleHeight
    /*! \internal
        Shift of the small scale mark in X- direction.
    */
    property real __smallScaleShift: S.Style[__styleName].thermometerSmallScaleShift
    /*! \internal
        Font Family of labels. Default Value is the default family for this style.
    */
    property string __textFontFamily: S.Style[__styleName].thermometerFontFamily
    /*! \internal
        Shift of the scale labels in X- direction.
    */
    property int __textHorizontalOffset: S.Style[__styleName].thermometerTextHorizontalOffset
    /*! \internal
        Font Pixel Size of labels. Default Value is the default family for this style.
    */
    property int __textPixelSize: S.Style[__styleName].thermometerFontSize
    /*! \internal
        Shift of the scale labels in Y- direction.
    */
    property int __textVerticalOffset: S.Style[__styleName].thermometerTextVerticalOffset
    /*! \internal
        Length of the intervall that is projected onto the thermomoeter. (Not just the
        intevall between the outermost scale marks)
    */
    property real __valueSpan: maxValue - minValue
    /*! \internal
        Value converted into unscaled y values within the component
    */
    property real __yValue: Math.max(valueToPercent(scaleStart),valueToPercent(value)) * ySpan

    /*! \internal
        Proxy variable to ensure IOs are not unset on state changes
    */
    property string __meterIO
    /*!
        \qmlProperty bool Thermomoeter::meterIO

        \brief The name of an IO whose value the meter shows.

        The default value is \c 0. */
    property string meterIO
    onMeterIOChanged: {
        if (meterIO !== ""){
            __meterIO = meterIO
        }
    }


    /*!
        \qmlmethod valueToPercent(value)
        Transforms \a value into fraction of value span.
    */
    function valueToPercent(value){
        return (value-minValue)/__valueSpan
    }

    /*!
        \qmlmethod valueToPercent(value)
        Transforms \a value unscaled y value within the component.
    */
    function valueToY(value){
        return startY + (1- (value-minValue)/__valueSpan)*ySpan
    }
    /*!
        \qmlmethod valueToPercent(value)
        Helper function to repeat a string
    */
    function repeatString(num, stringToRepeat) {
        return new Array(isNaN(num)? 1 : ++num).join(stringToRepeat);
    }
    /*!
        \qmlmethod valueToPercent(value)
        Helper function to format value to correct number of digits and scientific notation if requested.
    */
    function formatNumber(num, scientific, digits){
        if(scientific === true){
            return Number(num).toExponential(digits)
        }else{
            return Number(num).toFixed(digits)
        }
    }


    Image{
        id: marker
        width: __markerWidth * __internalScale
        height: __markerHeight* __internalScale
        x: __markerShift * __internalScale
        y: (Math.min(ySpan + startY,Math.max(overlay.y, startY))- marker.height/2)
        source:  markerImage
    }

    Image{
        id: background
        width: __scaledWidth
        height: __scaledHeight

        source:  backgroundImage
    }
    Image{
        id: foreground
        width: __scaledWidth
        height: __scaledHeight

        source:  foregroundImage
    }

    // overlay is used to trim the fillig to the appropriate height.
    Rectangle{
        id:overlay
        x: 0
        y: (ySpan-__yValue + startY) * __internalScale
        width: __scaledWidth
        height: (__yValue + startY) * __internalScale
        color: "#00000000"
        clip: true
        BorderImage{
            id: filling
            x: 0
            y: (__yValue - ySpan - startY)* __internalScale
            width: __scaledWidth
            height: __scaledHeight

            source:  fillingImage
            clip:true

        }

    }
    Text{
        id: minText
        font{
            family: __textFontFamily
            pixelSize: __textPixelSize
        }
        opacity: 0
        text: repeatString(formatNumber(scaleStart, scientificNumText, digitsAfterCommaLabel).toString().length, "8")
    }
    Text{
        id:maxText
        font{
            family: __textFontFamily
            pixelSize: __textPixelSize
        }
        text: repeatString(formatNumber(scaleStop, scientificNumText, digitsAfterCommaLabel).toString().length, "8")
        opacity: 0

    }
    Repeater {
        id: bigscales
//        property real actualRadius: __meterRadius + __bigScaleRadiusShift
        property int scaleNum:((scaleStop - scaleStart)/scaleStep) + 1
        property int maxTextLength: Math.max(minText.width, maxText.width)
        model: scaleNum
        delegate: Image {
            property real value: scaleStart +(index * scaleStep)
            width: __bigScaleWidth * __internalScale;
            height: __bigScaleHeight * __internalScale;
            source: scaleBigImage
            y: (valueToY(value) - __bigScaleHeight/2) * __internalScale
            x: __bigScaleShift * __internalScale
            opacity: 1.0

            Text {
                id: scaleText
                x: (bigscales.maxTextLength/2 +  __textHorizontalOffset) * __internalScale
                y: (__textVerticalOffset -scaleText.height/2) * __internalScale
                font{
                    pixelSize: __textPixelSize
                    family: __textFontFamily
                }
                visible: index % labelStepSize == 0
                text: formatNumber(parent.value, scientificNumText, digitsAfterCommaLabel)

            }
        }
    }
    Repeater {
        id: samllScales
//        property real actualRadius: __meterRadius + __bigScaleRadiusShift
        property int scaleNum:(bigscales.scaleNum -1) * (minorTickNum + 1)
        model: scaleNum
        delegate: Image {
//            property real radian: ((index/majorTickNum)* __radianSpan) + __startRadian
            property real value: scaleStart +(index * scaleStep/(minorTickNum + 1))
            width: __smallScaleWidth * __internalScale;
            height: __smallScaleHeight * __internalScale;
            source: scaleSmallImage
            y: (valueToY(value) - __smallScaleHeight/2) * __internalScale
            x: __smallScaleShift * __internalScale
            opacity: (index % (minorTickNum + 1) == 0) ? 0 : 1


        }
    }
    Label{
        x: __labelCenterX * __internalScale - width/2
        y: __labelCenterY * __internalScale - height/2
        width: Math.max(minText.width, maxText.width)  * 1.2
        height:  Math.max(minText.height, maxText.height)  * 1.1
        text: formatNumber(root.value, scientificNumText, digitsAfterCommaLabel)
        visible: labelVisible
    }
}
