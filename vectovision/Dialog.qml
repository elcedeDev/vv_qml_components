// $Revision: 17918 $
/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Components project.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
** * Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
** * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
** the names of its contributors may be used to endorse or promote
** products derived from this software without specific prior written
** permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/
import QtQuick 1.1
import "../../style/Style.js"  as S


/*!
    \qmltype Popup
    \inqmlmodule ??? VV7 HMI
    \since 06.09.2013
    \ingroup ??? NotClearYet
    \brief A Popup with an Open and a Closed State.
    In the Open state the background fades out and cannot
    be used.

 */
Popup {
    id: dialog
    property alias __visualParent: dialog.visualParent
    property string __styleName: S.styleName

    //property alias __animationDuration: dialog.__animationDuration

    signal ok
    signal no
    signal cancel
    signal clickedOutside
    property string __currentState: dialog.state
    function openDialog() {
        open()        
        dialog.forceActiveFocus()
    }
    function setOk() {
        if (status == S.DialogStatus.Open) {
            close()
            ok()
        }
    }
    function setNo() {
        if (status == S.DialogStatus.Open) {
            close()
            no()
        }
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    /*!
        \qmlsignal Button::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)

    onStyleChanged:{
        __styleName = strStyle
    }

//        onFaderClicked: root.clickedOutside()
    state: "Hidden"
    visible: false
    opacity: 0.0
    anchors.centerIn: parent
    __animationDuration: S.Style[__styleName].animationDuration
    // Consume all key events that are not processed by children
    Keys.onPressed: event.accepted = true
    Keys.onReleased: event.accepted = true



    states: [
        State {
            name: "Visible"
            when: status == S.DialogStatus.Opening || status == S.DialogStatus.Open
            PropertyChanges { target: dialog; opacity: 1.0; visible: true}
        },
        State {
            name: "Hidden"
            when: status == S.DialogStatus.Closing || status == S.DialogStatus.Closed
            PropertyChanges { target: dialog; opacity: 0.0; visible: false}
        }
    ]
    transitions: [
        Transition {
            from: "Visible"; to: "Hidden"
            SequentialAnimation {
                ScriptAction {script: status = S.DialogStatus.Closing }
                NumberAnimation {
                    properties: "opacity"
                    duration: dialog.__animationDuration
                    easing.type: Easing.Linear
                }

                ScriptAction {script: status = S.DialogStatus.Closed }
            }
        },
        Transition {
            from: "Hidden"; to: "Visible"
            SequentialAnimation {
                ScriptAction { script: { status = S.DialogStatus.Opening}}
                
                NumberAnimation {
                    properties: "opacity"
                    duration: dialog.__animationDuration
                    easing.type: Easing.Linear
                }


                ScriptAction { script: status = S.DialogStatus.Open }
            }
        }
    ]
}
