// $Revision: 19571 $
// commitIO() fixed by M.Hanusz 20210319
// added valueValidator by MH 20210426
// changed ioOffset behaviour by MH 20211215
// added bUseKeyReleased property by MH 20220111
// changed TextInput Keys.OnReleased by by MH 20220111
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1
import tools.shortcut 1.1

/*!
    \qmltype TextField
    \inqmlmodule ???
    \since ???
    \ingroup ???
    \brief Displays a single line of editable plain text.

    TextField is used to accept a line of text input. Input constraints can be
    placed on a TextField item (for example, through a \l validator or \l
    inputMask). Setting \l echoMode to an appropriate value enables
    TextField to be used for a password input field.
*/

FocusScope {
    id: textfield
    objectName: "AradexTextField"


    /*!
        \qmlproperty bool TextField::bUseKeyReleased

        This property controls the TextInput Keys.OnReleased behaviour

        The default value is \c false.
    */
    property bool bUseKeyReleased: false

    property real __textWidth: textInput.width
    property real __textHeight: textInput.height
	property bool acceptableInput: textInput.acceptableInput
    property alias topValue: valueValidator.topValue
    property alias bottomValue: valueValidator.bottomValue


    signal focusToRoot()

    /*!
        \qmlproperty bool TextField::activeFocusOnPress

        This property is set to \c true if the TextField should gain active
        focus on a mouse press.

        The default value is \c true.
    */
    property alias __activeFocusOnPress: textInput.activeFocusOnPress


    /*!
        \qmlproperty bool TextField::__virtualKeyboardMode

        This property controls the behave of he keyboard when focus is active.
        Possible values are:
        0 : Standard behave, the virtual keyboard is opened when in touch mode
        1 : A signal for opening the overlay application keyboard is emitted
        2 : No signal for opening a keyboard is emmited (for use with embedded application keyboard)

        The default value is \c 0.
    */
    property int __virtualKeyboardMode : 0

    /*!
        \qmlProperty string TextField::textfieldFontSize
        PixelSize of displayed Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontSize: S.Style[__styleName].textfieldFontSize
   


    /*! \internal
        Array holding all Text colors for the Label.
    */
    property variant __fontColors: [
		S.Style[__styleName].fontColor0,
        S.Style[__styleName].fontColor1,
        S.Style[__styleName].fontColor2,
        S.Style[__styleName].fontColor3]
    /*!
        \qmlProperty string TextField::fontColorNormal
        Font color of displayed Text, when TextField is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontColorChoice: 0

    /*! \internal
        Array holding all Text colors for the Label.
    */
    property variant __fontPlaceholderColors: [S.Style[__styleName].multiPlaceholderColor0,
        S.Style[__styleName].multiPlaceholderColor1,
        S.Style[__styleName].multiPlaceholderColor2,
        S.Style[__styleName].multiPlaceholderColor3]
    /*!
        \qmlProperty string TextField::fontPlaceholderNormal
        Font color of displayed placeholder Text. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property int fontPlaceholderColorChoice: 0

    /*!
        \qmlProperty string TextField::fontPlaceholderColor
        Font color of displayed Text when Textfield is inactive. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorInactive: S.Style[__styleName].textfieldFontInactiveColor

    /*!
        \qmlProperty string TextField::fontReadOnlyColor
        Font color of displayed Text when Textfield is in readOnly mode. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorReadOnly: S.Style[__styleName].textfieldFontReadOnlyColor


    /*!
        \qmlProperty string Button::imageNormal
        Background image URL of button, when  active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageEnabled: S.Style[__styleName].imageFolder +
                                  S.Style[__styleName].textfieldImageEnabled
    /*!
        \qmlProperty string Button::imageDisabled
        Background image URL of button, when  disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageDisabled: S.Style[__styleName].imageFolder +
                                   S.Style[__styleName].textfieldImageDisabled
    /*!
        \qmlProperty string Button::imagePressed
        Background image URL of button, while pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageFocus: S.Style[__styleName].imageFolder +
                                S.Style[__styleName].textfieldImageFocus

    /*!
        \qmlProperty string Button::imageReadOnly
        Background image URL of button, while in ReadOnly mode. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */

    property string __imageReadOnly: S.Style[__styleName].imageFolder +
                                   S.Style[__styleName].textfieldImageReadOnly

    /*!
       \qmlproperty string TextField::text

       This property holds the text displayed in the TextField.

       If \l echoMode is set to TextInput::Normal, this holds the
       same value as the TextField::text property. Otherwise,
       this property holds the text visible to the user, while
       the \l text property holds the actual entered text.
    */
    property string text

    onTextChanged: setTextOutput(text)

    /*!
        \qmlproperty enumeration TextField::echoMode

        Specifies how the text should be displayed in the
        TextField.

        The possible modes are:
        \list
        \li TextInput.Normal - Displays the text as it is. (Default)
        \li TextInput.Password - Displays asterisks instead of characters.
        \li TextInput.NoEcho - Displays nothing.
        \li TextInput.PasswordEchoOnEdit - Displays characters as they are
        entered while editing, otherwise displays asterisks.
        \endlist
    */
    property bool passwordInput: false


    /*!
        \qmlproperty font TextField::font.family

        Sets the font of the TextField.
    */
    property alias __fontFamily: textInput.font.family

    /*!
        Sets an input mask on the TextField, restricting the allowable text
        inputs. See QLineEdit::inputMask for further details, as the exact same
        mask strings are used by TextField.

        Character	Meaning
        A	ASCII alphabetic character required. A-Z, a-z.
        a	ASCII alphabetic character permitted but not required.
        N	ASCII alphanumeric character required. A-Z, a-z, 0-9.
        n	ASCII alphanumeric character permitted but not required.
        X	Any character required.
        x	Any character permitted but not required.
        9	ASCII digit required. 0-9.
        0	ASCII digit permitted but not required.
        D	ASCII digit required. 1-9.
        d	ASCII digit permitted but not required (1-9).
        #	ASCII digit or plus/minus sign permitted but not required.
        H	Hexadecimal character required. A-F, a-f, 0-9.
        h	Hexadecimal character permitted but not required.
        B	Binary character required. 0-1.
        b	Binary character permitted but not required.
        >	All following alphabetic characters are uppercased.
        <	All following alphabetic characters are lowercased.
        !	Switch off case conversion.
        \	Use \ to escape the special characters listed above to use them as separators.

        The mask consists of a string of mask characters and separators, optionally followed by a semicolon and the character used for blanks. The blank characters are always removed from the text after editing.

        Examples:
        Mask	Notes
        000.000.000.000;_	IP address; blanks are _.
        HH:HH:HH:HH:HH:HH;_	MAC address
        0000-00-00	ISO Date; blanks are space
        >AAAAA-AAAAA-AAAAA-AAAAA-AAAAA;#	License number; blanks are - and all (alphabetic) characters are converted to uppercase.

        \sa acceptableInput, validator
    */
    property alias __inputMask: textInput.inputMask

    /*!
        \qmlproperty enumeration TextField::inputMethodHints

        Provides hints to the input method about the expected content of the
        text field and how it should operate.

        The value is a bit-wise combination of flags, or \c Qt.ImhNone if no
        hints are set.

        The default value is \c Qt.ImhNone.

        Flags that alter behavior are:

        \list
        \li Qt.ImhHiddenText - Characters should be hidden, as is typically used when entering passwords.
                This is automatically set when setting echoMode to \c TextInput.Password.
        \li Qt.ImhSensitiveData - Typed text should not be stored by the active input method
                in any persistent storage like predictive user dictionary.
        \li Qt.ImhNoAutoUppercase - The input method should not try to automatically switch to upper case
                when a sentence ends.
        \li Qt.ImhPreferNumbers - Numbers are preferred (but not required).
        \li Qt.ImhPreferUppercase - Uppercase letters are preferred (but not required).
        \li Qt.ImhPreferLowercase - Lowercase letters are preferred (but not required).
        \li Qt.ImhNoPredictiveText - Do not use predictive text (for example, dictionary lookup) while typing.

        \li Qt.ImhDate - The text editor functions as a date field.
        \li Qt.ImhTime - The text editor functions as a time field.
        \endlist

        Flags that restrict input (exclusive flags) are:

        \list
        \li Qt.ImhDigitsOnly - Only12igits are allowed.
        \li Qt.ImhFormattedNumbersOnly - Only number input is allowed. This includes decimal point and minus sign.
        \li Qt.ImhUppercaseOnly - Only uppercase letter input is allowed.
        \li Qt.ImhLowercaseOnly - Only lowercase letter input is allowed.
        \li Qt.ImhDialableCharactersOnly - Only characters suitable for phone dialing are allowed.
        \li Qt.ImhEmailCharactersOnly - Only characters suitable for email addresses are allowed.
        \li Qt.ImhUrlCharactersOnly - Only characters suitable for URLs are allowed.
        \endlist

        Masks:
        \list
        \li Qt.ImhExclusiveInputMask - This mask yields nonzero if any of the exclusive flags are used.
        \endlist
    */
//    property int inputMethodHints: Qt.ImhDigitsOnly


    /*!
        \qmlproperty int TextField::maximumLength

        This property holds the maximum permitted length of the text in the
        TextField.

        If the text is too long, it is truncated at the limit.
    */
    property alias __maximumLength: textInput.maximumLength

    /*!
        \qmlproperty int TextField::isEnabled

        This property enables and disables the component.

        The default value is \c true.
    */
    property bool isEnabled: true

    /*!
        \qmlproperty string TextField::placeholderText

        This property contains the text that is shown in the text field when the
        text field is empty and has no focus.
    */
    property string placeholderText: ""


    /*!
        \qmlproperty string TextField::placeholderText

        This property contains the size of the placeholder Text.
    */
    property string __placeholderTextSize: S.Style[__styleName].textfieldPlaceholderTextSize
    /*!
        \qmlproperty bool TextField::readOnly

        Sets whether user input can modify the contents of the TextField. Read-
        only is different from a disabled text field in that the text field will
        appear to be active and text can still be selected and copied.

        If readOnly is set to \c true, then user input will not affect the text.
        Any bindings or attempts to set the text property will still
        work, however.
    */
    property alias readOnly: textInput.readOnly

    /*!
        \qmlproperty bool TextField::selectByMouse
        If true, the user can use the mouse to select text
    */
    property bool selectByMouse : true

    /*!
        \qmlproperty Validator TextField::validator

        Allows you to set a validator on the TextField. When a validator is set,
        the TextField will only accept input which leaves the text property in
        an intermediate state. The accepted signal will only be sent
        if the text is in an acceptable state when enter is pressed.

        Currently supported validators are \l{QtQuick2::IntValidator},
        \l{QtQuick2::DoubleValidator}, and \l{QtQuick2::RegExpValidator}. An
        example of using validators is shown below, which allows input of
        integers between 11 and 31 into the text input:

        \code
        import QtQuick 2.1
        import QtQuick.Controls 1.0

        TextField {
            validator: IntValidator {bottom: 11; top: 31;}
            focus: true
        }
        \endcode

        \sa acceptableInput, inputMask, accepted
    */
    property alias validator: textInput.validator
    /*!
        \qmlproperty enumeration TextField::verticalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li 1                   - align at the top
        \li 2                   - align at the bottom
        \li 4 or anything else  - align in the middle
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __verticalTextAlignement: 4

    /*!
        \qmlproperty enumeration TextField::horizontalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li TextInput.AlignLeft
        \li TextInput.AlignRight
        \li TextInput.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int horizontalTextAlignement:  S.Style[__styleName].textfieldHorizontalAlignement

    /*!
        \qmlProperty bool RadioButton::keyBinding
        Button can get a hotkey like F1 - F12 for different key values
        use \l Qt::KeyHotLinks and insert values without "Qt::Key_SysReq"
        For example:
        If you want to use page down as a hotkey, the list shows
        "Qt::Key_PageDown" and you need to insert "PageDown"
      */
    property string keyBinding

    property int __textBorderLeftSize: S.Style[__styleName].textfieldTextBorderLeftSize
    property int __textBorderRightSize: S.Style[__styleName].textfieldTextBorderRightSize
    property int __textBorderTopSize: S.Style[__styleName].textfieldTextBorderTopSize
    property int __textBorderBottomSize: S.Style[__styleName].textfieldTextBorderBottomSize

    property string __currentText: textInput.text
    signal currentTextChanged()
    /*!
        \qmlsignal TextField::openKeyboard()
    */
    signal openKeyboard(int layoutPage)

    /*!
        \qmlsignal TextField::openAppKeyboard()

        This signal is emitted for opening the embedded application Keyboard
    */
    signal openAppKeyboard()
    signal openKeypad()


    /*!
        \qmlsignal TextField::closeKeyboard()
    */
    signal closeKeyboard()

    /*!
        \qmlsignal TextField::accepted()

        This signal is emitted when the Return or Enter key is pressed.
        Note that if there is a \l validator or \l inputMask set on the text
        field, the signal will only be emitted if the input is in an acceptable
        state.
    */

    signal accepted()
    signal userInput()
    /*!
        \qmlsignal TextField::userInput()

        This signal is emitted when the Return or Enter key is pressed.
        Note that if there is a \l validator or \l inputMask set on the text
        field, the signal will only be emitted if the input is in an acceptable
        state.
    */

    /*!
        \qmlsignal TextField::clicked()
    */
    signal clicked()
//    onClicked: textInput.triggerEvent()
    /*!
        \qmlsignal TextField::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)
    /*!
        \qmlsignal TextField::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*!
        \qmlsignal TextField::scaledSizeChanged(real scaleFactor)

        When an the size of the main window is changed, this signal is emitted from
        c++ main class. A scale factor is given by \a scaleFactor

    */
    signal scaledSizeChanged(real scaleFactor)

    property string textIO
    property variant __textIO: eval(textIO)
    // make sure IO never changes to ""
    onTextIOChanged: {
        if(textIO !== ""){
            __textIO = eval(textIO)
            if (unitEnable && __internal_unit === "" && __textIO.unit !== "") {
                __internal_unit = __textIO.unit;
                setTextOutput();
            }
        }        
    }
    function bUseTextIo() { return (__textIO !== undefined && ( __textIO.type === IoType.IOT_STRING || textIOValue !== "")); }


    function setTextOutput(){
        if (bUseTextIo()) {
            textInput.text = formatTextIOOutput(textIOValue)
        }
        else {
            textInput.text = formatTextOutput(text)
        }
    }


    property variant p_doNotAccept: false
    onP_doNotAcceptChanged: setTextOutput();

    property variant p_onlyNumbers: false
    onP_onlyNumbersChanged: setTextOutput();

    property bool unitEnable: false
    onUnitEnableChanged: setTextOutput();

    /*!
        \qmlProperty string TextField::unit
        A string that is put behind the IO values shown on the text field.
        The default value is the empty string.
    */
    property string unit
    onUnitChanged:{
        __internal_unit = unit;
        setTextOutput();
    }

    /*!
        \qmlProperty string TextField::__internal_unit
        A string that is put behind the IO values shown in the text field.
        The default value is the empty string.
    */
    property string __internal_unit: unit

    /*!
        \qmlProperty bool TextField::substituteCommaWithPoint
        If this property is set a comma is substituted by a point.
        This is useful for german speaking countries.
    */
    property bool substituteCommaWithPoint: true

    property int digitsAfterComma:2
    onDigitsAfterCommaChanged: setTextOutput();

    function getTextIOValue() {
        if (__textIO === undefined) {
            return text;
        }
        else {
            return __textIO.value;
        }
    }

    property variant textIOValue: getTextIOValue()


    property real ioOffset: 0.0
    onIoOffsetChanged: {
        setTextOutput();
    }

    property real ioFactor: 1.0
    onIoFactorChanged: {
        if (0.0 === ioFactor)
        {
            ioFactor = 1.0;
        }
        setTextOutput();
    }

    function isValue(val) {
        if (val === null) { return false;}
        return ( (typeof val === 'number') || (typeof val === 'string') || (typeof val === 'boolean'));
    }

    function trim(text){
        if (!isValue(text)) {
            return "";
        }
        return text.toString().replace(/^\s+|\s+$/g, '');

    }
//MH-- format text input coming from textIO
    function formatTextIOOutput(rawTextPara){
        if (!isValue(rawTextPara)) {
            if (__textIO.type !== IoType.IOT_STRING) {
                return "";
            }
            return rawTextPara;
        }

        var text
        var rawText = rawTextPara // use internal variable to avoid warning when rawText is changed
        var unitText = ""

        // remove unit if given
        if (unitEnable == true){
            if(__internal_unit !== ""){
                rawText = rawText.toString().replace(__internal_unit, "")
                unitText = " " + __internal_unit;
            }
        }
        rawText = trim(rawText)// remove whitespaces

        if (bUseTextIo()){

            if (    __textIO.type === IoType.IOT_DOUBLE
                ||  __textIO.type === IoType.IOT_INTEGER
            ){
                if (!isNaN(rawText))
                {
                    text = (((Number(rawText)+ioOffset)*ioFactor)).toFixed(digitsAfterComma) + unitText
                 }
                else {
                    text = "0" + unitText
                }
            }
            else {
                text = rawText + unitText
            }
            return text
        }
        else{
            return rawText + unitText
        }
    }

    function formatTextOutput(rawTextPara){
        if (!isValue(rawTextPara)) {
            if (__textIO.type !== IoType.IOT_STRING) {
                return "";
            }
            return rawTextPara;
        }
        var text
        var rawText = rawTextPara // use internal variable to avoid warning when rawText is changed
        var unitText = ""

        // remove unit if given
        if (unitEnable == true){            
            if(__internal_unit !== ""){
                rawText = rawText.toString().replace(__internal_unit, "")
                unitText = " " + __internal_unit;
            }
        }
        rawText = trim(rawText)// remove whitespaces

        if (bUseTextIo()){

            if (    __textIO.type === IoType.IOT_DOUBLE
                ||  __textIO.type === IoType.IOT_INTEGER
            ){
                if (!isNaN(rawText))
                {
                    //MH-- text = ((Number(rawText)*ioFactor)+ioOffset).toFixed(digitsAfterComma) + unitText
                    text = ((Number(rawText)*ioFactor)).toFixed(digitsAfterComma) + unitText
                }
                else {
                    text = "0" + unitText
                }
            }
            else {
                text = rawText + unitText
            }
            return text
        }
        else{
            return rawText + unitText
        }
    }

    /*!
        \qmlmethod TextField::copy()

        Copies the currently selected text to the system clipboard.
    */
    function copy() {
        textInput.copy()
    }

    /*!
        \qmlmethod TextField::cut()

        Moves the currently selected text to the system clipboard.
    */
    function cut() {
        textInput.cut()
    }

    /*!
        \qmlmethod TextField::deselect()

        Removes active text selection.
    */
    function deselect() {
        textInput.deselect();
    }

    /*!
        \qmlmethod string TextField::getText(int start, int end)

        Removes the section of text that is between the \a start and \a end
        positions from the TextField.
    */
    function getText(start, end) {
        return textInput.getText(start, end);
    }

    /*!
        \qmlmethod TextField::insert(int position, string text)

        Inserts \a text into the TextField at \a position.
    */
    function insert(position, text) {
        textInput.insert(position, text);
    }

    /*!
        \qmlmethod bool TextField::isRightToLeft(int start, int end)

        Returns \c true if the natural reading direction of the editor text
        found between positions \a start and \a end is right to left.
    */
    function isRightToLeft(start, end) {
        return textInput.isRightToLeft(start, end);
    }

    /*!
        \qmlmethod TextField::paste()

        Replaces the currently selected text by the contents of the system
        clipboard.
    */
    function paste() {
        textInput.paste()
    }

    /*!
        \qmlmethod TextField::redo()

        Performs the last operation if redo is \l {canRedo}{available}.
    */
    function redo() {
        textInput.redo();
    }

    /*!
        \qmlmethod TextField::select(int start, int end)

        Causes the text from \a start to \a end to be selected.

        If either start or end is out of range, the selection is not changed.

        After calling select, selectionStart will become the lesser
        and selectionEnd will become the greater (regardless of the order passed
        to this method).

        \sa selectionStart, selectionEnd
    */
    function select(start, end) {
        textInput.select(start, end)
    }

    /*!
        \qmlmethod TextField::selectAll()

        Causes all text to be selected.
    */
    function selectAll() {
        textInput.selectAll()
    }

    /*!
        \qmlmethod TextField::selectWord()

        Causes the word closest to the current cursor position to be selected.
    */
    function selectWord() {
        textInput.selectWord()
    }

    /*!
        \qmlmethod TextField::undo()

        Reverts the last operation if undo is \l {canUndo}{available}. undo()
        deselects any current selection and updates the selection start to the
        current cursor position.
    */
    function undo() {
        textInput.undo();
    }

    function clickedAction(){
        clicked()
    }
    function disable(){
        textInput.deselect()
        textfield.isEnabled = false
    }
    function enable(){
        textfield.isEnabled = true
    }


    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    /*! \internal
        The scale Factor is always initialized with 1.
    */
    property real __scale: 1

    width: S.Style.textfieldWidthInitial
    height: S.Style.textfieldHeightInitial
    clip: true
    enabled: isEnabled

    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    onScaledSizeChanged: {
       __scale = scaleFactor
    }

    property bool bChangeByUserInput: false

    onAccepted: {
        bChangeByUserInput = true
        commitIO(textInput.text)
        userInput()
    }

    function commitIO(newText){
        if(p_doNotAccept == true){
            text = textIOValue
            textInput.text = formatTextOutput(text)
            return
        }

        if(p_onlyNumbers === true && isNaN(newText)){
            text = textIOValue
            textInput.text = formatTextOutput(text)
            return
        }
        if (bUseTextIo()){
            switch (__textIO.type){
                case IoType.IOT_DOUBLE:{
                    if(!isNaN(newText)){
                        //MH--org: __textIO.value =  ((parseFloat(newText)/ioFactor)-ioOffset)
                        //MH--text =  ((parseFloat(newText)/ioFactor)-ioOffset)
                        text =  ((parseFloat(newText)/ioFactor))
                        __textIO.value = ((parseFloat(newText)/ioFactor)-ioOffset)
                        //MH--org: text = __textIO.value
                    }
                    break;
                }
                case IoType.IOT_INTEGER:{
                    if(!isNaN(newText)){
                        //MH--org: text =  ((parseInt(newText)/ioFactor)-ioOffset)
                        text =  ((parseInt(newText)/ioFactor))
                        //MH--org: __textIO.value = text
                        __textIO.value =  ((parseInt(newText)/ioFactor)-ioOffset)
                    }
                    break;
                }
                case IoType.IOT_BOOLEAN:{
                    text = newText
                    __textIO.value = text
                    break;
                }
                case IoType.IOT_STRING:{
                    text = newText
                    __textIO.value = text
                    break;
                }
            }            
        }
        else
        {
            if( unitEnable === true ) {
                if(__internal_unit !== ""){
                    newText = newText.replace( __internal_unit, "")
                }
            }
            text = newText
        }
        textInput.text = formatTextOutput(text)
    }


    onTextIOValueChanged: {
        if(bChangeByUserInput){ bChangeByUserInput = false; return }
        if(!textfield.activeFocus && (__textIO !== undefined) ) {
            //MH--text = formatTextOutput(textIOValue)
            text = formatTextIOOutput(textIOValue)
			textInput.text = text
		}
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        z: textfield.activeFocus === true ? - 2 : 2

        onPressed: {
            if (enabled && !readOnly)
            {
            textInput.forceActiveFocus()
            }
        }
    }

    BorderImage {
        id: textfieldImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].textfieldBorderLeftSize
            top: S.Style[__styleName].textfieldBorderTopSize
            right: S.Style[__styleName].textfieldBorderRightSize
            bottom: S.Style[__styleName].textfieldBorderBottomSize
        }
        source: __imageEnabled
        visible: !readOnly && enabled
    }
    BorderImage {
        id: textfieldDisabledImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].textfieldBorderLeftSize
            top: S.Style[__styleName].textfieldBorderTopSize
            right: S.Style[__styleName].textfieldBorderRightSize
            bottom: S.Style[__styleName].textfieldBorderBottomSize
        }
        source: __imageDisabled
        opacity: !enabled ? S.Style[__styleName].textfieldDisabledOpacity : 0
    }
    BorderImage {
        id: textfieldReadOnlyImage
        anchors.fill: textfield
        border{
            left: S.Style[__styleName].textfieldBorderLeftSize
            top: S.Style[__styleName].textfieldBorderTopSize
            right: S.Style[__styleName].textfieldBorderRightSize
            bottom: S.Style[__styleName].textfieldBorderBottomSize
        }
        source: __imageReadOnly
        opacity: (readOnly && enabled) ? S.Style[__styleName].textfieldReadOnlyOpacity : 0
    }
    BorderImage {
        id: textfieldFocusImage
        anchors.fill: textfield
        opacity: (textfield.activeFocus && S.focusOn) ? S.Style[__styleName].textfieldFocusOpacity : 0

        anchors{
            leftMargin: S.Style[__styleName]. textfieldFocusShiftLeft
            topMargin:  S.Style[__styleName]. textfieldFocusShiftRight
            rightMargin: S.Style[__styleName]. textfieldFocusShiftTop
            bottomMargin: S.Style[__styleName]. textfieldFocusShiftBottom
        }
        border{
            left: S.Style[__styleName]. textfieldFocusBorderLeft
            right: S.Style[__styleName]. textfieldFocusBorderRight
            top: S.Style[__styleName]. textfieldFocusBorderTop
            bottom: S.Style[__styleName]. textfieldFocusBorderBottom
        }
        source: __imageFocus
    }

    Item {
        id: textContainer
        anchors{
            fill: parent
            leftMargin: __textBorderLeftSize
            rightMargin: __textBorderRightSize
            topMargin: __textBorderTopSize
            bottomMargin: __textBorderBottomSize
        }
        clip:true


        TextInput {
            id: textInput

            onActiveFocusChanged: {
                if(activeFocus === true){
                    errorplaceholderOn = false
                    if( unitEnable === true ) {
                        if(__internal_unit !== ""){
                            text = text.substring(0, text.length - __internal_unit.length -1)
                        }
                    }
                    selectAll()
                    switch (__virtualKeyboardMode) {
                    case 0: if ((__textIO === undefined || __textIO.type === IoType.IOT_STRING) && !textfield.p_onlyNumbers) {
                            openKeyboard(0) // show alphanumeric page
                         } else {
                            openKeyboard(1) // show numeric page
                        }
                        break
                    case 1: openAppKeyboard(); break
                    case 2: openKeypad(); break
                    default: break;
                    }
                }
                else {
                    closeKeyboard()
                    accepted()
                    userInput()
                }
            }

            objectName: "TextFieldTextInput"
            enabled: textfield.enabled
            signal languageChanged()
            property int fontSize: S.Style[__styleName].textfieldFontSize
            property real realInputWidth: textInput.width/ __scale
            property real realInputHeight: textInput.height / __scale
            property bool placeholderOn: (textInput.text.length === 0 && textInput.enabled && !textInput.readOnly)
            property bool errorplaceholderOn: false

            selectByMouse: textfield.selectByMouse
            selectionColor: S.Style[__styleName].textfieldSelectionColor
            selectedTextColor: isEnabled ? S.Style[__styleName].textfieldSelectedTextColor : "transparent"

            font.family: S.Style[__styleName].fontFamily
            font.pointSize: (textfield.fontSize * __scale) > 0 ? (textfield.fontSize * __scale) : 1

            anchors.horizontalCenter:  textContainer.horizontalCenter
            anchors.horizontalCenterOffset:{
                   ((realInputWidth - textContainer.width)/2)
            }

            horizontalAlignment: horizontalTextAlignement
            anchors.verticalCenter: textContainer.verticalCenter
            anchors.verticalCenterOffset: {
                if(__verticalTextAlignement == 1){
                    ((realInputHeight - textContainer.height)/2)
                }else if(__verticalTextAlignement == 2){
                     -(((realInputHeight - textContainer.height)/2) + 1)
                }else{
                    0
                }
            }
            width: ((textContainer.width) * __scale) - 2


            color:{
                if(enabled){
                    if(readOnly){
                        __fontColorReadOnly
                    }else{
                        __fontColors[fontColorChoice]
                    }
                }else{
                    __fontColorInactive
                }
            }
            focus : true
            clip: true
            onAccepted: {
                if( unitEnable === true ) {
                    if(__internal_unit !== ""){
                        text = text.replace( __internal_unit, "")
                    }
                }
                if (substituteCommaWithPoint)
                {
                    text = text.replace( ",", ".")
                }
                if(valueValidator.validate(text))
                    textfield.accepted()
                else
                {
                    errorplaceholder.text = text
                    text = textIOValue
                    textInput.text = formatTextIOOutput(text)
                    errorplaceholderOn = true
                }
            }
            echoMode: (textfield.passwordInput === true) ? TextInput.Password : TextInput.Normal
            activeFocusOnPress: enabled && !readOnly
            text: textfield.text
            onTextChanged: textfield.currentTextChanged()

            function commitKeyPressed () {
                textfield.focus = false
                //focusToRoot()
                //accepted()
                textfield.parent.forceActiveFocus()
            }

            Keys.onReturnPressed: commitKeyPressed()
            Keys.onEnterPressed: commitKeyPressed()
            Keys.onReleased: if (bUseKeyReleased) textfield.text = text
            opacity: textInput.errorplaceholderOn ?  0 : textfield.opacity
        }
        TextInput {
            id: placeholderLabel
            objectName: "TextFieldText"
            signal languageChanged()
            property int fontSize: S.Style[__styleName].textfieldFontSize
            property int leftBorderSize: S.Style[__styleName].textfieldBorderLeftSize
            property int rightBorderSize: S.Style[__styleName].textfieldBorderRightSize
            property int topBorderSize: S.Style[__styleName].textfieldBorderTopSize
            property int bottomBorderSize: S.Style[__styleName].textfieldBorderBottomSize
            property real realInputWidth: placeholderLabel.width / __scale
            property real realInputHeight: placeholderLabel.height / __scale
            selectByMouse: textfield.selectByMouse
            enabled: false

            selectionColor: S.Style[__styleName].textfieldSelectionColor

            font.family: textInput.font.family
            font.pointSize: (__placeholderTextSize  * __scale) > 0 ? (__placeholderTextSize  * __scale) : 1

            anchors.horizontalCenter:  textContainer.horizontalCenter
            anchors.horizontalCenterOffset:{
                   ((realInputWidth - textContainer.width)/2)
            }

            horizontalAlignment: horizontalTextAlignement
            anchors.verticalCenter: textContainer.verticalCenter
            anchors.verticalCenterOffset: {
                if(__verticalTextAlignement == 1){
                    ((realInputHeight - textContainer.height)/2)
                }else if(__verticalTextAlignement == 2){
                     - ((realInputHeight - textContainer.height)/2)
                }else{
                    0
                }
            }
            width: (textContainer.width * __scale) - 2

            color: __fontPlaceholderColors[fontPlaceholderColorChoice]

            clip: true
            text: placeholderText
            opacity: textInput.errorplaceholderOn ? 0 : textInput.placeholderOn ? S.Style[__styleName].textfieldPlaceholderOpacity : 0

        }
        TextInput {
            id: errorplaceholder
            signal languageChanged()

            selectByMouse: textfield.selectByMouse
            enabled: false

            font.family:textInput.font.family
            font.pointSize: textInput.font.pointSize

            anchors.horizontalCenter:  textInput.horizontalCenter
            anchors.horizontalCenterOffset:textInput.anchors.horizontalCenterOffset
            horizontalAlignment: textInput.horizontalTextAlignement
            anchors.verticalCenter: textInput.verticalCenter
            anchors.verticalCenterOffset:textInput.anchors.verticalCenterOffset

            width: textInput.width
            color: textInput.color
            clip: true

            opacity: textInput.errorplaceholderOn ?  1 : 0
        }

    }

    Rectangle{
        id: errorplaceholderrect
        anchors.fill: parent
        color:"transparent"
        border.color: "red"
        border.width: 5
        //opacity: 0.3
        visible: textInput.errorplaceholderOn
    }

    Item{
        id:valueValidator
        property real topValue
        property real bottomValue

        function validate(newText){
            var inputNumber
            if(textfield.p_doNotAccept == true){
                return true
            }
            if(textfield.p_onlyNumbers === true && isNaN(newText)){
                return true
            }
            if (textfield.bUseTextIo()){
                if( topValue === bottomValue ) return true
                switch (textfield.__textIO.type){
                case IoType.IOT_DOUBLE:
                    if(!isNaN(newText)){
                        inputNumber =  parseFloat(newText)
                        if( inputNumber >= bottomValue && inputNumber <= topValue) return true
                        return false
                    }
                    else return false

                case IoType.IOT_INTEGER:
                    if(!isNaN(newText)){
                        inputNumber =  parseInt(newText)
                        if( inputNumber >= bottomValue && inputNumber <= topValue) return true
                        return false
                    }
                    else return false

                case IoType.IOT_BOOLEAN:
                    return true

                case IoType.IOT_STRING:
                    return true

                }
            }
            else if( topValue === bottomValue ) return true
            else
            {
                inputNumber =  parseFloat(newText)
                if( inputNumber >= bottomValue && inputNumber <= topValue) return true
                return false
            }
        }
    }

}
