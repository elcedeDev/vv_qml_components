// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1

FocusScope {
    id: root
    objectName: "AradexLinearScale"

    implicitHeight: S.Style.linearHeightInitial
    implicitWidth: S.Style.linearWidthInitial
	property string __smooth: S.isFast
    property real __aspectFit: 4
    property bool __isHorizontal: (rotation === 90 || rotation === 270) ? true : false


    /*! \internal
        \qmlProperty string LinearScale::__minText
        Minimum text */
    property string __minText:(__startStopLabel === true && Math.abs(__angleSpan === 360))?
                                  formatNumber((maxValue),__scientificNumScale ,digitsAfterCommaScale).toString().replace(" ", "") +
                                  formatNumber((minValue), __scientificNumScale,digitsAfterCommaScale).toString().replace(" ", "")  :
                                  formatNumber(maxValue, __scientificNumScale, digitsAfterCommaScale)
    /*! \internal
        \qmlProperty string LinearScale::__maxText
        Maximum text. */
    property string __maxText: formatNumber(minValue, __scientificNumScale, digitsAfterCommaScale)

    /*! \internal
        \qmlProperty real LinearScale::__maxLabelSize
         Upper Estimate of the label width. */
    property real __maxLabelSize: Math.max(__minText.length, __maxText.length) * __fontPointSize* 0.70

    /*! \internal
        \qmlProperty string LinearScale::__minTextText 
	*/
    property string __minTextText:(__startStopLabel === true && Math.abs(__angleSpan === 360))?
                                  formatNumber((maxValue),__scientificNumText ,digitsAfterCommaText).toString().replace(" ", "") +
                                  formatNumber((minValue), __scientificNumText,digitsAfterCommaText).toString().replace(" ", "")  :
                                  formatNumber(maxValue, __scientificNumText, digitsAfterCommaText)
    /*! \internal
        \qmlProperty string LinearScale::__maxTextText */
    property string __maxTextText: formatNumber(minValue, __scientificNumText, digitsAfterCommaText)

    /*! \internal
        \qmlProperty real LinearScale::__maxLabelTextSize */
    property real __maxLabelTextSize: Math.max(__minTextText.length, __maxTextText.length) * __valueTextPointSize * 0.70

    //
    //  Public Properties
    //

    /*!
        \qmlProperty bool LinearScale::animationOn
        \brief Animation to smooth the appearance of a value Change.
        If set to true the meter will display intermediate values on a value change
        instead of just jumping from value A to value B. There is also a dampening effect.
        The default value is \c false. */
    property bool animationOn: false

    /*!
        \qmlProperty bool LinearScale::text_enable
        The default value is \c false. */
    property bool text_enable: false

    /*! \internal
        \qmlProperty bool LinearScale::__startStopLabel
        \brief If True first label
        The default value is \c false. */
    property bool __startStopLabel: false
    
	/*!
        \qmlProperty int LinearScale::digitsAfterCommaScale
        \brief Number of Ddecimal places for tick marks.
        Number of Ddecimal places for tick marks.
        The default value is \c 2. */
    property int digitsAfterCommaScale: 2
    
	/*!
        \qmlProperty int LinearScale::digitsAfterCommaText
        \brief Number of Ddecimal places for text.
        Number of Ddecimal places for text.
        The default value is \c 2. */
    property int digitsAfterCommaText: 2
    
	/*!
        \qmlProperty real LinearScale::initialValue
        \brief Initial value.
	Is only relevant if no meterIO is set.
        The default value is \c 0. */
    property real initialValue: 0
    
	/*! \internal
        \qmlProperty int LinearScale::__labelShift
        \brief Additional spacing for tick mark text in pixels.
        Additional spacing for tick mark text in pixels.
        The default value is \c S.Style[__styleName].linearScaleLabelShift . */
    property int __labelShift: S.Style[__styleName].linearScaleLabelShift
    
	/*! \internal
        \qmlProperty real LinearScale::__labelStepSize
        \brief Only every __labelStepSize-th tick mark is supplied with text. */
    property real __labelStepSize: 1
    
	/*! \internal
        \qmlProperty bool LinearScale::__turningScale
        \brief The scale turns instead of the clock hand.
        The default value is \c false.
    */
    property bool __turningScale: false
    
	/*! \internal
        \qmlProperty int LinearScale::__overshootAnimationDuration
        \brief Duration of the overshoot animation in milliseconds.
        The default value is \c 200. */
    property int __overshootAnimationDuration: 200


    /*!
        \qmlProperty real Meter360::ioFactor
        \brief A factor the meterIO values will be multiplied with.
        The default value is \c 1.  */
    property real ioFactor: 1
    
	/*!
        \qmlProperty real Meter360::ioOffset
        \brief An offset that will be added to the meterIO value.
        The default value is \c 0.  */
	property real ioOffset: 0
    
	/*! \internal
        \qmlProperty int LinearScale::__majorTickNum
        \brief Specifies the number of Major Tick Marks*/
    property int __majorTickNum: Math.abs(__valueSpan/ stepSize)
    
	/*!
        \qmlProperty real LinearScale::stepSize
        \brief Specifies the size of one tick step.
        The default value is \c 1. */
    property real stepSize: 1
    
	/*!
        \qmlProperty int LinearScale::minorSteps
        \brief Specifies how many minor tics to put between two major ticks.
        The default value is \c 3 */
    property int minorSteps: 3
    
	/*!
        \qmlProperty real LinearScale::maxValue
        \brief Maximum value of the scale.
        The default value is \c 1. */
    property real maxValue: 1

    /*! \internal
        \qmlProperty bool LinearScale::__scientificNumScale
        \brief If true the major tick Labels will be given in scientific notation.
        The default value is \c false. */
    property bool __scientificNumScale: false
    
	/*! \internal
        \qmlProperty bool LinearScale::__scientificNumText
        \brief If true the central label will be given in scientific notation.
        The default value is \c false. */
    property bool __scientificNumText: false
    
	/*!
        \qmlProperty variant LinearScale::value
        \brief The value the scale shows */
    property variant value: (__valueIO !== undefined) ? ioOffset +(ioFactor *__valueIO.value) : initialValue
    
	/*! \internal
        \qmlProperty variant LinearScale::__valueIO
        \brief The IO objectwhose value the scale shows. */
    property variant __valueIO: eval(valueIO)
    
	/*!
        \qmlProperty string LinearScale::valueIO
        \brief The name of an IO whose value the scale shows. */
	property string valueIO
    onValueIOChanged: {
        if (valueIO !== ""){
            __valueIO = eval(valueIO)
        }
    }

    //
    //  Images
    //

    /*! \internal
        \qmlProperty string LinearScale::scaleBigImage
        \brief Source of a big scale mark.´
        The default value is \c S.Style[__styleName].scaleBigImage. */
    property string __scaleBigImage: (S.Style[__styleName].linearScaleBigImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].linearScaleBigImage : ""
    
	/*! \internal
        \qmlProperty string LinearScale::scaleSmallImage
        \brief ource of a small scale mark.
        The default value is \c S.Style[__styleName].scaleSmallImage. */
    property string __scaleSmallImage: (S.Style[__styleName].linearScaleSmallImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].linearScaleSmallImage : ""
    
	/*! \internal
        \qmlProperty string LinearScale::backgroundImage
        \brief ource of a background image.
        The default value is \c S.Style[__styleName].backgroundImage. */
    property string __backgroundImage: (S.Style[__styleName].linearBackgroundImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].linearBackgroundImage : ""
    
	/*!
        \qmlProperty string LinearScale::overlayImage
        \brief ource of a overlay Image.
        The default value is \c S.Style[__styleName].overlayImage. */
    property string __overlayImage: (S.Style[__styleName].linearOverlayImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].linearOverlayImage : ""
    
	/*! \internal
        \qmlProperty string LinearScale::handImage
        \brief Source of a clock hand.
        The default value is \c S.Style[__styleName].handImage. */
    property string __handImage: (S.Style[__styleName].linearHandImage !== "") ? S.Style[__styleName].imageFolder + S.Style[__styleName].linearHandImage : ""


    //
    //  Private Properties
    //
	
    /*! \internal
        \qmlProperty int LinearScale::__actualSmallscaleCount
        \brief Number of small scales including invisible ones. */
    property int __actualSmallscaleCount: (minorSteps + 1)* __majorTickNum


    property real __currentHandAngle: 0
    property real __valueSpan: Math.abs(maxValue - minValue)
    property real minValue: 0
    property string __fontFamily:S.Style[__styleName].linearFontFamily
    property string __fontColorMiddleText: S.Style[__styleName].linearFontColorMiddleText
    property string __fontColorLabelText: S.Style[__styleName].linearFontColorLabelText
    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change. */
    property string __styleName: S.styleName
    property int __fontPointSize: S.Style[__styleName].linearFontPointSize
    property bool __fontBold: S.Style[__styleName].linearFontBold

    property alias __edgeLength: background.__edgeLength

    property real __shownValue: (__tooSmall)? minValue : (__tooBig ? maxValue : Number(value))
    property bool __tooBig: stepSize > 0 ? (value > maxValue): (value < maxValue)
    property bool __tooSmall: stepSize > 0 ? value < minValue : value > minValue
    property bool __outOfRange: __tooBig || __tooSmall
    property bool __completed: false
    property int __valueTextPointSize: S.Style[__styleName].linearValueTextPointSize
    property real __valueTextVerticalOffsetFactor: S.Style[__styleName].linearTextVerticalOffsetFactor
	
	property int __maxMajorScaleStepsToDraw: Math.max(height, width) / 10 
	property int __maxMinorScaleStepsToDraw: Math.max(height, width) / 3

    Component.onCompleted: __completed = true

    onValueChanged: {

        if(__tooBig === true && animationOn === true){
            overshootanimation.start()
        }else if(__tooSmall === true && animationOn === true){
            undershootanimation.start()
        }
    }
    function repeatString(num, stringToRepeat) {
        return new Array(isNaN(num)? 1 : ++num).join(stringToRepeat);
    }
    function formatNumber(num, scientific, digits){
        if(scientific === true){
            return Number(num).toExponential(digits)
        }else{
            return Number(num).toFixed(digits)
        }
    }
    property real __scaleYStart: S.Style[__styleName].linearYStartFactor * __edgeLength
    property real __scaleYStop: S.Style[__styleName].linearYStopFactor * __edgeLength
    property real __scaleYRange: (__scaleYStop - __scaleYStart)
    property real __scaleXStart: S.Style[__styleName].linearXStartFactor * __edgeLength
    property real __smallScaleXStart: S.Style[__styleName].linearSmallScaleXStart * __edgeLength
    property real __bigScaleXStart: S.Style[__styleName].linearBigScaleXStart * __edgeLength

    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    signal scaledSizeChanged(real scaleFactor)


	Item {
		id: background

		property real __buffer: __maxLabelSize
		property real originX: width / 2
		property real originY: height / 2

		anchors.top: parent.top
		anchors.left: parent.left
		anchors.leftMargin: __buffer + bigscales.bigscaleWidth * (S.Style[__styleName].linearTextVerticalOffsetFactor - 0.5)
		property real __edgeLength: Math.min((root.width - __maxLabelSize - __maxLabelTextSize/2) * root.__aspectFit , root.height)
		height: __edgeLength
		width: __edgeLength / __aspectFit

		smooth: S.isFast

		Image {
			id: verticalline
			source: __backgroundImage
			y: __scaleYStart
			x: __scaleXStart -bigscales.bigscaleWidth/ 3
			height: __scaleYRange
			width: S.Style[__styleName].linearVerticalLineWidthFactor * __edgeLength
			smooth: __smooth
			sourceSize{
				height: height
				width: width
			}
		}

		Text{
			id: shownText
			font.pointSize: __valueTextPointSize > 0 ? __valueTextPointSize : 1
			font.bold: true
			font.family: __fontFamily
			color: __fontColorMiddleText
			text: formatNumber(Number(value),__scientificNumText ,digitsAfterCommaText)
			horizontalAlignment: Text.AlignHCenter
			anchors.horizontalCenter: parent.right
			anchors.bottom: parent.bottom
			rotation: - root.rotation
			opacity: text_enable
		}

		Repeater {
			id: bigscales
			property real bigscaleWidth: S.Style[__styleName].linearBigScaleWidthFactor * __edgeLength
			property real bigscaleHeight: S.Style[__styleName].linearBigScaleHeightFactor * __edgeLength                
			model: Math.min(__majorTickNum + 1, __maxMajorScaleStepsToDraw)
			delegate: Image {
				width: bigscales.bigscaleWidth
				height: bigscales.bigscaleHeight
				source: __scaleBigImage
				y: __scaleYStart + (__scaleYRange * index /(bigscales.model -1)) - height/2
				x: __bigScaleXStart - width/2
				opacity: 1
				smooth: __smooth
				sourceSize{
					height: height
					width: width
				}
			}
		}

		Repeater {
			id: smallscales
			property real smallscaleWidth: S.Style[__styleName].linearSmallScaleWidthFactor * __edgeLength
			property real smallscaleHeight: S.Style[__styleName].linearSmallScaleHeightFactor * __edgeLength
			model: Math.min(__actualSmallscaleCount, __maxMinorScaleStepsToDraw)
			delegate: Image {
				width: smallscales.smallscaleWidth
				height: smallscales.smallscaleHeight
				opacity: (index%(minorSteps + 1)=== 0) ? 0 : 1
				source: __scaleSmallImage
				y: __scaleYStart + (__scaleYRange * index /smallscales.model) - height/2
				x: __smallScaleXStart - width/4
				smooth: __smooth
				sourceSize{
					height: height
					width: width
				}
			}
		}
		
		Repeater {
			id: scalelabels
			model: Math.min(__majorTickNum + 1, __maxMajorScaleStepsToDraw)
			delegate: Text {
				id: tester
				font.family: __fontFamily
				font.pointSize: __fontPointSize > 0 ? __fontPointSize : 1
				font.bold: __fontBold
				color: __fontColorLabelText
				y: __scaleYStart + (__scaleYRange * index /(bigscales.model -1)) +
				   ((__isHorizontal === true) ? - height/ 2 :- height/2 )
				x: __scaleXStart  - bigscales.bigscaleWidth * S.Style[__styleName].linearTextVerticalOffsetFactor +
				   ((__isHorizontal === true) ? - width : - width )
				rotation: - root.rotation
				opacity: (index%__labelStepSize != 0) ? 0 : 1
				text: formatNumber((Math.min(minValue, maxValue) + index * stepSize), __scientificNumScale, digitsAfterCommaScale).toString()
			}
		}

		Image {
			id: meterHand
			width: S.Style[__styleName].linearHandWidthFactor * __edgeLength
			height: S.Style[__styleName].linearHandHeightFactor * __edgeLength
			source: __handImage
			y: __scaleYStart + (__scaleYRange * (__shownValue - minValue)/__valueSpan) - height/2
			x: __scaleXStart + bigscales.bigscaleWidth * S.Style[__styleName].linearHandVerticalOffsetFactor
			sourceSize{
				height: height
				width: width
			}

			opacity: 1
			smooth: true
		}

	}
	SequentialAnimation{
		id: overshootanimation

		NumberAnimation{
			target: root

			easing.type: Easing.InSine
			property: "__shownValue"
			duration: 200
			to: root.__shownValue * 1.005
		}
		NumberAnimation{
			target: root
			property: "__shownValue"
			easing.type: Easing.InSine
			duration: 200
			to: root.__shownValue
		}
	}
	
	SequentialAnimation{
		id: undershootanimation
		NumberAnimation{
			target: root
			easing.type: Easing.InSine
			property: "__shownValue"
			duration: 200
			to: root.__shownValue / 0.996
		}
		NumberAnimation{
			target: root

			property: "__shownValue"
			easing.type: Easing.InSine
			duration: 200
			to: root.__shownValue
		}
	}
	
	Behavior on __shownValue {
		enabled: (animationOn) ? true : false
		SpringAnimation {
			spring: 1.4
			damping: 0.15
		}
	}
}
