/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1
import vectovision 1.1
import "../../style/Style.js"  as S

SwipeArea{
    id: scrollBar

    isKeyboardBackground: false
    objectName: "AradexBrowserScrollbar"

    property variant __colors: [
        S.Style[__styleName].fontColor0,
        S.Style[__styleName].fontColor1,
        S.Style[__styleName].fontColor2,
        S.Style[__styleName].fontColor3]

    property int colorChoice: 0




    //    onPressed: console.log("P X/Y:    " +  mouseX + "/" + mouseY + "/" + position() + " / " + scrollArea.contentY)
    onClicked: {
//        console.log("MOVE   width: " + width + " contentWidth: " + scrollArea.contentWidth + "Goal: " + (mouseX/width * scrollArea.contentWidth - width))
        if (scrollBar.orientation == Qt.Vertical){
            scrollArea.contentY = Math.max((mouseY/height * scrollArea.contentHeight) -height, 0)
        }else{
            scrollArea.contentX = Math.max((mouseX/width * scrollArea.contentWidth) - width, 0)
        }
    }
    signal styleChanged(string strStyle)
    property string __styleName: S.styleName
    property bool __smooth: S.isFast
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    signal scaledSizeChanged(real scaleFactor)
    property variant scrollArea
    property int orientation: Qt.Vertical
    property real dragStart: 0

    opacity: {
        if (scrollBar.orientation == Qt.Vertical){
            return scrollArea.contentHeight > height
        }else{
            return scrollArea.contentWidth > width
        }
    }
    onPressed: (scrollBar.orientation == Qt.Vertical) ? dragStart = scrollArea.contentY : dragStart = scrollArea.contentX
    onDrag: {
        if (scrollBar.orientation == Qt.Vertical){
            var maxPos = scrollArea.contentHeight - scrollArea.height
            var newPos = dragStart +(dy/scrollArea.height * scrollArea.contentHeight)
            scrollArea.contentY = Math.max(0, Math.min(maxPos, newPos))
            scrollArea.returnToBounds()


        }else{
            var maxPos = scrollArea.contentWidth - scrollArea.width
            var newPos = dragStart + (dx/scrollArea.width * scrollArea.contentWidth)
            scrollArea.contentX = Math.max(0, Math.min(maxPos, newPos))
            scrollArea.returnToBounds()
        }
    }

    function position()
    {
        var ny = 0;
        var maxPos = 0;
        if (scrollBar.orientation == Qt.Vertical) {
            ny = scrollArea.visibleArea.yPosition * scrollBar.height;
            maxPos = scrollArea.height - scrollBar.width
        }
        else {
            ny = scrollArea.visibleArea.xPosition * scrollBar.width;
            maxPos = scrollArea.width - scrollBar.height
        }
        ny = Math.max(Math.min(ny, maxPos),2);
        return ny;
    }

    function size()
    {
        var nh, ny;

        if (scrollBar.orientation == Qt.Vertical){
            nh = scrollArea.visibleArea.heightRatio * scrollBar.height;
        }
        else {
            nh = scrollArea.visibleArea.widthRatio * scrollBar.width;
        }

        if (scrollBar.orientation == Qt.Vertical) {
            ny = scrollArea.visibleArea.yPosition * scrollBar.height;
        }
        else {
            ny = scrollArea.visibleArea.xPosition * scrollBar.width;
        }

        if (ny > 3) {
            var t;
            if (scrollBar.orientation == Qt.Vertical)
                t = Math.ceil(scrollBar.height - 3 - ny);
            else
                t = Math.ceil(scrollBar.width - 3 - ny);
            if (nh > t) return t; else return nh;
        } else return nh + ny;
    }


        Rectangle{
            id: scrollRect

            radius: scrollBar.orientation == Qt.Vertical ? width/2 : height/2
            x: scrollBar.orientation == Qt.Vertical ? 2 : position()
            width: scrollBar.orientation == Qt.Vertical ? scrollBar.width - 4 : Math.max(size(), scrollBar.height)
            y: scrollBar.orientation == Qt.Vertical ? position() : 2
            height: scrollBar.orientation == Qt.Vertical ?  Math.max(size(), scrollBar.width) : scrollBar.height - 4

            color: scrollBar.__colors[colorChoice]
        }

        states: State {
            name: "visible"
            when: scrollBar.orientation == Qt.Vertical ? scrollArea.movingVertically : scrollArea.movingHorizontally
            PropertyChanges { target: scrollBar; opacity: 1.0 }
        }

        transitions: Transition {
            from: "visible"; to: ""
            NumberAnimation { properties: "opacity"; duration: 600 }
        }

}
