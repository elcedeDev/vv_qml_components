// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S



Button{
    id: root
    objectName: "AradexStyleButton"
    implicitHeight: S.Style.styleButtonHeightInitial
    implicitWidth:  S.Style.styleButtonWidthInitial
    property string styleString
    property bool __alternate: false
//    property string stylesList
//    property variant langList: stylesList.replace(/\s+/g, '').split(",")
//    property string stylesNameList
//    property variant langNameList: stylesNameList.replace(/\s+/g, '').split(",")
    property int __choice: 0
    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    foregroundImageNormal : (S.Style[__styleName].styleButtonImage[styleString] != "" && typeof S.Style[__styleName].styleButtonImage[styleString] != 'undefined') ?
                                  S.Style[__styleName].imageFolder + S.Style[__styleName].styleButtonImage[styleString] : ""
    foregroundImagePressed : (S.Style[__styleName].styleButtonImage[styleString] != "" && typeof S.Style[__styleName].styleButtonImage[styleString] != 'undefined') ?
                                   S.Style[__styleName].imageFolder + S.Style[__styleName].styleButtonImage[styleString] : ""
    __foregroundImageHeight : S.Style[__styleName].stylebuttonImageHeight
    __foregroundImageWidth : S.Style[__styleName].stylebuttonImageWidth
    __foregroundHorizontalAlignement : S.Style[__styleName].stylebuttonImageHorizontalAlignement
    __foregroundVerticalAlignement : S.Style[__styleName].stylebuttonImageVerticalAlignement
    __foregroundXShift: S.Style[__styleName].stylebuttonImageXShift
    __foregroundYShift: S.Style[__styleName].stylebuttonImageYShift
    signal changeRootStyle(string style)

    onClicked:{
//        if(__alternate === true){
//            changeRootStyle(styleString)
//            __choice = (__choice + 1) % langList.length
//            styleString = langList[__choice]
//            text = langNameList[__choice]
//        }else{
           changeRootStyle(styleString)
//        }

    }

//    Component.onCompleted: {
//        if (__alternate === true) {
//            styleString = langList[0]
//        }
//   }


}
