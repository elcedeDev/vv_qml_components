import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1

Dialog{
    id: messageDialog
    objectName: "AradexDialog"
    property alias status: messageDialog.status
    property string __styleName: S.styleName
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    property int colorChoice: 1
    property string messageText: ""
    property string titleText: ""
    property int imageChoice: 0
    property bool __smooth: S.isFast
    signal styleChanged(string strStyle)

    property bool buttonCancelVisible: true
    property string buttonCancelText: "Cancel"
    property bool buttonNoVisible: true
    property string buttonNoText: "No"
    property bool buttonOkVisible: true
    property string buttonOkText: "OK"
    property alias dialogHeight: maindialogBox.height
    property alias dialogWidth: maindialogBox.width






    property string __errorImage: (S.Style[__styleName].errorImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].errorImage : ""
    property string __warningImage: (S.Style[__styleName].warningImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].warningImage : ""
    property string __messageImage: (S.Style[__styleName].messageImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                         S.Style[__styleName].messageImage : ""
    property alias customImage: messageImage.source

    GroupBox{
        id: maindialogBox
        colorChoice: messageDialog.colorChoice
        height: S.Style.dialogYesNoHeightInitial
        width: S.Style.dialogYesNoWidthInitial
        anchors.centerIn: parent
        headerText: messageDialog.titleText
        headerWidth: width

        Image{
            id: messageImage
            source: (imageChoice === 1) ? __errorImage :
                                          (imageChoice === 2) ? __warningImage :
                                          (imageChoice === 3) ? __messageImage : ""
            smooth: __smooth
            height: customImage !== "" ? S.Style.dialogButtonHeightInitial : 0
            width: height
            anchors{
                top: parent.top
                topMargin: maindialogBox.__headerHeight * 2
                left: parent.left
                leftMargin:  maindialogBox.__headerHeight


            }
        }

        MultilineTextField{
            id: messageTextField
            anchors{
                top: messageImage.top
                left: messageImage.right
                leftMargin: maindialogBox.__headerHeight / 2
                right: maindialogBox.right
                rightMargin: maindialogBox.__headerHeight / 2
                bottom:  buttonRow.top
                bottomMargin: maindialogBox.__headerHeight


            }
            text:  messageDialog.messageText
            readOnly: true
            __imageEnabled: ""
            __imageDisabled: ""
            __imageReadOnly: ""
            __imageFocus: ""


        }
        Row{
            id: buttonRow
            anchors{
                bottom: maindialogBox.bottom
                bottomMargin: S.Style.dialogButtonHeightInitial / 2
                horizontalCenter: maindialogBox.horizontalCenter
            }
            spacing: S.Style.dialogButtonHeightInitial / 2
            Button{
                id: okButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonOkText
                onClicked: {
                    messageDialog.setOk()
//                    messageDialog.close()
                }
                opacity: (buttonOkVisible === true) ? 1 : 0
            }
            Button{
                id: noButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonNoText
                onClicked: {
                    messageDialog.setNo()
//                    messageDialog.close()
                }
                opacity: (buttonNoVisible === true) ? 1 : 0
            }
            Button{
                id: cancelButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonCancelText
                onClicked: {
                    messageDialog.close()
                    cancel()
                }
                opacity: (buttonCancelVisible === true) ? 1 : 0
            }

        }
    }
}

