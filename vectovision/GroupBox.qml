// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S

FocusScope{
    id: root
    objectName: "AradexContainer"
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    signal scaledSizeChanged(real scaleFactor)


    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    height:  S.Style.containerHeightInitial
    width:  S.Style.containerWidthInitial

    /*!
        \qmlProperty bool GroupBox::headerVisible

        Specifies whether the header is visible.

        The default value is \c true. */
    property bool headerVisible:  S.Style[__styleName].containerHeaderVisible
    /*!
        \qmlProperty bool GroupBox::__headerInBorder

        If true the the the top margin will be increased by half the fontSize and the header will
        have no image and will get the background color.

        The default value is \c false. */
    property bool __headerInBorder:  S.Style[__styleName].containerHeaderTextInBorder
    /*!
        \qmlProperty int GroupBox::headerWidth

        Specifies the width of the header.

        The default value is \c S.Style[__styleName].containerHeaderWidth. */
    property int headerWidth:  S.Style[__styleName].containerHeaderWidth
    /*!
        \qmlProperty int GroupBox::__headerHeight

        Specifies the height of the header.

        The default value is \c S.Style[__styleName].containerHeaderHeight. */
    property int __headerHeight:  S.Style[__styleName].containerHeaderHeight
    /*!
        \qmlProperty real GroupBox::__textXShift

        Specifies how many pixels the text will be moved to the right.

        The default value is \c true. */
    property real __textXShift: S.Style[__styleName].containerTextXShift
    /*!
        \qmlProperty real GroupBox::__textYShift


        Specifies how many pixels the text will be moved down.

        The default value is \c true. */
    property real __textYShift: S.Style[__styleName].containerTextYShift

    property real __paintedHeaderWidth: headerText.__paintedWidth
    /*!
        \qmlProperty string GroupBox::headerText
        \brief The Text displayed in the header.

        The default value is \c "". */
    property alias headerText: headerText.text
    /*!
        \qmlProperty int GroupBox::__labelImage

        \brief The background image for the header.

        The background image for the header. The selection has to be an Integer between 0-3.
        The selected images will be S.Style[__styleName].labelImage0 - S.Style[__styleName].labelImage3
        accordingly.

        The default value is \c true. */
    property alias __labelImage: headerText.__imageChoice
    /*!
        \qmlProperty string GroupBox::__textColor

        Specifies the color of the header text.

        The default value is \c true. */
    property alias __textColor: headerText.__fontColorNormal
    /*!
        \qmlProperty int GroupBox::textSize

        Specifies the point size of the header text.

        The default value is \c true. */
    property alias fontSize: headerText.fontSize 

    property bool __smooth: S.isFast
    property int colorChoice: 0

    /*! \internal
        Array holding all container colors.
    */
    property variant __colorList: [S.Style[__styleName].containerColors0,
        S.Style[__styleName].containerColors1,
        S.Style[__styleName].containerColors2,
        S.Style[__styleName].containerColors3]

    ThemeShape {
        id: containerShape
        anchors.fill: parent
        anchors{
            leftMargin: __headerInBorder === true ? __textXShift : 0
            topMargin: __headerInBorder === true ? __textYShift : 0
        }
        colorChoice: root.__colorList[root.colorChoice][0]
        ThemeShape{
            id: headerShape
            anchors{
                left: parent.left
                leftMargin: containerShape.anchors.leftMargin
                top: parent.top
                topMargin: - containerShape.anchors.topMargin
            }
            width: headerWidth
            height: __headerHeight
            colorChoice: (headerVisible === true) ? root.__colorList[root.colorChoice][1] : 7

                Label{
                    id: headerText
                    anchors.fill: parent
                    colorChoice: root.__colorList[root.colorChoice][2]
                    __verticalTextAlignement: Qt.AlignTop
                    horizontalTextAlignement: Qt.AlignLeft
                    fontSize: 10
                    Rectangle{
                        anchors.fill: parent
                        color: S.Style[__styleName].backgroundColor
                        opacity: (__headerInBorder === true) ? true : false
                        z:-1
                    }
                }
        }



    }


}
