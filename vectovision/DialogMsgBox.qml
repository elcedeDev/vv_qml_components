import QtQuick 1.1
import "../../style/Style.js"  as S
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1

Dialog{
    id: messageDialog
    objectName: "AradexDialog"
    property alias status: messageDialog.status
    property string __styleName: S.styleName
    signal scaledSizeChanged(real scaleFactor)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    property int colorChoice: 1
    property string messageText: ""
    property string titleText: ""
    property string __okText: "OK"
    property int imageChoice: 0
    property bool __smooth: S.isFast
    signal styleChanged(string strStyle)
    property alias dialogHeight: maindialogBox.height
    property alias dialogWidth: maindialogBox.width



    property string __errorImage: (S.Style[__styleName].errorImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].errorImage : ""
    property string __warningImage: (S.Style[__styleName].warningImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].warningImage : ""
    property string __messageImage: (S.Style[__styleName].messageImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                         S.Style[__styleName].messageImage : ""
    property alias customImage: messageImage.source


    GroupBox{
        id: maindialogBox
        colorChoice: messageDialog.colorChoice
        height: S.Style.dialogMsgBoxHeightInitial
        width: S.Style.dialogMsgBoxWidthInitial
        anchors.centerIn: parent
        headerText: messageDialog.titleText
        headerWidth: width

        Image{
            id: messageImage
            source: (imageChoice === 1) ? __errorImage :
                                          (imageChoice === 2) ? __warningImage :
                                          (imageChoice === 3) ? __messageImage : ""
            smooth: __smooth
            height: customImage !== "" ? S.Style.dialogButtonHeightInitial : 0
            width: height
            anchors{
                top: parent.top
                topMargin: maindialogBox.__headerHeight * 2
                left: parent.left
                leftMargin:  maindialogBox.__headerHeight


            }
        }

        MultilineTextField{
            id: messageTextField
            anchors{
                top: messageImage.top
                left: messageImage.right
                leftMargin: maindialogBox.__headerHeight / 2
                right: maindialogBox.right
                rightMargin: maindialogBox.__headerHeight / 2
                bottom:  okButton.top
                bottomMargin: maindialogBox.__headerHeight

            }
            text:  messageDialog.messageText
            readOnly: true
            __imageEnabled: ""
            __imageDisabled: ""
            __imageReadOnly: ""
            __imageFocus: ""
            fontColorChoice: 3

        }
        Button{
            id: okButton
            width: S.Style.dialogButtonWidthInitial
            height: S.Style.dialogButtonHeightInitial
            anchors{
                bottom: maindialogBox.bottom
                bottomMargin: S.Style.dialogButtonHeightInitial / 2
                horizontalCenter: maindialogBox.horizontalCenter
            }
            text: __okText
            onClicked: messageDialog.setOk()
        }


    }
}

