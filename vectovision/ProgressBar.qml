// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools.shortcut 1.1
import tools 1.1

FocusScope {
    id:root
    objectName: "AradexProgressBar"
    implicitWidth: S.Style.progressbarWidthInitial
    implicitHeight: S.Style.progressbarHeightInitial

    /*! \internal
    The style from Style.js needs to be redirected
    to a property in order to trigger events on change.
    */

    signal styleChanged(string strStyle)
    property string __styleName: S.styleName
    property bool __smooth: S.isFast
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    signal scaledSizeChanged(real scaleFactor)

    property string __backgroundImage: (S.Style[__styleName].progressBarBackground != "") ?
                                             S.Style[__styleName].imageFolder + S.Style[__styleName].progressBarBackground : ""


    property string __fillingImage: (S.Style[__styleName].progressBarFilling != "") ?
                                      S.Style[__styleName].imageFolder + S.Style[__styleName].progressBarFilling : ""

    property real progressValue: 0

	//property alias value: root.progressValue
	
	property string valueIo
    property variant __valueIo: eval(valueIo)
    // make sure IO never changes to ""
    onValueIoChanged: {
        if(valueIo !== "") {
            __valueIo = eval(valueIo)
        }
    }
	
	property real valueOffset: 0
    property real valueFactor: 100
	property real maxValue: 100
	property string valueUnit : "%"
	
    property bool showValue: true

	property variant valueIOValue: (__valueIo !== undefined) ? (__valueIo.value * valueFactor + valueOffset) : (progressValue * valueFactor + valueOffset)


    BorderImage {
        id: behaelterImage
        anchors.fill: parent
        smooth: __smooth
        source: __backgroundImage
        border.left: S.Style[__styleName].progressBarBackgroundBorderLeft
        border.top: S.Style[__styleName].progressBarBackgroundBorderTop
        border.right: S.Style[__styleName].progressBarBackgroundBorderRight
        border.bottom: S.Style[__styleName].progressBarBackgroundBorderBottom
        anchors.centerIn: parent
        clip:true
        Image {
            id: fillingImage
            x: S.Style[__styleName].progressBarFillingX
            y: S.Style[__styleName].progressBarFillingY
            fillMode: Image.Tile
            smooth: __smooth
            source: __fillingImage
            height: parent.height - (2 * x)
            width: (parent.width - (2 * x)) * valueIOValue / maxValue
            z: S.Style[__styleName].progressBarFillingInBackground === true ? -1 :0
            sourceSize{
                height: S.Style[__styleName].progressBarFillingSourceWidth * thisScale
                width: S.Style[__styleName].progressBarFillingSourceHeight  * thisScale
            }
        }
        Label{
            id: valueLabel
            horizontalTextAlignement: Text.AlignHCenter
            __verticalTextAlignement: Text.AlignVCenter
            text: (Math.round(valueIOValue)).toString() + valueUnit
            anchors.centerIn: parent
            __fontBold: S.Style[__styleName].progressBarTextBold
            colorChoice: S.Style[__styleName].progressBarTextColorChoice
            __fontOutline: S.Style[__styleName].progressBarTextOutline
            __fontOutlineColor: S.Style[__styleName].progressBarTextOutlineColor
            opacity: showValue == true ? 1 : 0
        }



    }

}
