import QtQuick 1.1
import "../../style/Style.js"  as S


Rectangle {
    id: backgroundRectangle
    objectName: "AradexBackground"
    signal styleChanged(string strStyle)
    property string __styleName: S.styleName
    property bool __smooth: S.isFast
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    color: S.Style[__styleName].backgroundColor
}
