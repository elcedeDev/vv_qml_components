// $Revision: 17742 $
import QtQuick 1.1
import "../../style/Style.js"  as S
import tools.shortcut 1.1
import tools 1.1



Button{
    id: root
    objectName: "AradexLanguageButton"
    implicitHeight: S.Style.languageButtonHeightInitial
    implicitWidth: S.Style.languageButtonWidthInitial
    property string languageString
    property string __languageString_short: (languageString != "") ? languageString.substring(0,2) : ""
    property bool __alternate: false
    property bool defaultLanguage: false
//    property string languagesList
//    property variant langList: languagesList.replace(/\s+/g, '').split(",")
//    property string languagesNameList
//    property variant langNameList: languagesNameList.replace(/\s+/g, '').split(",")
    property int __choice: 0
    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    foregroundImageNormal : (S.Style[__styleName].languageButtonImage[languageString] != "") ?
                                S.Style[__styleName].imageFolder + S.Style[__styleName].languageButtonImage[__languageString_short] : ""
    foregroundImagePressed :  (S.Style[__styleName].languageButtonImage[languageString] != "") ?
                                  S.Style[__styleName].imageFolder + S.Style[__styleName].languageButtonImage[__languageString_short] : ""
    __foregroundImageHeight : S.Style[__styleName].languagebuttonImageHeight
    __foregroundImageWidth : S.Style[__styleName].languagebuttonImageWidth
    __foregroundHorizontalAlignement : S.Style[__styleName].languagebuttonImageHorizontalAlignement
    __foregroundVerticalAlignement : S.Style[__styleName].languagebuttonImageVerticalAlignement
    __foregroundXShift: S.Style[__styleName].languagebuttonImageXShift
    __foregroundYShift: S.Style[__styleName].languagebuttonImageYShift
    onClicked:{
           ltTranslate.selectLanguage(languageString, defaultLanguage)

    }



}
