import QtQuick 1.1

Item {
    id: postMessageV8
    objectName: "PostMessageV8"

    property int ms_CRITICAL_ERROR : 0
    property int ms_ERROR          : 1
    property int ms_WARNING        : 2
    property int ms_INFO           : 3


    signal postMessage(int eSeverity, string strMessage)
}

